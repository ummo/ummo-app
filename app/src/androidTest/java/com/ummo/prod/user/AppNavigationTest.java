package com.ummo.prod.user;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.Gravity;

import com.ummo.prod.user.view.BottomNavActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerActions.open;
import static android.support.test.espresso.contrib.DrawerMatchers.isClosed;
import static android.support.test.espresso.contrib.NavigationViewActions.navigateTo;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Jose on 2017/07/17.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AppNavigationTest {
    @Rule
    public ActivityTestRule<BottomNavActivity> bottomNavActivityActivityTestRule =
            new ActivityTestRule<>(BottomNavActivity.class);

    @Test
    public void navigateHomeFromBottomNav(){
        //Navigate through Bottom Navigation & go Home
        onView(withId(R.id.bottom_navigation)).perform(navigateTo(R.id.bottom_nav_home));
        //Check that the active fragment is Home
        String expectedHomeScreenText = "Join a Queue Below";
        onView(withId(R.id.textLabel)).check(matches(withText(expectedHomeScreenText)));
    }

    @Test
    public void navigateToDiscoverFromBottomNav(){
        //Navigate to Discover
        onView(withId(R.id.bottom_navigation)).perform(navigateTo(R.id.bottom_nav_discover));
        String expectedDiscoverScreenText = "Discover Important Services";
        onView(withId(R.id.textLabel)).check(matches(withText(expectedDiscoverScreenText)));
    }

    @Test
    public void navigateToProfileFromBottomNav(){
        //Navigate to Profile
        onView(withId(R.id.bottom_navigation)).perform(navigateTo(R.id.bottom_nav_profile));
        String expectedProfileScreenText = "My Profile";
        onView(withId(R.id.textLabel)).check(matches(withText(expectedProfileScreenText)));
    }
}
