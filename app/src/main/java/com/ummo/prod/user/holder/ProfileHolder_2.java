package com.ummo.prod.user.holder;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.johnkil.print.PrintView;
import com.ummo.prod.user.R;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by barnes on 8/6/15.
 **/
public class ProfileHolder_2 extends TreeNode.BaseNodeViewHolder<IconTreeItemHolder.IconTreeItem> {


    public ProfileHolder_2(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(TreeNode node, IconTreeItemHolder.IconTreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_profile_node, null, false);

        TextView tvValue = (TextView) view.findViewById(R.id.node_value);
        tvValue.setText(value.text);
        tvValue.setTextColor(Color.GRAY);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(view.getContext(),"Category currently empty...",Toast.LENGTH_SHORT).show();
            }
        });

        final PrintView iconView = (PrintView) view.findViewById(R.id.icon);
        iconView.setIconColor(R.color.gray);
        iconView.setIconText(context.getResources().getString(value.icon));
        return view;
    }



    @Override
    public void toggle(boolean active)
    {
    }

    @Override
    public int getContainerStyle() {
        return R.style.TreeNodeStyleCustom;
    }
}