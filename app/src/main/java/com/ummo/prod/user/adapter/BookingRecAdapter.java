package com.ummo.prod.user.adapter;

/**
 * Created by José on 24/02/17
 **/

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.Main_Activity;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.view.BookedActivity;
import com.ummo.prod.user.R;
import com.ummo.prod.user.interfaces.UmmoEventListener;
import com.ummo.prod.user.view.BottomNavActivity;
import com.ummo.prod.user.view.fragment.DiscoverFragment;
import com.ummo.prod.user.ummoAPI.Agent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class BookingRecAdapter extends RecyclerView.Adapter<BookingRecAdapter.BookingHolder> {

    //private List<BookingWrapper> bookingData;
    private LayoutInflater inflater;
    private List<Agent> bookingsData = new ArrayList<>();
    private String location;

    private DiscoverFragment context;

    private final String projectToken = "67547a5eed2c873e61ccbfd531eb6d26";
    private MixpanelAPI mixpanelAPI;

    public interface itemClickCallback {
        void onItemClick(int p);
        void onSecondaryIconClick(int p);
    }

    public void setItemClickCallback (final itemClickCallback itemClickCallback) {
    }

    BookingRecAdapter(List<Agent> bookingData, DiscoverFragment c, String _location) {
        //Come back to review
        this.inflater = LayoutInflater.from(c.getActivity());
        context=c;
        this.location=_location;
        this.bookingsData = bookingData;
        Log.e("CONSTRUCTOR",bookingsData.toString());
    }

    @Override
    public BookingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.booking_item, parent, false);
        return new BookingHolder(view);
    }

    @Override
    public void onBindViewHolder(final BookingHolder holder, final int position) {
        Agent item = bookingsData.get(position);
        Log.e("AGENT ",bookingsData.get(position).toString());
        holder.bookedService.setText(bookingsData.get(position).getEmail());
        holder.booker.setText(bookingsData.get(position).getFirstName()+" "+bookingsData.get(position).getSurName());
        holder.contactDetails.setText(bookingsData.get(position).getCell());
        holder.bookedDateTime.setText(location);
        holder.bookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("BOOKINGS","BOOK CLICKED");
                mixpanelAPI = MixpanelAPI.getInstance(v.getContext(), projectToken);

                /*View parentLayout = findViewById(android.R.id.content);

                final ForegroundColorSpan redSpan =
                        new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.warning_stroke_color));
                SpannableStringBuilder snackbarWarning = new SpannableStringBuilder("No Queues found...");
                snackbarWarning.setSpan(redSpan, 0, snackbarWarning.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                Snackbar snackbar = Snackbar.make(parentLayout, snackbarWarning, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(R.color.ummo_1));
                snackbar.show();*/

                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("makeBooking", "onClick");
                    mixpanelAPI.track("Make Booking", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
               // try {
                   // holder.newBooking.put("year",0);
                    //holder.newBooking.put("month",0);
                   // holder.newBooking.put("day",0);
                   // holder.newBooking.put("hour_end",0);
                   // holder.newBooking.put("hour_start",23);
                   // holder.newBooking.put("min_end",0);
                    //holder.newBooking.put("min_start",59);
                    //BookingWraper.loadAgentBookings(holder.data.getString("_id"));
                    //Intent intent = new Intent(context,BookingModActivity.class);
                    //intent.putExtra("agent",bookingsData.get(position).get_id());
                   // Log.e("agent",holder.data.toString());
                    //holder.bookBtn.getContext().startActivity(intent);
                //}catch(JSONException jse){
                //    Log.e("JSON ERR",jse.toString());
                //}
            }
        });
        if (position == 0) {
            int here = holder.getAdapterPosition();
            Log.e("REC-VIEW" + here, "!");
        }

    }

    private void showDayDialog(final BookingHolder vh) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(vh.container.getContext());
        LayoutInflater inflater = (LayoutInflater) vh.container.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        Log.e("DATE","CLICKED");
        assert inflater != null;
        final View view = inflater.inflate(R.layout.date_lv, null);
        final DatePicker datePicker = view.findViewById(R.id.datePicker1);
        builder.setView(view)
                // Add action buttons
                .setPositiveButton("Start Time", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        Log.e("Show Dialog","START Time");
                        Log.e("BOOKING ELAPSED",""+ qman.getQman().getDayElapsed(datePicker.getYear(),datePicker.getMonth(),datePicker.getDayOfMonth(),0,0));
                        try {

                            vh.newBooking.put("day",datePicker.getDayOfMonth());
                            vh.newBooking.put("month",datePicker.getMonth());
                            vh.newBooking.put("year",datePicker.getYear());
                            Log.e("BOOKING",vh.newBooking.toString());
                        }
                        catch (JSONException jse){
                            Log.e("JSON ERROR",jse.toString());
                        }

                        Log.e("Day Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking));
                        if(qman.getQman().bookingElapsed(vh.newBooking)){
                            Log.e("Booking Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking));

                            new AlertDialog.Builder(vh.container.getContext())
                                    .setTitle("Selected Date has Elapsed")
                                    .setPositiveButton("Set Date", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            showDayDialog(vh);
                                        }
                                    })
                                    .create().show();
                            return;
                        }
                        Log.e("SELECTEDDATE",vh.newBooking.toString());
                        showStartTimeDialog(vh);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //LoginDialogFragment.this.getDialog().cancel();
                    }
                }).create().show();
    }

    private void showStartTimeDialog(final BookingHolder vh){
        AlertDialog.Builder builder = new AlertDialog.Builder(vh.container.getContext());
        LayoutInflater inflater = (LayoutInflater) vh.container.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        assert inflater != null;
        final View v = inflater.inflate(R.layout.start_time_ly, null);
        final TimePicker timePicker = v.findViewById(R.id.timePicker2);
        timePicker.setIs24HourView(true);
        final AppCompatTextView unavailableStart = v.findViewById(R.id.unavailable_times_start);
        String times = "Unavailable times: \n";

        try {
            JSONArray bks = vh.data.getJSONArray("bookings");
            for (int i=0;i<bks.length();i++){
                String t1 =bks.getJSONObject(i).getString("start");
                String t2= bks.getJSONObject(i).getString("end");
                Calendar startC=Calendar.getInstance();
                Calendar endC = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                simpleDateFormat.setTimeZone(TimeZone.getDefault());
                try {
                    startC.setTime(simpleDateFormat.parse(t1));
                    endC.setTime(simpleDateFormat.parse(t2));
                    Log.e("DATE",t1);
                    String day = vh.newBooking.getInt("day")>10?""+(vh.newBooking.getInt("day")):"0"+(vh.newBooking.getInt("day"));
                    String mnt=vh.newBooking.getInt("month")+1>10?""+(1+vh.newBooking.getInt("month")):"0"+(1+vh.newBooking.getInt("month"));
                    times+=t1.contains(vh.newBooking.getInt("year")+"-"+mnt+"-"+day)?startC.get(Calendar.HOUR_OF_DAY)+":"+startC.get(Calendar.MINUTE)+" => "+endC.get(Calendar.HOUR_OF_DAY)+":"+endC.get(Calendar.MINUTE)+"\n":"";
                }catch (ParseException pse){
                    Log.e("PARSE-ERR",getClass().getCanonicalName()+pse.toString());
                }

            }
        }
        catch (JSONException jse){
            Log.e("JSON ERR",jse.toString());
        }
        unavailableStart.setText(times);
        Log.e("DATE","CLICKED");
        builder.setView(v)
                // Add action buttons
                .setPositiveButton("End Time", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        try {
                            vh.newBooking.put("hour_start",android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M?timePicker.getHour():timePicker.getCurrentHour());
                            vh.newBooking.put("min_start",android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M?timePicker.getMinute():timePicker.getCurrentMinute());
                            if(vh.data.has("bookings")){
                                JSONArray _bookings=vh.data.getJSONArray("bookings");
                                for(int i=0;i<_bookings.length();i++){
                                    if(new TimeUtils(vh.newBooking).overlaps(_bookings.getJSONObject(i))){
                                        Log.e("OVERLAP",vh.newBooking.toString()+":"+_bookings.getJSONObject(i).toString());
                                        new AlertDialog.Builder(vh.container.getContext()).setTitle("Selected Time Slot is not available")
                                                .setPositiveButton("Start Time", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        showStartTimeDialog(vh);
                                                    }
                                                })
                                                .create().show();
                                        return;
                                    }
                                }
                            }
                        }
                        catch (JSONException jse){
                            Log.e("JOSON ERR",jse.toString());
                        }
                        Log.e("Booking Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking)+vh.newBooking.toString());
                        if(qman.getQman().bookingElapsed(vh.newBooking)){
                            Log.e("Booking Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking));
                            new AlertDialog.Builder(vh.container.getContext())
                                    .setTitle("Selected Time has Elapsed")
                                    .setPositiveButton("Set Date", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            showStartTimeDialog(vh);
                                        }
                                    })
                                    .create().show();
                            return;
                        }
                        Log.e("SELECTED-ST",vh.newBooking.toString());
                        showEndTime(vh);

                    }
                })
                .setNegativeButton("Day", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showDayDialog(vh);
                    }
                }).create().show();

    }

    private void showEndTime(final BookingHolder vh){
        AlertDialog.Builder builder = new AlertDialog.Builder(vh.container.getContext());
        LayoutInflater inflater = (LayoutInflater) vh.container.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        int maxTime=0;
        Log.e("DATE","CLICKED");
        assert inflater != null;
        final View v = inflater.inflate(R.layout.layout_end_time, null);
        final TimePicker timePicker = v.findViewById(R.id.timePicker1);
        final TextView endTimeLimitTv= v.findViewById(R.id.end_time_limit);
        timePicker.setIs24HourView(true);
        try {
            maxTime =vh.data.has("endTime")? new TimeUtils(vh.newBooking).nearestTime(vh.data.getJSONArray("bookings"), vh.data.getJSONObject("endTime").getInt("hour") * 60 + vh.data.getJSONObject("endTime").getInt("min")):new TimeUtils(vh.newBooking).nearestTime(vh.data.getJSONArray("bookings"), 23 * 60 + 59);
            endTimeLimitTv.setText("You have up to "+new Integer(maxTime/60)+":"+maxTime%60);
        }
        catch (JSONException jse){
            Log.e("SELECTABLE-TREE",jse.toString());
        }
        if(!vh.data.has("bookings")){
            if(vh.data.has("endTime")&&maxTime==0){
                try {
                    endTimeLimitTv.setText("You have up to "+vh.data.getJSONObject("endTime").getInt("hour")+":"+vh.data.getJSONObject("endTime").getInt("min"));
                }
                catch (JSONException jse){
                    Log.e("SELECTABLE TREE",jse.toString());
                }
            }

        }

        builder.setView(v)
                // Add action buttons
                .setPositiveButton("SERVICE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...


                        try {
                            vh.newBooking.put("hour_end",android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M?timePicker.getHour():timePicker.getCurrentHour());
                            vh.newBooking.put("min_end",android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M?timePicker.getMinute():timePicker.getCurrentMinute());
                            if((vh.newBooking.getInt("hour_start")>vh.newBooking.getInt("hour_end")||(vh.newBooking.getInt("hour_start")==vh.newBooking.getInt("hour_end")&&vh.newBooking.getInt("min_start")>vh.newBooking.getInt("min_end")))){
                                new AlertDialog.Builder(vh.container.getContext())
                                        .setTitle("You cannot finish a booking before you start it")
                                        .setPositiveButton("Set Date", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                showDayDialog(vh);
                                            }
                                        })
                                        .create().show();
                                return;
                            }

                            if(qman.getQman().bookingElapsed(vh.newBooking)){
                                Log.e("Booking Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking));

                                new AlertDialog.Builder(vh.container.getContext())
                                        .setTitle("Selected Time has Elapsed")
                                        .setPositiveButton("Set Date", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                showDayDialog(vh);
                                            }
                                        })
                                        .create().show();
                                return;
                            }
                            showDialogServiceInput(vh);
                        }
                        catch (JSONException jse){
                            Log.e("JOSON ERR",jse.toString());
                        }
                        Log.e("SELECTEDET",vh.newBooking.toString());
                    }
                })
                .setNegativeButton("START TIME", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showStartTimeDialog(vh);
                    }
                }).create().show();
    }

    private void showDialogServiceInput(final BookingHolder vh){
        final AlertDialog.Builder builder = new AlertDialog.Builder(vh.container.getContext());
        LayoutInflater inflater = (LayoutInflater) vh.container.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        assert inflater != null;
        final View v = inflater.inflate(R.layout.layout_service_input, null);
        builder.setView(v)
                // Add action buttons
                .setPositiveButton("MAKE BOOKING", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String service = "";
                        try{
                            int hour_start = Integer.valueOf(vh.newBooking.getString("hour_start"));
                            int hour_end =Integer.valueOf(vh.newBooking.getString("hour_end"));
                            int min_start = Integer.valueOf(vh.newBooking.getString("min_start"));
                            int min_end =Integer.valueOf(vh.newBooking.getString("min_end"));
                            int year = Integer.valueOf(vh.newBooking.getString("year"));
                            int month = Integer.valueOf(vh.newBooking.getString("month"));
                            int day = Integer.valueOf(vh.newBooking.getString("day"));

                            Calendar startDate = Calendar.getInstance();
                            startDate.set(year,month,day,hour_start,min_start);
                            Calendar endDate = Calendar.getInstance();
                            endDate.set(year,month,day,hour_end,min_end);
                            vh.newBooking.put("agent",vh.data.get("_id"));
                            vh.newBooking.put("booker", qman.getQman().getUmmoId());
                            vh.newBooking.put("service",((AppCompatEditText)v.findViewById(R.id.serviceInput)).getText().toString());
                            //Log.e("BOOK")
                            Log.e("SELECTEDDATE",vh.newBooking.toString());
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(vh.container.getContext());
                            Log.e("DATE",startDate.toString());
                            String bookingMessage = "You are booking "+vh.data.getJSONObject("fullName").getString("firstName")
                                    +" For "+vh.newBooking.getString("service")
                                    +" \nFrom : "+ startDate.get(Calendar.DAY_OF_MONTH)
                                    +" "+startDate.getDisplayName(Calendar.MONTH,Calendar.LONG,Locale.getDefault())+" "+startDate.get(Calendar.YEAR)
                                    +" "+ startDate.get(Calendar.HOUR_OF_DAY)+":"+startDate.get(Calendar.MINUTE)
                                    +" \nTo : "+ startDate.get(Calendar.DAY_OF_MONTH)
                                    +" "+endDate.getDisplayName(Calendar.MONTH,Calendar.LONG,Locale.getDefault())+" "+endDate.get(Calendar.YEAR)
                                    +" "+ endDate.get(Calendar.HOUR_OF_DAY)+":"+endDate.get(Calendar.MINUTE);
                            builder1.setTitle("Confirm Details")
                                    .setMessage(bookingMessage)
                                    .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            qman.getQman().createBooking(vh.newBooking);
                                            //showLoadingDialog(vh);
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .setNeutralButton("Back", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            showDialogServiceInput(vh);
                                        }
                                    })
                                    .create().show();
                           //
                        }
                        catch (JSONException jse){
                            Log.e("JSON ERR",jse.toString());
                        }
                        Log.e("BOOKING",vh.newBooking.toString());
                       // showLoadingDialog(vh);
                    }
                })
                .setNegativeButton("END TIME", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //LoginDialogFragment.this.getDialog().cancel();
                        showEndTime(vh);
                    }
                }).create().show();

    }

    /*private void showLoadingDialog(final BookingHolder vh){
        final AlertDialog.Builder builder = new AlertDialog.Builder(vh.container.getContext());
        LayoutInflater inflater = (LayoutInflater) vh.container.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        assert inflater != null;
        //final View v = inflater.inflate(R.layout.layout_loading, null);
        //final AlertDialog dialog = builder.setView(v).setTitle("Saving your booking").create();
        dialog.show();
        qman.getQman().registerEventListener("create-booking", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                ((Activity)vh.container.getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("BOOKING-RV","BOOKED");
                        dialog.dismiss();
                        vh.container.getContext().startActivity(new Intent(vh.container.getContext(), BookedActivity.class));
                    }
                });

                return null;
            }
        });
    }*/


    public void setBookingsData(List<JSONObject> exerciseList) {
        this.bookingsData.clear();
       // this.bookingsData.addAll(exerciseList);
    }

    @Override
    public int getItemCount() {
        return bookingsData.size();
    }

    class TimeUtils{
        JSONObject newBooking;
        TimeUtils(JSONObject _newBooking){
            newBooking = _newBooking;
        }

        public boolean overlaps(JSONObject _booking){ //This Funtion Checks if the Start of a new Booking does not overlap _booking
            boolean ovl=true;
            boolean ovr=true;
            boolean sameday = true;
            try{
                String _start=_booking.getString("start");
                String _end=_booking.getString("end");
                Log.e("OVERLAP",_end.substring(11,13));
                ovr=(newBooking.getInt("hour_start")*60+newBooking.getInt("min_start"))<(60*new Integer(_end.substring(11,13))+new Integer(_end.substring(14,16)));//+Integer.getInteger(_end.substring(14,16));*/
                ovl=(newBooking.getInt("hour_start")*60+newBooking. getInt("min_start"))>(60*new Integer(_start.substring(11,13))+new Integer(_start.substring(14,16)));
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                simpleDateFormat.setTimeZone(TimeZone.getDefault());
                try {
                    Calendar cal = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    cal.setTime(simpleDateFormat.parse(_booking.getString("start")));
                    cal2.set(newBooking.getInt("year"),newBooking.getInt("month"),newBooking.getInt("day"));
                    sameday=cal.get(Calendar.DAY_OF_YEAR)==cal2.get(Calendar.DAY_OF_YEAR);
                }catch (ParseException pse){
                    Log.e("PARSE-ERR",getClass().getCanonicalName()+pse.toString());
                }
            }
            catch (JSONException jse){
                Log.e("ERROR",jse.toString());
            }
            return ovr&&ovl&&sameday;
        }

        int nearestTime(JSONArray _bookings, int endTime){
            int nextStart=endTime;
            Log.e("NEXT-START",""+nextStart);
            try {
                for(int i = 0;i<_bookings.length();i++){
                    JSONObject booking = _bookings.getJSONObject(i);
                    String hour = booking.getString("start").substring(11,13);
                    String min = booking.getString("start").substring(14,16);
                    int mins=0;
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    simpleDateFormat.setTimeZone(TimeZone.getDefault());
                    try {
                        Calendar cal = Calendar.getInstance();
                        Calendar cal2 = Calendar.getInstance();
                        cal.setTime(simpleDateFormat.parse(booking.getString("start")));
                        cal2.set(newBooking.getInt("year"),newBooking.getInt("month"),newBooking.getInt("day"));
                        mins = cal.get(Calendar.HOUR_OF_DAY)*60+cal.get(Calendar.MINUTE);
                        Log.e("TIME",""+cal2.get(Calendar.DAY_OF_YEAR));
                        int bookingStart = newBooking.getInt("hour_start")*60+newBooking.getInt("min_start");
                        nextStart=(bookingStart<mins)&&(mins<nextStart)&&cal.get(Calendar.DAY_OF_YEAR)==cal2.get(Calendar.DAY_OF_YEAR)?mins:nextStart;
                    }catch (ParseException pse){
                        Log.e("PARSERR",getClass().getCanonicalName()+pse.toString());
                    }
                }
            }
            catch(JSONException jse){
                Log.e("SELECTABLE-TREE",jse.toString());
            }
            return nextStart;
        }
    }

    class BookingHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView booker;
        private TextView contactDetails;
        private TextView bookedService;
        private TextView bookedDateTime;
        private AppCompatTextView bookBtn;
        public JSONObject newBooking=new JSONObject();
        public JSONObject data;
        //private ImageView avatar;
        private View container;

        BookingHolder(View itemView) {
            super(itemView);
            booker = itemView.findViewById(R.id.booker_tv);
            contactDetails = itemView.findViewById(R.id.booker_contact_tv);
            bookedService = itemView.findViewById(R.id.agent_email);
            bookedDateTime = itemView.findViewById(R.id.agent_location);
            bookBtn = itemView.findViewById(R.id.subscribe_btn);
            bookBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(DiscoverFragment.newInstance().getContext(), "Service Subscriptions coming soon...", Toast.LENGTH_LONG).show();
                }
            });
            container = itemView.findViewById(R.id.container_item_root);
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.container_item_root) {
//                itemClickCallback.onItemClick(getAdapterPosition());
            } else {
   //             itemClickCallback.onSecondaryIconClick(getAdapterPosition());
            }

        }
    }
}