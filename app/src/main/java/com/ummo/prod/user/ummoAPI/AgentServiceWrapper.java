package com.ummo.prod.user.ummoAPI;

import android.util.Log;

import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mosaic on 6/26/17.
 */

public class AgentServiceWrapper {
    private String name;
    private String location;
    private String email;
    private String category;
    private static List<AgentServiceWrapper> services = new ArrayList<>();
    private List<Agent> agents = new ArrayList<>();
    private final static String TAG = "AgentWrapper";

    private static DataReadyCallback onBookingServices;

    public static void setOnBookingServices(DataReadyCallback _onBookingServices) {
        onBookingServices = _onBookingServices;
    }

    public static DataReadyCallback getOnBookingServices() {
        return onBookingServices;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public static List<AgentServiceWrapper> getServices() {
        return services;
    }

    public static void setServices(List<AgentServiceWrapper> services) {
        AgentServiceWrapper.services = services;
    }

    public List<Agent> getAgents() {
        return agents;
    }

    public void setAgents(List<Agent> agents) {
        this.agents = agents;
    }

    public String getName() {

        return name;
    }

    private AgentServiceWrapper(JSONObject obj){
        try{
            setEmail(obj.getString("email"));
            setCategory(obj.getString("category"));
            setLocation(obj.getString("address"));
            setName(obj.getString("name"));

            if(obj.has("qmasters")){
                JSONArray _agents = obj.getJSONArray("qmasters");
                for (int i=0;i<_agents.length();i++){
                    Agent agent = new Agent(_agents.getJSONObject(i));
                    this.agents.add(agent);
                }
            }
        }catch (JSONException jse){
            Log.e("AGENTERR",jse.toString());
        }
    }

    public static void fetchAgentServices(){
        SocketClass.emit("agent-services", "", new SocketClass.Response() {
            @Override
            public Object ready(Object val) {
                Log.e(TAG+" fetchAgent", "Agent->"+val.toString());
                try {
                    JSONArray arr = (JSONArray)val;
                    services.clear();
                    for(int i=0;i<arr.length();i++){
                        JSONObject obj = arr.getJSONObject(i);
                        services.add(new AgentServiceWrapper(obj));
                    }

                    if(onBookingServices!=null){
                        onBookingServices.exec(services);
                    }else {
                        Log.e("ONBOOKINGSERVICES","Listener Not Set");
                    }

                }catch (JSONException jse){
                    Log.e("AGENTSERVERR",jse.toString());
                }
                return null;
            }
        });
    }
    //private List<Agent>
}
