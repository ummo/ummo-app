package com.ummo.prod.user.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.github.edsergeev.TextFloatingActionButton;
import com.github.florent37.hollyviewpager.HollyViewPagerBus;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import com.ummo.prod.user.Main_Activity;
import com.ummo.prod.user.R;
import com.ummo.prod.user.db.room.AppDatabase;
import com.ummo.prod.user.db.room.entities.QueueJoin;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;
import com.ummo.prod.user.ummoAPI.QueueJoinWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.doorbell.android.Doorbell;

import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

/**
 * Created by bane on 12/30/15.
 **/

public class RPTFragment extends Fragment {
    //DrawableAwesome bg;
    @BindView(R.id.rpt_scrollView) ObservableScrollView scrollView;
    @BindView(R.id.man_one_text) TextView alphaNum;
    @BindView(R.id.alphanum) TextView numTV;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.exit_fab) FloatingActionButton exitFab;
    @BindView(R.id.man_one) TextFloatingActionButton btn1;
    @BindView(R.id.man_two) TextFloatingActionButton btn2;
    @BindView(R.id.man_three) TextFloatingActionButton btn3;
    @BindView(R.id.man_four) TextFloatingActionButton btn4;
    @BindView(R.id.man_five) TextFloatingActionButton btn5;
    View view;
    private int i = -1;
    Context context;

    static String qName;
    static String qPosition;
    static String qAlphanum;
    static String qID;
    private boolean paused=false;
    private static String TAG = "RPTFragment";
    private static JSONArray retrievedQueueArray;
    private static String retrievedQueueJoins = Main_Activity.savedQueueJoins;
    private static JSONObject args_off = new JSONObject();
    QueueJoinWrapper qInfo;
    List<Animation> pulses = new ArrayList<>();
    List<TextFloatingActionButton> posFABs = new ArrayList<>();
    private AppDatabase appDb;
    private QueueJoinWrapper queueJoinWrapper;

    public MixpanelAPI mixpanel;
    private boolean isConnected = qman.getQman().isNetWorkStateConnected();

    public static RPTFragment newInstance(QueueJoinWrapper joinedQ){//Was "static"
        Bundle args_on = new Bundle();

        try {
            retrievedQueueArray = new JSONArray(retrievedQueueJoins);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RPTFragment rptFragment = new RPTFragment();

        Log.e(TAG+" constructor", "(QJW) Joins->"+joinedQ.toString());
        args_on.putString("title", joinedQ.getQueue().getqName());
        args_on.putString("qname", joinedQ.getQueue().getqName());
        args_on.putString("qposition", ""+joinedQ.getPosition());
        args_on.putString("qalphanum", joinedQ.getNumCode());
        args_on.putString("qID",joinedQ.getQueue().get_id());
        qID =joinedQ.getQueue().get_id();

        rptFragment.setArguments(args_on);

        if (retrievedQueueArray!=null) {
            for (int i = 0; i < retrievedQueueArray.length(); i++) {
                Log.e(TAG + " constructor_3", "For loop (array)->" + retrievedQueueArray);

                try {
                    JSONObject joinedQueues;
                    joinedQueues = retrievedQueueArray.getJSONObject(i);
                    //JSONArray joinedQueue = joinedQueues.getJSONArray("queue");
                    Log.e(TAG + " constructor_4.0", "joinedQueues->" + joinedQueues);

                    if (joinedQueues.has("queue")) {
                        String _id = joinedQueues.getString("_id");
                        //Log.e(TAG+" constructor_4.0.1", "_id->"+_id);
                        String qname = joinedQueues.getJSONObject("queue").getString("qName");
                        //Log.e(TAG+" constructor_4.0.1", "qName->"+qname);
                        int position = joinedQueues.getInt("possition");
                        //Log.e(TAG+" constructor_4.0.1", "position->"+position);
                        args_off.put("title", qname);
                        args_off.put("name", qname);
                        args_off.put("position", position);
                        args_off.put("state", "Offline..."); //TODO:Add string resource for translation
                        args_off.put("ID", joinedQ.getQueue().get_id());
                        qID = _id;
                    }

                } catch (JSONException e) {
                    Log.e(TAG + " constructor_4.1", "JSONException->" + e.toString() + "!");
                    e.printStackTrace();
                }
            }
        }

        //Initialising Fragment
        //rptFragment.setArguments(args_on);
        Log.e(TAG+" constructor_5"," Main_Act's savedQueueJoins->"+Main_Activity.savedQueueJoins);

        rptFragment.qInfo =joinedQ;
        return rptFragment;
    }

    public void loadQInfo(FloatingActionButton qPositionBtn, final String position) {
        Log.e(TAG+" loadQInfo", "Position=>" + position);

        int ps = Integer.valueOf(position)%5==0?4:Integer.valueOf(position)%5-1;

        qPositionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog(context, "", position);
            }
        });

        //RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) qPositionbtn.getLayoutParams();
        posFABs.get(ps).setAnimation(pulses.get(ps));

        pulses.get(ps).setRepeatCount(Animation.INFINITE);
        pulses.get(ps).start();

        for(int i=0; i<5; i++){
            if(i!=ps){
                //pulses.get(i).cancel();
                posFABs.get(i).setBackgroundTintList(ColorStateList.valueOf(getResources()
                        .getColor(R.color.ummo_4)));
                posFABs.get(i).setText("");
                posFABs.get(i).setSize(FloatingActionButton.SIZE_NORMAL);
            }
        }

        posFABs.get(ps).setText(position);
        posFABs.get(ps).setBackgroundTintList(ColorStateList.valueOf(getResources()
                .getColor(R.color.ummo_2)));
        posFABs.get(ps).setSize(FloatingActionButton.SIZE_NORMAL);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appDb = AppDatabase.getInMemoryDatabase(getApplicationContext());
        paused=false;

        if(savedInstanceState!=null) {
            assert getArguments() != null;
            qName = getArguments().getString("qname");
            qPosition = getArguments().getString("qposition");
            qAlphanum = getArguments().getString("qalphanum");
            qID =getArguments().getString("qID");

            Log.e(TAG+" onCreate", "name->"+qName+"; position->"+qPosition+"; state->"+qAlphanum);
        }
        networkToggle();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG+" onPause","RPTFrag paused!");
        paused=true;
    }

    @Override
    public void onResume() {
        super.onResume();
        paused=false;
        Log.e(TAG+" onResume","RPTFrag resumed!");
        /*qInfo.setqUpdate(new DataReadyCallback() {
            @Override
            public Object exec(final Object obj) {
                if (paused)
                    return null;

                assert getActivity() != null;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // setInfo((QueueJoinWrapper) obj);
                        Log.e(TAG + " onCreateView_1.1", "Queue update event (qInfo)->" + qInfo);
                        int ps = qInfo.getPosition() % 5 == 0 ? 4 : qInfo.getPosition() % 5 - 1;
                        FloatingActionButton bt = posFABs.get(ps);
                        loadQInfo(bt, "" + qInfo.getPosition());
                    }
                });
                return null;
            }
        });*/
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void networkToggle(){
        if(qman.getQman().isNetWorkStateConnected()){
            //Objects.requireNonNull(getView()).findViewById(R.id.offline).setVisibility(View.GONE);
            Log.e(TAG+" networkTog","Visibility View.GONE");
            //Log.e(TAG+" networkTog", "Main_Acts's Data->"+ Main_Activity.getJoinQueueData());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_rpt, container, false);
        context = view.getContext();
        this.view =view;

        //If connected, keep using fresh version of joined queue data
        //if (isConnected) {

            ButterKnife.bind(this, view);
            assert getArguments() != null;
            Log.e(TAG + " onCreateView_2", "Position->" + getArguments().getString("qposition"));

            title.setText(getArguments().getString("title"));
            alphaNum.setText("Queue-ID: ");
            numTV.setText(getArguments().getString("qalphanum"));
            HollyViewPagerBus.registerScrollView(getActivity(), scrollView);
            qPosition = getArguments().getString("qposition");
            String pos = qPosition;
            qName = getArguments().getString("qname");

            Log.e(TAG + " onCreateView_3", "PulseSize" + pulses.size());
            pulses.clear();

            for (int i = 0; i < 5; i++)
                pulses.add(AnimationUtils.loadAnimation(context, R.anim.pulse));

            int ps = Integer.valueOf(pos) % 5 == 0 ? 4 : Integer.valueOf(pos) % 5 - 1;

            TextFloatingActionButton man_Q_1 = view.findViewById(R.id.man_one);
            TextFloatingActionButton man_Q_2 = view.findViewById(R.id.man_two);
            TextFloatingActionButton man_Q_3 = view.findViewById(R.id.man_three);
            TextFloatingActionButton man_Q_4 = view.findViewById(R.id.man_four);
            TextFloatingActionButton man_Q_5 = view.findViewById(R.id.man_five);
            posFABs.clear();
            posFABs.add(man_Q_1);
            posFABs.add(man_Q_2);
            posFABs.add(man_Q_3);
            posFABs.add(man_Q_4);
            posFABs.add(man_Q_5);

            //Waiting for any queue update events
            qInfo.setqUpdate(new DataReadyCallback() {
                @Override
                public Object exec(final Object obj) {
                    if (paused)
                        return null;

                    assert getActivity() != null;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // setInfo((QueueJoinWrapper) obj);
                            Log.e(TAG + " onCreateView_1.1", "Queue update event (qInfo)->" + qInfo);
                            int ps = qInfo.getPosition() % 5 == 0 ? 4 : qInfo.getPosition() % 5 - 1;
                            FloatingActionButton bt = posFABs.get(ps);
                            loadQInfo(bt, "" + qInfo.getPosition());
                        }
                    });
                    return null;
                }
            });

            //Activating the Exit Queue Floating Action Button and listening for onClick events, then calling the ejectDialog func.
            exitFab = this.view.findViewById(R.id.exit_fab);
            exitFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG + " onCreateView_4", "Exit FAB Clicked->" + qID);
                    ejectDialog(getActivity());
                    //Mixpanel crumbs //TODO
                    /*try {
                        JSONObject user_lytics = new JSONObject();
                        user_lytics.put("ExitFAB", "onSelect");
                        mixpanel.track("RPTFragment", user_lytics);
                    } catch (JSONException e) {
                        Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                    }*/
                }
            });
            loadQInfo(posFABs.get(ps), pos);
        //}
        //If not connected, use saved copy of data
        /*else {
            Log.e(TAG+" onCreateView_5.0","isNotConnected!");
            qInfo.setqUpdate(new DataReadyCallback() {
                @Override
                public Object exec(final Object obj) {
                    if (paused)
                        return null;

                    assert getActivity() != null;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // setInfo((QueueJoinWrapper) obj);
                            Log.e(TAG + " onCreateView_5.1", "Queue update event (qInfo)->" + qInfo);
                            int position = 0;
                            try {
                                position = args_off.getInt("position") % 5 == 0 ? 4 : args_off.getInt("position") % 5 - 1;
                                FloatingActionButton bt = posFABs.get(position);
                                loadQInfo(bt, "" + position);
                            } catch (JSONException e) {
                                Log.e(TAG+" onCreateView_5.2","onDataReady JSE->"+e.toString());
                                e.printStackTrace();
                            }
                            //FloatingActionButton bt = posFABs.get(position);
                            //loadQInfo(bt, "" + position);
                        }
                    });
                    return null;
                }
            });

            ButterKnife.bind(this, view);

            assert getArguments() != null;
            try {
                Log.e(TAG + " onCreateView_6", "Position->" + args_off.getInt("position"));
                title.setText(args_off.getString("title"));
                alphaNum.setText(R.string.rpt_state_string);
                numTV.setText(args_off.getString("state"));
                qPosition = String.valueOf(args_off.getInt("position"));
                qName = args_off.getString("name");
            } catch (JSONException e) {
                Log.e(TAG+" onCreateView_7", "JSONException->"+e.toString());
                e.printStackTrace();
            }

            HollyViewPagerBus.registerScrollView(getActivity(), scrollView);

            String pos = qPosition;


            Log.e(TAG + " onCreateView_8.0", "PulseSize->" + pulses.size());
            pulses.clear();

            for (int i = 0; i < 5; i++)
                pulses.add(AnimationUtils.loadAnimation(context, R.anim.pulse));

            int ps = Integer.valueOf(pos) % 5 == 0 ? 4 : Integer.valueOf(pos) % 5 - 1;

            TextFloatingActionButton man_Q_1 = view.findViewById(R.id.man_one);
            TextFloatingActionButton man_Q_2 = view.findViewById(R.id.man_two);
            TextFloatingActionButton man_Q_3 = view.findViewById(R.id.man_three);
            TextFloatingActionButton man_Q_4 = view.findViewById(R.id.man_four);
            TextFloatingActionButton man_Q_5 = view.findViewById(R.id.man_five);
            posFABs.clear();
            posFABs.add(man_Q_1);
            posFABs.add(man_Q_2);
            posFABs.add(man_Q_3);
            posFABs.add(man_Q_4);
            posFABs.add(man_Q_5);

            exitFab = this.view.findViewById(R.id.exit_fab);
            exitFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG + " onCreateView_8.1", "Exit FAB Clicked->" + qID);
                    ejectDialog(getActivity());
                }
            });
            loadQInfo(posFABs.get(ps), pos);
        }*/

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void dialog(Context context, final String text, final String position) {
        final Context con = context;
        final SweetAlertDialog pDialog;
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(" "+text)
                .setContentText("Loading...");
        pDialog.show();
        pDialog.setCancelable(false);
        new CountDownTimer(800 * 7, 800) {
            public void onTick(long millisUntilFinished) {
                i++;
                switch (i) {
                    case 0:
                        pDialog.getProgressHelper().setBarColor(con.getResources()
                                .getColor(R.color.blue_btn_bg_color));
                        break;
                    case 1:
                        pDialog.getProgressHelper().setBarColor(con.getResources()
                                .getColor(R.color.material_deep_teal_50));
                        break;
                    case 2:
                        pDialog.getProgressHelper().setBarColor(con.getResources()
                                .getColor(R.color.success_stroke_color));
                        break;
                    case 3:
                        pDialog.getProgressHelper().setBarColor(con.getResources()
                                .getColor(R.color.material_deep_teal_20));
                        break;
                    case 4:
                        pDialog.getProgressHelper().setBarColor(con.getResources()
                                .getColor(R.color.material_blue_grey_80));
                        break;
                    case 5:
                        pDialog.getProgressHelper().setBarColor(con.getResources()
                                .getColor(R.color.warning_stroke_color));
                        break;
                    case 6:
                        pDialog.getProgressHelper().setBarColor(con.getResources()
                                .getColor(R.color.success_stroke_color));
                        break;
                }
            }

            public void onFinish() {
                i = -1;
                Log.e(TAG+" onFinish_1","QInfo->"+ qInfo.toString());
                final String projectToken = getString(R.string.mixpanelToken);
                mixpanel= MixpanelAPI.getInstance(getApplicationContext(), projectToken);

                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("State", "onPosCheck");
                    mixpanel.track("Position Checked", user_lytics);
                } catch (JSONException e) {
                    Log.e(TAG+" onFinish_2", "Unable to add properties to JSONObject->", e);
                }

                int ttdq=0,mins=0,qLength=0;
                mins= qInfo.getTtdq()* qInfo.getPosition();
                final int ttdq_mins = (qInfo.getTtdq()/(60*1000))* qInfo.getPosition();
                final int ttqd_secs = mins/(1000)-ttdq_mins*60;
                final int final_length_mins=position.contentEquals("1")?(qLength*mins)/(60*1000):((qLength-1)*mins)/(60*1000);
                final int final_length_secs=(qLength*mins)/(1000)-final_length_mins*60;
                final String phrase = position.contentEquals("1")?"Your Service Time is ":"Your Wait Time is ";
                final String post = qInfo.getTtdq()==0?"Not Available yet":ttdq_mins+" min";

                String j_id = qInfo.get_id();
                String u_id = qInfo.getUserCell();

                List queueJoin = appDb.queueJoinModel().listQueueJoin();
                Log.e(TAG+" onFinish","List<QueueJoin>"+queueJoin);

                int roomPosition = appDb.queueJoinModel().getQueueJoinPosition("5adf43c7393f690004bac29f","+26876157688");
                pDialog.setTitleText(" " + text + " Queue")
                        .setContentText("Your Position is: "+ roomPosition +
                                "\n" +phrase+final_length_mins+" min"/*final_length_secs+"sec"*/)
                        .setConfirmText("OK")
                        .showCancelButton(false)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                pDialog.dismiss();
                            }
                        });
            }
        }.start();
    }

    public void ejectDialog(Context context) {
        final SweetAlertDialog pDialog;
        final String projectToken = getString(R.string.mixpanelToken);

        final QueueJoin queueJoin = new QueueJoin();

        pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.exitingQueueString))
                .setContentText("Are you sure you want to leave this queue?");
        pDialog.show();
        pDialog.setCancelable(true);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
              pDialog.dismiss();
            }
        });

        pDialog.showCancelButton(true);
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
              Log.e(TAG+" ejectDialog","Confirmed");
                mixpanel= MixpanelAPI.getInstance(getApplicationContext(), projectToken);
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("State", "onExit");
                    mixpanel.track("Queue Exited", user_lytics);
                } catch (JSONException e) {
                    Log.e(TAG+" ejectDialog", "Unable to add properties to JSONObject", e);
                }

                pDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                //qman.getQman().exitQueue(qID);
                qInfo.exitQueue();

                QueueJoinWrapper.setOnExit(new DataReadyCallback() {
                    @Override
                    public Object exec(Object obj) {
                        pDialog.dismiss();
                        assert getActivity() != null;

                        String join_id = obj.toString();
                        appDb.queueJoinModel().deleteQueueJoinById(join_id);

                        Log.e(TAG + " onExit", "QueueJoin After Del->" + queueJoin.join_id);

                        getActivity().runOnUiThread(new Runnable() {
                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
                            @Override
                            public void run() {

                                Doorbell doorbell = new Doorbell(getActivity(), 7790, getString(R.string.doorbellPrivateKey));
                                doorbell.setTitle("Ummo Feedback")
                                        .setName(qman.getQman().getName())
                                        .addProperty("Contact", qman.getQman().getCellNumb())
                                        .addProperty("User-Name", qman.getQman().getUname())
                                        .setEmailFieldVisibility(View.VISIBLE)
                                        .setEmailHint("Email (optional)")
                                        .setMessageHint("Let us know what you think")
                                        .setNegativeButtonText("Maybe Later")
                                        .setOnFeedbackSentCallback(new io.doorbell.android.callbacks.OnFeedbackSentCallback() {
                                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                            @Override//TODO: Handle this Snackbar well
                                            public void handle(String name) {
                                                new Thread(new Runnable() {
                                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            //Show the user that we care about their feedback (for future convenience)
                                                            final ForegroundColorSpan feedbackSpan =
                                                                    new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.white));
                                                            SpannableStringBuilder snackbarText = new SpannableStringBuilder("Thank you for your feedback " + qman.getQman().getName() + ", it will help improve your service.");
                                                            snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                                                            Snackbar snackbar = Snackbar.make(Objects.requireNonNull(getView()).findViewById(R.id.rpt_scrollView), snackbarText, Snackbar.LENGTH_LONG);
                                                            snackbar.show();
                                                            //Stall the UI to allow user time to read message
                                                            Thread.sleep(4000);
                                                            //Exit view
                                                            getActivity().finish();
                                                        }
                                                        catch (InterruptedException ie){
                                                            Log.e(TAG+" FeedbackDialog","Interrupted->",ie);
                                                        }
                                                    }
                                                }).start();
                                            }
                                        });

                                doorbell.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        new Thread(new Runnable() {
                                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                            @Override
                                            public void run() {
                                                try {
                                                    //Show the user that we care about their feedback (for future convenience)
                                                    final ForegroundColorSpan feedbackSpan =
                                                            new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.white));
                                                    SpannableStringBuilder snackbarText = new SpannableStringBuilder("Thank you for using Ummo " + qman.getQman().getName() + ", feel free to leave us feedback.");
                                                    snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                                                    Snackbar snackbar = Snackbar.make(Objects.requireNonNull(getView()).findViewById(R.id.rpt_scrollView), snackbarText, Snackbar.LENGTH_LONG);
                                                    snackbar.show();
                                                    //Stall the UI to allow user time to read message
                                                    Thread.sleep(4000);
                                                    //Exit view
                                                    getActivity().finish();
                                                }
                                                catch (InterruptedException ie){
                                                    Log.e(TAG+" FeedbackDialog","Interrupted->",ie);
                                                }
                                            }
                                        }).start();
                                    }
                                });

                                doorbell.setPoweredByVisibility(View.GONE)
                                        .captureScreenshot()
                                        .setIcon(R.mipmap.ummo_new)
                                        .setCancelable(false)
                                        //.create()
                                        .show();
                            }
                        });
                        return null;
                    }
                });
            }
        });
    }

    /*@Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }*/
}