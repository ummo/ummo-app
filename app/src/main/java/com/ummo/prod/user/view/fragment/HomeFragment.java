package com.ummo.prod.user.view.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.appevents.AppEventsLogger;
import com.github.edsergeev.TextFloatingActionButton;
import com.github.fabtransitionactivity.SheetLayout;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.Main_Activity;
import com.ummo.prod.user.R;
import com.ummo.prod.user.db.room.AppDatabase;
import com.ummo.prod.user.db.room.entities.Queue;
import com.ummo.prod.user.db.room.entities.Service;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.interfaces.UmmoEventListener;
import com.ummo.prod.user.holder.IconTreeItemHolder;
import com.ummo.prod.user.holder.MaterialLetterIconTreeItemHolder;
import com.ummo.prod.user.holder.ProfileHolder;
import com.ummo.prod.user.holder.ProfileHolder_2;
import com.ummo.prod.user.holder.SelectableHeaderHolder_2;
import com.ummo.prod.user.holder.SelectableItemHolder;
import com.ummo.prod.user.view.BottomNavActivity;
import com.ummo.prod.user.ummoAPI.QueueJoinWrapper;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.doorbell.android.Doorbell;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements SheetLayout.OnFabAnimationEndListener{

    private SwipeRefreshLayout swipeRefreshLayout;
    public MixpanelAPI mixpanelAPI;

    @BindView(R.id.bottom_sheetlayout) SheetLayout mSheetLayout;
    @BindView(R.id.fab) TextFloatingActionButton fab;
    private RelativeLayout loader;
    public List<String> qServiceTypeList = null;
    private Bundle savedInstanceState;
    private boolean onPause =false;
    private boolean panelUp = false;
    private static final int REQUEST_CODE = 1;
    private static final String TAG = "HomeFragment";
    private String sharedPrefFile = "com.ummo.prod.user";
    private Object savedQueue;
    //private QueueJoinWrapper queueJoinWrapper = new QueueJoinWrapper();
    private List<QueueJoinWrapper> joinedQueues = new ArrayList<>();
    private Boolean isConnected = true; //This Bool will inform which app data to default to
    public BottomNavActivity bottomNavActivity = new BottomNavActivity();
    static JSONArray savedQueueDetails = new JSONArray();
    private AppDatabase appDb;

    //Intro View Elements
    private CardView introCard;
    private ImageView dismissCard;
    private boolean dismissed = false;
    private LinearLayout suggestionLayout;

    //Offline Data
    private String _serviceName, _serviceCategory, _serviceID;
    private JSONArray _serviceQueues;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(){
        return new HomeFragment();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appDb = AppDatabase.getInMemoryDatabase(getContext());
        loadSavedService();
        this.savedInstanceState=savedInstanceState;
        savedQueueDetails = bottomNavActivity.getSavedQueueDetails();
        Log.e(TAG+" onCreate", "savedQueueDetails->"+savedQueueDetails);

        /*
         * Checking for network conditions (Connected)
         */
        qman.getQman().registerEventListener("connect", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                Log.e(TAG+" setupUI","isConnected check - things look normal");
                final Activity activity = getActivity();
                if(activity==null)
                    return null;

                //TODO:possible null pointer bug here (investigate) -> bugs out when using Instant Debugger
                /*if (activity!=null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Objects.requireNonNull(getActivity())
                                    .findViewById(R.id.offline).setVisibility(View.GONE);
                            isConnected = true;
                        }
                    });
                }*/
                return null;
            }
        });

        /*
         * Checking for network conditions (Disconnected)
         */
        qman.getQman().registerEventListener("disconnected", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                Activity activity = getActivity();
                if(activity==null)
                    return null;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Objects.requireNonNull(getActivity())
                                .findViewById(R.id.offline).setVisibility(View.VISIBLE);
                        isConnected = false;
                    }
                });
                return null;
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        android.support.v7.widget.Toolbar toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.tool_bar);
        toolbar.setTitle(getString(R.string.home_fragment_title));
        loader = Objects.requireNonNull(getView()).findViewById(R.id.load_queues);

        introCard = Objects.requireNonNull(getView()).findViewById(R.id.queue_intro);
        dismissCard = getView().findViewById(R.id.dismissImageView);
        suggestionLayout = getView().findViewById(R.id.ummoSuggestionLayout);
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        dismissCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                introCard.setVisibility(View.GONE);
                dismissed = true;
                sharedPreferences.edit().putBoolean("UMMO_INTRO_DISMISSED", dismissed).apply();
                //Mixpanel crumbs
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("Intro Card", "onDismiss");
                    mixpanelAPI.track("Intro Card", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
            }
        });

        suggestionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Doorbell doorbell = new Doorbell(getActivity(), 8206, getString(R.string.doorbellSuggestions));
                doorbell.setTitle("Suggestions")
                        .setName(qman.getQman().getName())
                        .addProperty("Contact", qman.getQman().getCellNumb())
                        .addProperty("User-Name", qman.getQman().getUname())
                        .setEmailFieldVisibility(View.VISIBLE)
                        .setEmailHint("Email (optional)")
                        .setMessageHint("Where would you love to queue with Ummo?")
                        .setNegativeButtonText("Maybe Later")
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                View parentLayout = getView().findViewById(R.id.fragment_home);
                                final ForegroundColorSpan feedbackSpan =
                                        new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.white));
                                SpannableStringBuilder snackbarText = new SpannableStringBuilder("Feel free to suggest a service anytime...");
                                snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                                Snackbar snackbar = Snackbar.make(parentLayout, snackbarText, Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        });

                doorbell.setOnFeedbackSentCallback(new io.doorbell.android.callbacks.OnFeedbackSentCallback(){
                            @Override//TODO: Handle this Snackbar well
                            public void handle (String name){
                                View parentLayout = getView().findViewById(R.id.fragment_home);
                                final ForegroundColorSpan feedbackSpan =
                                        new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.white));
                                SpannableStringBuilder snackbarText = new SpannableStringBuilder("Thank you "+qman.getQman().getName()+", we'll be in touch");
                                snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                                Snackbar snackbar = Snackbar.make(parentLayout, snackbarText, Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        })
                        .setPoweredByVisibility(View.GONE)
                        .captureScreenshot()
                        .setIcon(R.mipmap.ummo_new)
                        .setCancelable(false)
                        //.create()
                        .show();
            }
        });

        Boolean dismissPreference = sharedPreferences.getBoolean("UMMO_INTRO_DISMISSED",dismissed);

        if (dismissPreference){
            introCard.setVisibility(View.GONE);
        }

        loadCategories();
        networkToggle();

        /*Boolean isConnected = qman.getSocket().connected();
        if (!isConnected) {
            View parentLayout = getView().findViewById(android.R.id.content);
            final ForegroundColorSpan redSpan =
                    new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.color_red));
            SpannableStringBuilder snackbarText = new SpannableStringBuilder("Connection lost...");
            snackbarText.setSpan(redSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            Snackbar snackbar = Snackbar.make(parentLayout, snackbarText, Snackbar.LENGTH_LONG);
            snackbar.show();
        }*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*dismissed = true;

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        sharedPreferences.edit().putBoolean("UMMO_INTRO_DISMISSED", dismissed).apply();*/
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void networkToggle(){
        if(qman.getQman().isNetWorkStateConnected()){
            Objects.requireNonNull(getView()).findViewById(R.id.offline).setVisibility(View.GONE);
            Log.e(TAG+" networkTog","Visibility View.GONE");
        }
    }

    private void loadSavedService() {
        List<Service> _service = appDb.serviceModel().loadAllServices();
        //JSONArray _savedServices = new JSONArray();

        for (int i = 0; i<_service.size(); i++) {
            _serviceName = _service.get(i).service_name;
            _serviceCategory = _service.get(i).service_cat;
            _serviceID = _service.get(i).service_id;
            _serviceQueues = new JSONArray(_service.get(i).service_queue);

            Log.e(TAG+" onLoad", "S_Name->"+_serviceName+
                    " S_Queues->"+_serviceQueues+
                    " S_Cat->"+_serviceCategory);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onResume() {
        super.onResume();

        //Facebook Events Logger
        AppEventsLogger.activateApp(getActivity());

        List<Service> _service = appDb.serviceModel().loadAllServices();
        JSONArray _savedServices = new JSONArray();
        Log.e(TAG+" onResume", "Service->"+_service);

        qman.getQman().setCurrentActivity(getActivity());
        qman.getQman().registerEventListener("connect", new UmmoEventListener() {
            @Override
            public Object run(Object data) {

                Log.e(TAG+" onResume", "Object Data->"+data);
                Activity activity = getActivity();
                if(activity==null)
                    return null;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Objects.requireNonNull(getActivity()).findViewById(R.id.offline).setVisibility(View.GONE);
                        isConnected = true;
                    }
                });

                return null;
            }
        });

        qman.getQman().registerEventListener("dequed", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                if(onPause ||panelUp)
                    return null ;

                Activity activity = getActivity();
                if(activity==null)
                    return null;

                //TODO:Check if everything works as expected here
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG+" onResume","Activity De-queued!");
                        //DialogFragment newFragment = new FeedbackDialogFragment();
                        //newFragment.show(getFragmentManager(), "feedback");
                        //panelUp=true;
                        //finish();
                        BottomNavActivity bottomNavActivity = new BottomNavActivity();
                        bottomNavActivity.confirmFeedback();
                    }
                });
                return null;
            }
        });

        qman.getQman().registerEventListener("disconnected", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                Activity activity = getActivity();
                if(activity==null)
                    return null;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Objects.requireNonNull(getActivity())
                                .findViewById(R.id.offline).setVisibility(View.VISIBLE);
                        isConnected = false;
                        //TODO: Reload all service data from Room
                        Log.e(TAG+" onResume", "Disconnected!!!");
                        //List<Service> roomServices =  appDb.serviceModel().loadAllServices();
                        //Log.e(TAG+ " off", "LiveData Services->"+roomServices.toString());

                        loadSavedService();
                    }
                });

                return null;
            }
        });

        qman.getQman().registerEventListener("categories", new UmmoEventListener() {

            @Override
            public Object run(final Object data) {
                Log.e(TAG+" onResume_1","Categories data->"+data.toString());

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONArray categories = new JSONArray(data.toString());
                            Log.e(TAG+" onResume_1","Async: categories->"+categories);
                            for (int i = 0; i<categories.length(); i++){
                                JSONObject category = categories.getJSONObject(i);
                                //JSONObject serviceProvider = category.getJSONObject("providers");
                                Log.e(TAG+ " onResume_1","Category("+i+")->"+category);
                                JSONArray serviceProviders = category.getJSONArray("providers");

                                for (int j = 0; j<serviceProviders.length(); j++) {
                                    JSONObject serviceProvider = serviceProviders.getJSONObject(j);

                                    ArrayList<String> q_ids = new ArrayList<>();
                                    String serviceName = serviceProvider.getString("name");
                                    String serviceId = serviceProvider.getString("_id");
                                    String serviceCat = serviceProvider.getString("category");
                                    JSONArray queues = serviceProvider.getJSONArray("queues");

                                    for (int m = 0; m<queues.length(); m++) {
                                        String q_id = queues.getJSONObject(m).getString("_id");
                                        q_ids.add(q_id);
                                    }

                                    Service service = new Service();
                                    service.service_id = serviceId;
                                    service.service_name = serviceName;
                                    service.service_cat = serviceCat;
                                    service.service_queue = q_ids;

                                    appDb.serviceModel().insertService(service);
                                    Log.e(TAG+" onResume", "Room-Service (Q_IDS)->"+service.service_queue);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG+ " JSE","Err... Error->"+e);
                        }
                    }
                });

                Activity activity = getActivity();
                if(activity==null)
                    return null;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(swipeRefreshLayout!=null)
                            swipeRefreshLayout.setRefreshing(false);
                        setupUi();
                    }
                });

                return null;
            }
        });

        onPause =false;

        setupUi();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void loadCategories(){
        try {
            JSONArray categories = qman.getQman().getCategories();

            JSONObject ob = categories==null?null:categories.getJSONObject(0);
            if(categories==null){
                qman.getQman().registerEventListener("categories", new UmmoEventListener() {
                    @Override
                    public Object run(Object data) {
                        JSONArray _data = (JSONArray)data;
                        //sp.edit().putString("categories", String.valueOf(_data)).apply();
                        //SharedPreferences sharedPreferences =
                        //        getActivity().getSharedPreferences(sharedPrefFile, Activity.MODE_PRIVATE);

                        //SharedPreferences.Editor editor = sharedPreferences.edit();
                        //editor.putString("SAVED_CATEGORIES", String.valueOf(_data));
                        //editor.apply();

                        Log.e(TAG+" loadCats", "Data stringValue->" + String.valueOf(_data));
                        Activity activity = getActivity();
                        if(activity==null)
                            return null;

                        Log.e(TAG+" loadCats","Data rawValue->"+_data.toString());
                        activity.runOnUiThread(new Runnable() {
                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public void run() {
                                setupUi();
                            }
                        });

                        return null;
                    }
                });
            }
            else {
                Log.e(TAG+" loadCats","Categories object->"+categories.toString());

                for (int i = 0; i<categories.length(); i++) {
                    JSONObject category = categories.getJSONObject(i);
                    Log.e(TAG+ " loadCats", "Category->"+category);
                }
                setupUi();
            }
        }
        catch (JSONException jse) {
            Log.e(TAG+" loadCats","CATEGORIES ERR ->"+jse.toString());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void setupUi(){
        final String projectToken = getString(R.string.mixpanelToken);
        mixpanelAPI= MixpanelAPI.getInstance(getActivity().getApplicationContext(), projectToken);
        loader = Objects.requireNonNull(getView()).findViewById(R.id.load_queues);

        if(qman.getQman().getCategories().length()==0){
            Log.e(TAG+" setupUI", "No Categories");
            loader.setVisibility(View.VISIBLE);
        } else {
            Log.e(TAG+ " setupUI", "Yes Categories->"+qman.getQman().getCategories());
            loader.setVisibility(View.GONE);
        }

        fab = Objects.requireNonNull(getView()).findViewById(R.id.fab);
        mSheetLayout = Objects.requireNonNull(getView()).findViewById(R.id.bottom_sheetlayout);
        //LayoutInflater inflater = LayoutInflater.from(this);
        //setSupportActionBar(toolbar);
        //final Toolbar tb = findViewById(R.id.tool_bar);
        //initNavigationDrawer();
        //minflater = getMenuInflater();
        //final CardView content = getView().findViewById(R.id.content);
        final ViewGroup containerView = Objects.requireNonNull(getView())
                .findViewById(R.id.container);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Ubuntu-C.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        ButterKnife.bind((Activity) Objects.requireNonNull(getContext())); //Questionable
        swipeRefreshLayout = Objects.requireNonNull(getView())
                .findViewById(R.id.swipeRefCategory);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                qman.getQman().refreshCategories();
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("State", "onRefresh");
                    //mixpanel.track("View Refreshed", user_lytics);
                } catch (JSONException e) {
                    Log.e(TAG+" Mixpanel", "Unable to add properties to JSONObject", e);
                }

                //Log.e(TAG+" setupUI","Done refreshing!");
            }

        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Mixpanel crumbs
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("FAB", "onSelect");
                    mixpanelAPI.track("Home Fragment", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
                mSheetLayout.expandFab();
            }
        });

        //Intent intent = getIntent();
        containerView.removeAllViews();

        //context = SelectableTreeFragment_dead.this;
        TreeNode root = TreeNode.root();

        if (isConnected){
            Log.e(TAG+" setupUI", "isConnected!");
        } else {
            //TODO: Reload all service data from sharedPrefs
            for (int n=0; n<savedQueueDetails.length(); n++){
                /*try {
                    serviceNameColl.add(savedQueueDetails.getJSONObject(n));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                Log.e(TAG+" setupUI", "isNotConnected!");
            }
        }

        //TODO: Check for network conditions below before defaulting to sharedPrefs
        try {
            ArrayList<TreeNode> treeNodeList = new ArrayList<>();
            ArrayList<TreeNode> treeNodeList2 = new ArrayList<>();
            final ArrayList<JSONObject> serviceNameColl = new ArrayList<>();
            JSONArray array= qman.getQman().getCategories();
            qServiceTypeList = new ArrayList<>();

            for (int i =0; i<array.length();i++) {
                JSONObject object = array.getJSONObject(i);
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
               // sp.edit().putString(getContext()).apply();
                String categoryName = object.getString("name");
                //Log.e(TAG+" "+categoryName,object.getJSONArray("providers").length()==0?"FALSE":"TRUE");
                qServiceTypeList.add(categoryName);
                if (categoryName.equals("Governmental")) {
                    treeNodeList.add(new TreeNode(new IconTreeItemHolder.IconTreeItem(categoryName,R.string.fa_globe))
                            .setViewHolder(object.getJSONArray("providers")
                                    .length()==0?new ProfileHolder_2(getActivity()):new ProfileHolder(getActivity())));
                }
                else if(categoryName.equals("Financial")) {
                    treeNodeList.add(new TreeNode(new IconTreeItemHolder.IconTreeItem(categoryName,R.string.fa_credit_card))
                            .setViewHolder(object.getJSONArray("providers")
                                    .length()==0?new ProfileHolder_2(getActivity()):new ProfileHolder(getActivity())));
                }
                else if(categoryName.equals("Entertainment")) {
                    treeNodeList.add(new TreeNode(new IconTreeItemHolder.IconTreeItem(categoryName,R.string.fa_music))
                            .setViewHolder(object.getJSONArray("providers")
                                    .length()==0?new ProfileHolder_2(getActivity()):new ProfileHolder(getActivity())));
                }
                else if(categoryName.equals("Medical")) {
                    treeNodeList.add(new TreeNode(new IconTreeItemHolder.IconTreeItem(categoryName,R.string.fa_stethoscope))
                            .setViewHolder(object.getJSONArray("providers")
                                    .length()==0?new ProfileHolder_2(getActivity()):new ProfileHolder(getActivity())));
                }
                else if(categoryName.equals("Miscellaneous")||categoryName.startsWith("B")) {
                    treeNodeList.add(new TreeNode(new IconTreeItemHolder
                            .IconTreeItem(categoryName,R.string.fa_sort_alpha_desc)).setViewHolder(object.getJSONArray("providers")
                            .length()==0?new ProfileHolder_2(getActivity()):new ProfileHolder(getActivity())));
                }
                else {
                    treeNodeList.add(new TreeNode(new IconTreeItemHolder.IconTreeItem(categoryName, R.string.ic_sd_storage))
                            .setViewHolder(object.getJSONArray("providers")
                                    .length()==0?new ProfileHolder_2(getActivity()):new ProfileHolder(getActivity())));
                }
                if (object.has("providers")) {
                    JSONArray providers = object.getJSONArray("providers");
                    for (int j = 0; j < providers.length(); j++) {
                        //Setting the Service Categories
                        final JSONObject providerObj = providers.getJSONObject(j);
                        JSONArray services= providerObj.has("queues")?providerObj
                                .getJSONArray("queues"):new JSONArray();

                        //Log.e(TAG+" setupUI","Service Queues (toString)->"+services.toString()+"");
                        if(services.length()!=0){
                            //Log.e(TAG+" setupUI","Service Providers (Object)->"+providerObj.toString());
                            TreeNode treeNode = new TreeNode(new MaterialLetterIconTreeItemHolder
                                    .IconTreeItem(providerObj.getString("name")))
                                    .setViewHolder(new SelectableHeaderHolder_2(getActivity()));
                            treeNodeList.get(i).addChildren(treeNode);
                            serviceNameColl.clear();
                            for (int k = 0; k < services.length(); k++) {
                                Log.e(TAG+" setupUI","Service Queue (final For Loop)->"
                                        +services.getJSONObject(k).toString());
                                serviceNameColl.add(services.getJSONObject(k));
                            }
                            customFillFolder(treeNode, serviceNameColl);
                        }
                    }
                }
                serviceNameColl.clear();
                treeNodeList2.clear();
            }
            //Log.e(TAG+" setupUI","JSONArray->"+array.toString());
            root.addChildren(treeNodeList);
        }
        catch (JSONException jse) {
            Log.e(TAG+" JSON Error",jse.toString());
        }

        AndroidTreeView tView = new AndroidTreeView(getContext(), root);
        tView.setDefaultAnimation(true);
        containerView.addView(tView.getView());

        if (savedInstanceState != null) {
            String state = savedInstanceState.getString("tState");
            if (!TextUtils.isEmpty(state)) {
                tView.restoreState(state);
            }
        }
        tView.setSelectionModeEnabled(true);

        final Animation growAnimation = AnimationUtils.loadAnimation(getContext(),R.anim.simple_grow);
        final Animation shrinkAnimation = AnimationUtils.loadAnimation(getContext(),R.anim.simple_shrink);

        Display mdisp = Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay();
        Point mdispSize = new Point();
        mdisp.getSize(mdispSize);
        int maxX = mdispSize.x;
        int maxY = mdispSize.y;
        fab.setVisibility(View.VISIBLE);
        fab.startAnimation(growAnimation);

        /*for (int i=0; i<=qman.getQman().joinedQs.size(); i++) {
            JSONObject joinedQ = qman.getQman().joinedQs.get(i);
            Log.e(TAG+" setupUI", "JoinedQ->"+joinedQ);
        }*/
        joinedQueues=QueueJoinWrapper.qmanQJoins;

        //
        if (!joinedQueues.isEmpty()){

            SharedPreferences sharedPreferences = getActivity()
                    .getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
            String savedQueueJoins = sharedPreferences.getString("SAVED_JOINED_QUEUES","");
            Log.e(TAG+" setupUI","savedQueueJoins->"+savedQueueJoins);

            try {
                JSONArray queueArray = new JSONArray(savedQueueJoins);
                Log.e(TAG+" setupUI", "QueueArray->"+queueArray);

                for (int m=0; m<queueArray.length(); m++){
                    JSONObject queueObject = queueArray.getJSONObject(m);
                    int position = /*queueJoinWrapper.getPosition();*/queueObject.getInt("possition");

                    final Animation pulseAnimation = AnimationUtils
                            .loadAnimation(getContext(), R.anim.pulse);
                    pulseAnimation.setRepeatCount(Animation.INFINITE);
                    fab.startAnimation(pulseAnimation);
                    fab.setText("#"+position);
                    fab.setSize(FloatingActionButton.SIZE_NORMAL);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            fab.setImageResource(R.drawable.ic_queue);
        }

        mSheetLayout.setFab(fab);
        mSheetLayout.setPivotX(maxX);
        mSheetLayout.setPivotY(-maxY);

        mSheetLayout.setFabAnimationEndListener(this);

        shrinkAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                fab.startAnimation(shrinkAnimation);
            }
            @Override
            public void onAnimationEnd(Animation animation)
            {
                fab.setVisibility(View.GONE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void customFillFolder(TreeNode folder, ArrayList<JSONObject> list) {
        //List<String> qJoinedList = null;
        try {
            for (int i = 0; i < list.size(); i++) {
                folder.addChildren(new TreeNode(list.get(i).getString("qName"))
                        .setViewHolder(new SelectableItemHolder(getActivity(), list.get(i), "serviceName")));
                String q_name = list.get(i).getString("qName");
                String q_loc = list.get(i).getString("location");
                String q_req = list.get(i).getString("qRequirements");
                String q_serv = list.get(i).getString("qService");
                String _id = list.get(i).getString("_id");
                int length = 1;
                int ttdq = list.get(i).getInt("ttdq");

                final Queue queue = new Queue();
                queue.queue_name = q_name;
                queue.service_id = q_serv;
                queue.queue_location = q_loc;
                queue.queue_id = _id;
                queue.queue_req = q_req;
                queue.queue_ttdq = ttdq;
                queue.queue_length = length;


                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //TODO:Figure out how this can work without breaking the app
                        //FK constraint failed
                       // appDb.queueModel().insertQueue(queue);
                    }
                });

                Log.e(TAG+ " customFill", "Room-Queue ("+i+")->"+queue.queue_name);
            }
            Log.e(TAG+ " customFill", "List used ->"+list);
        }
        catch(JSONException jse) {
            Log.e(TAG+" customFill","Json Error Passing Qs"+jse.toString());
        }
    }

    @Override
    public void onFabAnimationEnd() {
        Intent intent = new Intent(getActivity(), Main_Activity.class);
        //intent.putExtra("FAB","bk_id");
        startActivityForResult(intent, REQUEST_CODE);
    }
}
