package com.ummo.prod.user;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.github.florent37.hollyviewpager.HollyViewPager;
import com.github.florent37.hollyviewpager.HollyViewPagerConfigurator;
//import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ummo.prod.user.interfaces.UmmoEventListener;
import com.ummo.prod.user.view.BottomNavActivity;
import com.ummo.prod.user.view.fragment.BaseActivity;
import com.ummo.prod.user.view.fragment.FeedbackDialogFragment;
import com.ummo.prod.user.view.fragment.RPTFragment;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;
import com.ummo.prod.user.ummoAPI.JoinedQ;
import com.ummo.prod.user.ummoAPI.QUser;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.ummoAPI.QueueJoinWrapper;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//import butterknife.Bind;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.doorbell.android.Doorbell;

import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

/**
 * Created by barnes on 12/30/15.
 **/

//TODO: Describe what this activity does

public class Main_Activity extends BaseActivity implements FeedbackDialogFragment.FeedbackDialogListener {
    private boolean onPause =false;
    private boolean panelUp = false;
    private String TAG = "Main_Activity";
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.hollyViewPager) HollyViewPager hollyViewPager; //That fancy thing Bane used for the RPT screen (unreliable repo*)
    @BindView(R.id.rpt_refresh) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.empty_qlist) TextView emptyQList;
    @BindView(R.id.addQueue) ImageView im;
    FragmentPagerAdapter adapter;

    public MixpanelAPI mixpanelAPI;
    private List<QueueJoinWrapper> joinedQueues = new ArrayList<>();
    private String sharedPrefFile = "com.ummo.prod.user";
    public static String savedQueueJoins;
    private BottomNavActivity bottomNavActivity = new BottomNavActivity();

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialogFragment) {
        Toast.makeText(Main_Activity.this,
                "Your feedback helps us improve your experience...", Toast.LENGTH_LONG).show();
        panelUp=false;
        QueueJoinWrapper.loadData();
        finish();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialogFragment) {
        Log.e(TAG+" onDialogP","FEEDBACK"+dialogFragment.toString());
        panelUp=false;
        QueueJoinWrapper.loadData();
        Toast.makeText(Main_Activity.this,
                "Thank you for your feedback...", Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    protected void exitToBottomAnimation() {
        super.exitToBottomAnimation();
    }

    @Override
    protected void onPause() {
        //exitToBottomAnimation();
        onPause =true;
        qman.getQman().deRegisterUmmoEvent("dequed");
        qman.getQman().deRegisterUmmoEvent("joinedQs");
        //MixpanelAPI.flush();
        Log.e(TAG+" onPause","RPTHolder paused!");

        Main_Activity.this.finish();
        super.onPause();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        toolbar.setBackgroundColor(getResources().getColor(R.color.ummo_3));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        //Subscribing user to onQueue topic for FCM
        FirebaseMessaging.getInstance().subscribeToTopic("ON_QUEUE");
        /*Fix orientation to portrait view*/
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //setView();//Checking
        //TODO: Look up usages of HollyViewPager and get to the bottom of this math below
        hollyViewPager.getViewPager().setPageMargin(getResources().getDimensionPixelOffset(R.dimen.viewpager_margin));
        hollyViewPager.setConfigurator(new HollyViewPagerConfigurator() {
            @Override
            public float getHeightPercentForPage(int page) {
                return ((page + 4) % 10) / 10f;
            }
        });

        String dequeue_id=getIntent().getStringExtra("DEQUEUE_ID");
        if (dequeue_id!=null) {
            getFeedback();
            Log.e(TAG+" onCreate", "Dequeued->"+dequeue_id);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        savedQueueJoins = sharedPreferences.getString("SAVED_JOINED_QUEUES","");
        Log.e(TAG+" onCreate","savedQueueJoins->"+savedQueueJoins);
    }

    /**
     *Below function initializes the RPT-adapter and checks if there are any joined queues;
     * if there aren't any currently joined, sets the view to the 'blank-view'
     **/
    public void setView(){
        initAdapter();

        if (joinedQueues.isEmpty()) {
            emptyQList.setVisibility(View.VISIBLE);
            im.setVisibility(View.VISIBLE);
            QueueJoinWrapper.loadData();
        }
    }

    /**
     *The below adapter initializer allows the user to swipe through "pages"
     * of content that is usually represented as different fragments;
     * We use it to inform HollyViewPager of the data-set it must contain
     **/
    public void initAdapter(){
        adapter=null;//Why do we have to init to 'null'?
        //hollyViewPager.removeAllViews();
        adapter = new FragmentPagerAdapter(getSupportFragmentManager()){
            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                super.destroyItem(container, position, object);
            }

            @Override
            public int getItemPosition(@NonNull Object object) {
                return super.getItemPosition(object);
            }

            @Override
            public Fragment getItem(int position) {
                //This is where the position persistence will be implemented ideally...
                return RPTFragment.newInstance(joinedQueues.get(position));
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                Log.e(TAG+" notifyDataSet","Changed!");
                //TODO: Check here for RPT position bug workaround

            }

            /**
             * Counts the number of queues joined;
             * uses this to create the number of pages to navigate through
             **/
            @Override
            public int getCount() {
                return joinedQueues.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                Log.e(TAG+" getPageTitle"," Position->"+joinedQueues.size()+" pos->"+position);//Position is accurate here
                return joinedQueues.get(position).getQueue().getqName();
            }
        };
        //The order below may be up for tweaking depending on the 'pos.-change' bug
        adapter.notifyDataSetChanged();
        hollyViewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_refresh:
                onRefresh();
                final String projectToken = getString(R.string.mixpanelToken);
                mixpanelAPI= MixpanelAPI.getInstance(getApplicationContext(), projectToken);
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("Refresh", "onRefresh");
                    mixpanelAPI.track("RPT_Main_Activity", user_lytics);
                } catch (JSONException e) {
                    Log.e(TAG+" Mixpanel", "Unable to add properties to JSONObject", e);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onRefresh() {
        QueueJoinWrapper.loadData();
        //initAdapter();

        Log.e(TAG+" onOptions", "Refreshing...");

        QueueJoinWrapper.setDataReadyCallback(new DataReadyCallback() {
            @Override
            public Object exec(final Object obj) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(false);
                        Log.e(TAG+" onRefresh_1","Refreshed UI Thread");
                        if(joinedQueues.isEmpty()){
                            emptyQList.setVisibility(View.VISIBLE);
                            im.setVisibility(View.VISIBLE);
                            hollyViewPager.setVisibility(View.GONE);
                            Log.e(TAG+" onRefresh_2", "QJW-setDataReady: No Joined Qs");

                            View parentLayout = findViewById(android.R.id.content);

                            final ForegroundColorSpan redSpan =
                                    new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.warning_stroke_color));
                            SpannableStringBuilder snackbarWarning = new SpannableStringBuilder("No Queues found...");
                            snackbarWarning.setSpan(redSpan, 0, snackbarWarning.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                            Snackbar snackbar = Snackbar.make(parentLayout, snackbarWarning, Snackbar.LENGTH_LONG)
                                    .setAction("Take me home", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            startActivity(new Intent(Main_Activity.this, BottomNavActivity.class));
                                        }
                                    })
                                    .setActionTextColor(getResources().getColor(R.color.ummo_1));
                            snackbar.show();

                        }else {
                            joinedQueues=QueueJoinWrapper.qmanQJoins;
                            adapter.notifyDataSetChanged();
                            //hollyViewPager.setAdapter(adapter);
                            //initAdapter();
                            hollyViewPager.setVisibility(View.VISIBLE);
                            emptyQList.setVisibility(View.GONE);
                            im.setVisibility(View.GONE);
                            Log.e(TAG+" onRefresh_3.0", "QJW-setDataReady:Saved Joined Qs->"+savedQueueJoins);
                            Log.e(TAG+" onRefresh_3.1", "QJW-setDataReady:Orig. Joined Qs->"+joinedQueues);

                            View parentLayout = findViewById(android.R.id.content);
                            final ForegroundColorSpan redSpan =
                                    new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.ummo));
                            SpannableStringBuilder snackbarText = new SpannableStringBuilder("Checking for updates...");
                            snackbarText.setSpan(redSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                            Snackbar.make(parentLayout,snackbarText, Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
                return null;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Re-setting the Refresh loader
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                QueueJoinWrapper.loadData();
                initAdapter();
                View parentLayout = findViewById(android.R.id.content);
                final ForegroundColorSpan redSpan =
                        new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.ummo));
                SpannableStringBuilder snackbarText = new SpannableStringBuilder("Checking for updates...");
                snackbarText.setSpan(redSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                Snackbar snackbar = Snackbar.make(parentLayout,snackbarText, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });

        String dequeue_id=getIntent().getStringExtra("DEQUEUE_ID");
        if (dequeue_id!=null) {
            getFeedback();
            Log.e(TAG+" onCreate", "Dequeued->"+dequeue_id);
        }

        Log.e(TAG+" onResume","RPTHolder resumed!");
        initAdapter();
        //String bookingDoneId=getIntent().getStringExtra("FAB");
        //if (bookingDoneId!=null)getFeedback();
        //QueueJoinWrapper.loadData();

        onPause =false;
        qman.getQman().setCurrentActivity(this);

         joinedQueues=QueueJoinWrapper.qmanQJoins;
        //Log.e(TAG+" onResume", "mMessageReceiver->"+mMessageReceiver);
        //adapter.notifyDataSetChanged();
        setView(); //Initialising the RPT view

        QueueJoinWrapper.setOnDequeue(new DataReadyCallback() {
            @Override
            public Object exec(Object obj) {
                if (onPause)
                    return null;
                adapter.notifyDataSetChanged();
                getFeedback();
                //setView();
                Log.e(TAG+" onResume_1", "QJW-setOnDequeue");
                return null;
            }
        });

        qman.getQman().registerEventListener("joinedQs", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                if(onPause||panelUp)
                    return null ;
                Log.e(TAG+" onResume_2", "Qman-joinedQs->"+data);
                return null;
            }
        });

        QueueJoinWrapper.setDataReadyCallback(new DataReadyCallback() {
            @Override
            public Object exec(final Object obj) {
                //refreshLayout.setRefreshing(false);
                //Log.e(TAG+" onResume","QJW-setDataReady->"+obj.toString());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(false);
                        Log.e(TAG+" onResume_3","Refreshed UI Thread");
                        if(joinedQueues.isEmpty()){
                            emptyQList.setVisibility(View.VISIBLE);
                            im.setVisibility(View.VISIBLE);
                            hollyViewPager.setVisibility(View.GONE);
                            Log.e(TAG+" onResume_4.0", "QJW-setDataReady: No Joined Qs");
                        }else {
                            joinedQueues=QueueJoinWrapper.qmanQJoins;
                            adapter.notifyDataSetChanged();
                            //hollyViewPager.setAdapter(adapter);
                            //initAdapter();
                            hollyViewPager.setVisibility(View.VISIBLE);
                            emptyQList.setVisibility(View.GONE);
                            im.setVisibility(View.GONE);
                            //Log.e(TAG+" onResume_4.1.0", "QJW-setDataReady:Saved Joined Qs->"+savedQueueJoins);
                            //Log.e(TAG+" onResume_4.1.1", "QJW-setDataReady:Orig. Joined Qs->"+joinedQueues);
                        }
                    }
                });
                return null;
            }
        });
    }

    public void getFeedback(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Doorbell doorbell = new Doorbell(Main_Activity.this, 8174, getString(R.string.doorbellServicesKey));
                doorbell.setTitle("Service Feedback")
                        .setName(qman.getQman().getName())
                        .addProperty("Contact", qman.getQman().getCellNumb())
                        .addProperty("User-Name", qman.getQman().getUname())
                        .setEmailFieldVisibility(View.VISIBLE)
                        .setEmailHint("Email (optional)")
                        .setMessageHint("How was your service?")
                        .setNegativeButtonText("Maybe Later")
                        .setOnFeedbackSentCallback(new io.doorbell.android.callbacks.OnFeedbackSentCallback(){
                            @Override//TODO: Handle this Snackbar well
                            public void handle (String name){
                                View parentLayout = findViewById(android.R.id.content);
                                final ForegroundColorSpan feedbackSpan =
                                        new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.ummo_3));
                                SpannableStringBuilder snackbarText = new SpannableStringBuilder("Thank you "+qman.getQman().getName()+", we'll be in touch");
                                snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                                Snackbar snackbar = Snackbar.make(parentLayout, snackbarText, Snackbar.LENGTH_LONG);
                                snackbar.show();
                                finish();
                            }
                        })
                        .setPoweredByVisibility(View.GONE)
                        .captureScreenshot()
                        .setIcon(R.mipmap.ummo_new)
                        .setCancelable(false)
                        .show();
            }
        });
        /*final AlertDialog.Builder builder = new AlertDialog.Builder(Main_Activity.this);
        LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        builder.setTitle("We would appreciate your feedback");
        Log.e(TAG+" getFeedback","FEEDBACK STARTING");
        assert inflater != null;
        final View view = inflater.inflate(R.layout.feedback_custom, null);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                builder.setView(view)
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        })
                        .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .create().show();
            }
        });*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_qs, menu);
        return super.onCreateOptionsMenu(menu);
    }
}