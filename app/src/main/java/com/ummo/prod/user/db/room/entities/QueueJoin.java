package com.ummo.prod.user.db.room.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity (indices = {@Index("position"),
                    @Index("user_join")}/*,
        foreignKeys = {@ForeignKey(entity = Queue.class,
                                    parentColumns = "queue_id",
                                    childColumns = "queue_id"),
                        @ForeignKey(entity = User.class,
                                    parentColumns = "user_contact",
                                    childColumns = "user_join")}*/)
public class QueueJoin {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "join_id")
    public String join_id;

    @ColumnInfo(name = "queue_id")
    public String queue_id; //TODO:FK

    @ColumnInfo(name = "user_join")
    public String user_join; //TODO:FK

    @ColumnInfo(name = "position")
    public int position;
}