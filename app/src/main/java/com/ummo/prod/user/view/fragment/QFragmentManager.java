package com.ummo.prod.user.view.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ummo.prod.user.ummoAPI.JoinedQ;

import java.util.List;

/**
 * Created by barnes on 10/12/15.
 **/
public class QFragmentManager extends FragmentPagerAdapter {
    private int frag_count;
    private List<JoinedQ> list_;
    private int tPosition;

    //So I had to change the Constructor too
    public QFragmentManager(FragmentManager fm, List<JoinedQ> list, int tabPosition) {
        super(fm);
        list_ = list;
        frag_count = list.size();
        tPosition = tabPosition;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        return list_.get(position).getqName();
    }

    @Override
    public Fragment getItem(int position) {
        return QFragment.newInstance(position + 1, tPosition, list_.get(position));
    }

    @Override
    public int getCount()
    {
        return frag_count;
    }
}