package com.ummo.prod.user.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.danielstone.materialaboutlibrary.MaterialAboutActivity;
import com.danielstone.materialaboutlibrary.model.MaterialAboutActionItem;
import com.danielstone.materialaboutlibrary.model.MaterialAboutCard;
import com.danielstone.materialaboutlibrary.model.MaterialAboutList;
import com.danielstone.materialaboutlibrary.model.MaterialAboutTitleItem;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by barnes on 1/17/17.
 **/

public class AboutActivity extends MaterialAboutActivity {
    public MixpanelAPI mixpanelAPI;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }
    //String legal = "Ummo's <a href='http://privacy_policy.ummo.xyz/UmmoTermsofUse.pdf'>Terms of Use</a> and <a href='http://privacy_policy.ummo.xyz/'> Privacy Policy </a></div>";
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected MaterialAboutList getMaterialAboutList() {
        final String projectToken = getString(R.string.mixpanelToken);
        mixpanelAPI = MixpanelAPI.getInstance(this, projectToken);

        MaterialAboutCard.Builder appCardBuilder = new MaterialAboutCard.Builder();

        // Add items to card
        appCardBuilder.addItem(new MaterialAboutTitleItem.Builder()
                .text("Ummo")
                .icon(R.mipmap.ummo_new)
                .build());
        appCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("Version")
                //.subText(BuildConfig.VERSION_NAME)
                .subText("1.4.0")
                .icon(R.drawable.ic_about_version)
                .setOnClickListener(new MaterialAboutActionItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        Log.i("test", "onClick: Version Tapped");
                        //Toast.makeText(AboutActivity.this, "Version Tapped", Toast.LENGTH_SHORT).show();
                    }
                })
                .build());
        appCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("Made eSwatini")
                .subText("Mbabane")
                .icon(R.drawable.ic_about_location)
                .build());
        /*appCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("Changelog")
                .icon(R.drawable.ic_about_changelog)
                .setOnClickListener(new MaterialAboutActionItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        Toast.makeText(AboutActivity.this, "Changelog coming soon...", Toast.LENGTH_SHORT).show();
                    }
                })
                .build());*/
        /*appCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("Licenses")
                .subText("Copyright 2017, Ummo Inc.")
                .icon(R.drawable.ic_about_licenses)
                .setOnClickListener(new MaterialAboutActionItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        Toast.makeText(AboutActivity.this, "Licenses coming soon...", Toast.LENGTH_SHORT).show();
                    }
                })
                .build());*/

        MaterialAboutCard.Builder authorCardBuilder = new MaterialAboutCard.Builder();
        authorCardBuilder.title("What is Ummo?");

        authorCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("Ummo is your Service Assistant")
                .subText("At Ummo, we believe that your time is everything!\n\nThis is why the Ummo app has been designed to give you more control of your time when accessing your service.")
                .icon(R.drawable.ic_action_help)
                .build());

        MaterialAboutCard.Builder supportCardBuilder = new MaterialAboutCard.Builder();
        supportCardBuilder.title("Get in touch");
        /*supportCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("Report Bugs")
                .subText("Report bugs or request new features.")
                .icon(R.drawable.ic_about_bug)
                .setOnClickListener(new MaterialAboutActionItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        Toast.makeText(AboutActivity.this, "Bug report coming soon...", Toast.LENGTH_SHORT).show();
                    }
                })
                .build());*/
        supportCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("Help & Support")
                .subText("Email: ask@ummo.xyz \nCall:  (+268) 7615 7688")
                .icon(R.drawable.ic_about_contact_us)
                .setOnClickListener(new MaterialAboutActionItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        try {
                            JSONObject user_lytics = new JSONObject();
                            user_lytics.put("Help & Support", "onSelect");
                            mixpanelAPI.track("About Screen", user_lytics);
                        } catch (JSONException e) {
                            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                        }
                    }
                })
                .build());

        MaterialAboutCard.Builder legalCard = new MaterialAboutCard.Builder();
        legalCard.title("Legal");
        legalCard.addItem(new MaterialAboutActionItem.Builder()
                .text("Terms of Use")
                .icon(R.drawable.ic_about_legal)
                .setOnClickListener(new MaterialAboutActionItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        try {
                            JSONObject user_lytics = new JSONObject();
                            user_lytics.put("Terms of Use", "onSelect");
                            mixpanelAPI.track("About Screen", user_lytics);
                        } catch (JSONException e) {
                            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                        }
                    }
                })
                .build());

        legalCard.addItem(new MaterialAboutActionItem.Builder()
                .text("Privacy Policy")
                .subText("\u00a9 2015-2018 Ummo Inc.\n   All rights reserved.")
                .icon(R.drawable.ic_about_privacy_policy)
                .setOnClickListener(new MaterialAboutActionItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        try {
                            JSONObject user_lytics = new JSONObject();
                            user_lytics.put("Privacy Policy", "onSelect");
                            mixpanelAPI.track("About Screen", user_lytics);
                        } catch (JSONException e) {
                            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                        }
                        Intent privacyPolicy = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("'http://privacy_policy.ummo.xyz"));
                        startActivity(privacyPolicy);
                    }
                })
                .build());

        return new MaterialAboutList(appCardBuilder.build(), authorCardBuilder.build(), supportCardBuilder.build() , legalCard.build());
    }

    @Override
    protected void onDestroy() {
        mixpanelAPI.flush();
        super.onDestroy();
    }

    @Override
    protected CharSequence getActivityTitle() {
        return getString(R.string.mal_title_about);
    }
}