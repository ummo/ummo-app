package com.ummo.prod.user.view;

/**
 * Created by José on 23/02/17
 */

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.klinker.android.peekview.PeekViewActivity;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.R;
import com.ummo.prod.user.RVItemDecoration;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.adapter.BookingTreeAdapter;
import com.ummo.prod.user.view.fragment.FeedbackDialogFragment;
import com.ummo.prod.user.ummoAPI.AgentServiceWrapper;
import com.ummo.prod.user.ummoAPI.BookingWraper;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;
import com.wooplr.spotlight.prefs.PreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static com.ummo.prod.user.R.id.nav_drawer_feedback;
import static com.ummo.prod.user.R.id.nav_drawer_intro;
import static com.ummo.prod.user.R.string.nav_about;

public class BookingsActivity extends PeekViewActivity implements FeedbackDialogFragment.FeedbackDialogListener{

    public MixpanelAPI mixpanelAPI;
    private BookingTreeAdapter adapter;
    private ArrayList listData;
    private ArrayList bookeeData;

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private SwipeRefreshLayout swipeRefreshLayout;
    int[] mMaterialColors;
    Random RANDOM = new Random();
    MaterialLetterIcon profile;
    private static final String INTRO_CARD = "fab_intro";
    private static final String INTRO_SWITCH = "Select a service to view available queues";
    private static final String INTRO_CATEGORY = "Select";
    private static final String INTRO_FEEDBACK = "Select a Feedback";
    PreferencesManager mPreferencesManager;
    private static final String BUNDLE_EXTRAS = "BUNDLE EXTRAS";
    private static final String EXTRA_DETAIL = "EXTRA_DETAIL";
    private static final String EXTRA_ATTR = "EXTRA ATTR";
    private ArrayList bookingsData = new ArrayList<>();
    private Toolbar toolbar;
    ActionBarDrawerToggle mDrawerToggle;

    @Override
    public void onDialogPositiveClick(DialogFragment dialogFragment) {
        Log.e("FEEDBACK",dialogFragment.toString());

        final String projectToken = getString(R.string.mixpanelToken);
        mixpanelAPI = MixpanelAPI.getInstance(this, projectToken);
        try {
            JSONObject user_lytics = new JSONObject();
            user_lytics.put("Feedback", "onConfirmFeedback");
            mixpanelAPI.track("Bookings Screen", user_lytics);
        } catch (JSONException e) {
            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
        }

        Toast.makeText(this, "Thank you for your feedback...", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialogFragment) {
        final String projectToken = getString(R.string.mixpanelToken);
        mixpanelAPI = MixpanelAPI.getInstance(this, projectToken);
        try {
            JSONObject user_lytics = new JSONObject();
            user_lytics.put("Feedback", "onCancelFeedback");
            mixpanelAPI.track("Bookings Screen", user_lytics);
        } catch (JSONException e) {
            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
        }

        Toast.makeText(this, "Your feedback helps us improve your experience...", Toast.LENGTH_LONG).show();

    }

    private void initNavigationDrawer() {

        mDrawerLayout = findViewById(R.id.DrawerLayout);
        mNavigationView = findViewById(R.id.nav_view);
        View headerLayout = mNavigationView.getHeaderView(0);
        mMaterialColors = getResources().getIntArray(R.array.colors);
        profile = headerLayout.findViewById(R.id.circleView);
        TextView userName = headerLayout.findViewById(R.id.name);
        TextView cellNumber = headerLayout.findViewById(R.id.email);

        String NAME = qman.getQman().getName();
        String CELL = qman.getQman().getCellNumb();
        String SURNAME = qman.getQman().getSurname();

        userName.setText(NAME+" "+SURNAME);
        cellNumber.setText(CELL);

        profile.setShapeColor(mMaterialColors[RANDOM.nextInt(mMaterialColors.length)]);
        profile.setInitials(true);
        profile.setInitialsNumber(2);
        profile.setLetterSize(18);
        profile.setLetter(NAME+SURNAME);


        setupActionBarDrawerToggle();
        if (mNavigationView != null) {
            setupDrawerContent(mNavigationView);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BookingWraper.initMyBookings();
    }

    private void setupActionBarDrawerToggle() {

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                //Snackbar.make(view, R.string.drawer_close, Snackbar.LENGTH_SHORT).show();
            }

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView)
            {
                //Snackbar.make(drawerView, R.string.drawer_open, Snackbar.LENGTH_SHORT).show();
             //   mPreferencesManager.resetAll();

            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    private void setupDrawerContent(NavigationView navigationView) {

        addItemsRunTime(navigationView);
    }

    private void addItemsRunTime(NavigationView mNavigationView) {
        // refreshing navigation drawer adapter
        for (int i = 0, count = mNavigationView.getChildCount(); i < count; i++) {
            final View child = mNavigationView.getChildAt(i);
            if (child != null && child instanceof ListView) {
                final ListView menuView = (ListView) child;
                final HeaderViewListAdapter adapter = (HeaderViewListAdapter) menuView.getAdapter();
                final BaseAdapter wrapped = (BaseAdapter) adapter.getWrappedAdapter();
                wrapped.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookings);
        setTitle("Ummo");

        final String projectToken = getString(R.string.mixpanelToken);

        swipeRefreshLayout = findViewById(R.id.swipeRefBooking);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                AgentServiceWrapper.fetchAgentServices();
                mixpanelAPI = MixpanelAPI.getInstance(getApplicationContext(), projectToken);
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("State", "onRefresh");
                    mixpanelAPI.track("Booking View Refreshed", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }

                Log.e("REFRESH","Done refreshing");
            }

        });

        mixpanelAPI = MixpanelAPI.getInstance(this, projectToken);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setSubtitle("Tap on the menu for settings");
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("FAB", "onSelect");
                    mixpanelAPI.track("Bookings Screen", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
                startActivity(new Intent(BookingsActivity.this,BookedActivity.class));
            }
        });
        final RelativeLayout ummoLoading = findViewById(R.id.one);
        ummoLoading.setVisibility(View.VISIBLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        AgentServiceWrapper.setOnBookingServices(new DataReadyCallback() {
            @Override
            public Object exec(Object obj) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        ummoLoading.setVisibility(View.GONE);
                        adapter.notifyDataSetChanged();
                    }
                });
                return null;
            }
        });
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Ubuntu-C.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        RecyclerView recyclerView = findViewById(R.id.booking_rv);
        assert recyclerView != null;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new RVItemDecoration(20));
        //adapter = new BookingTreeAdapter(AgentServiceWrapper.getServices(),this);
        recyclerView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
        toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        initNavigationDrawer();
        if(AgentServiceWrapper.getServices().size()>0){
            ummoLoading.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
        }

        final CardView content = findViewById(R.id.content);
        //mNavigationView.getMenu().findItem(R.id.nav_queuing).setTitle("Queueing");
        //mNavigationView.getMenu().findItem(R.id.nav_queuing).setIcon(R.drawable.ummo_queue);
        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener()
                {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull android.view.MenuItem menuItem)
                    {
                        Log.e("NAVIGATION SELECTED",""+menuItem.getItemId()+" "+nav_about);
                        if (menuItem.getItemId() == nav_drawer_intro)
                        {
                            /*SpotlightSequence.getInstance(BookingsActivity.this,null)
                                    .addSpotlight(content, "Services", "Select a service to queue in...", INTRO_SWITCH)
                                    .addSpotlight(fab,"Joined queues", "Joined a queue?\n" + "Click here to view your progress...", INTRO_CARD)
                                    .addSpotlight(tb.getChildAt(2), "Feedback", "Let us know what you think...", INTRO_FEEDBACK)
                                    .startSequence();*/
                            Intent intent = new Intent(BookingsActivity.this, IntroActivity.class);
                            startActivity(intent);
                        }
                        else if (menuItem.getItemId()==R.id.nav_drawer_home){
                            try {
                                JSONObject user_lytics = new JSONObject();
                                user_lytics.put("Queue-Module", "onSelect");
                                mixpanelAPI.track("Navigation Drawer", user_lytics);
                            } catch (JSONException e) {
                                Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                            }

                            //startActivity(new Intent(BookingsActivity.this,SelectableTreeFragment_dead.class));
                            finish();
                        }
                        else if (menuItem.getItemId() == R.id.nav_drawer_about)
                        {
                            try {
                                JSONObject user_lytics = new JSONObject();
                                user_lytics.put("About", "onSelect");
                                mixpanelAPI.track("Navigation Drawer", user_lytics);
                            } catch (JSONException e) {
                                Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                            }
                            Intent i = new Intent(BookingsActivity.this, AboutActivity.class);
                            startActivity(i);

                        }
                        else if (menuItem.getItemId() == nav_drawer_feedback)
                        {
                            confirmFeedback();
                            try {
                                JSONObject user_lytics = new JSONObject();
                                user_lytics.put("Feedback", "onSelect");
                                mixpanelAPI.track("Navigation Drawer", user_lytics);
                            } catch (JSONException e) {
                                Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                            }
                        }
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_feedback, menu);
        //return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
              /*  Intent intent = new Intent(this, Console.class);
                startActivity(intent);*/

                return true;
            case R.id.feedBackDialog:
                confirmFeedback();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        mixpanelAPI.flush();
        super.onDestroy();
    }

    public void confirmFeedback() {
        DialogFragment newFragment = new FeedbackDialogFragment();
        newFragment.show(getSupportFragmentManager(), "feedback");

        //panelUp=true;
    }
}
