package com.ummo.prod.user.fcm;

import android.util.Log;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by José 06/02/17.
 **/
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        String TAG = "FirebaseInstanceID";
        Log.e(TAG, "Refreshed token: " + refreshedToken);

        System.out.println("Registration.onTokenRefresh TOKEN: " + refreshedToken );

        try {
            AppEventsLogger.setPushNotificationsRegistrationId(refreshedToken);
        } catch (Exception e) {
            Log.e("test", "Failed to complete token refresh", e);
        }
    }
}