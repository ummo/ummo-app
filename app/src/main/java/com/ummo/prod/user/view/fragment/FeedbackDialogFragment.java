package com.ummo.prod.user.view.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.ummo.prod.user.R;
import com.ummo.prod.user.qman;

public class FeedbackDialogFragment extends DialogFragment {
    EditText editText ;

    //Implementing interfaces that handle (receive event callbacks from the DialogFragment - Not sure if this will work as we want though
    public interface FeedbackDialogListener {
        void onDialogPositiveClick(DialogFragment dialogFragment);
        void onDialogNegativeClick(DialogFragment dialogFragment);
    }

    FeedbackDialogListener mListener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstance){
        //Inflating the layout to use as a dialog or embedded fragment (depending on the screen size)
      //  editText = (EditText) container.findViewById(R.id.user_feedback);
        return inflater.inflate(R.layout.feedback_custom, container, false);
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
       // editText = (EditText) activity.findViewById(R.id.user_feedback);
        editText=new EditText(this.getContext());

        try {
            mListener = (FeedbackDialogListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                        + " must implement FeedbackDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState){

        assert getActivity()!=null;
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.feedback_custom, null))
                .setView(editText)
                .setTitle(R.string.feedback_title)
                .setPositiveButton(R.string.feedback_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        qman.getQman().sendFeedback(editText.getText().toString());
                        mListener.onDialogPositiveClick(FeedbackDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.feedback_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDialogNegativeClick(FeedbackDialogFragment.this);
                    }
                });
        return builder.create();
    }
}
