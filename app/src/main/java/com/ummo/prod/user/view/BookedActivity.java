package com.ummo.prod.user.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.klinker.android.peekview.PeekViewActivity;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.R;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.adapter.BookingCardsAdapter;
import com.ummo.prod.user.ummoAPI.BookingWraper;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

@SuppressLint("Registered")
public class BookedActivity extends PeekViewActivity {

    private List<BookingWraper> mDataset = new ArrayList<BookingWraper>();
    private SwipeRefreshLayout refreshLayout;
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    public MixpanelAPI mixpanelAPI;

    @Override
    public void onPause() {
        qman.getQman().bookingsPause =true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("BOOKED ","onResume");
        BookingWraper.initMyBookings();
        BookingWraper.setOnLoadedBookings(new DataReadyCallback() {
            @Override
            public Object exec(Object obj) {
                mDataset=BookingWraper.getMyBookings();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(false);
                        mAdapter.notifyDataSetChanged();
                    }
                });
                return null;
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String projectToken = getString(R.string.mixpanelToken);

        mixpanelAPI = MixpanelAPI.getInstance(this, projectToken);
        qman.getQman().bookingsPause =false;
        String bookingDoneId=getIntent().getStringExtra("BOOKING_DONE_ID");
        if (bookingDoneId!=null)getFeedback();
        setContentView(R.layout.activity_booked);
        refreshLayout = findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                BookingWraper.initMyBookings();
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("swipeRefresh", "onComplete");
                    mixpanelAPI.track("Pending Bookings", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
            }
        });
        RelativeLayout load = findViewById(R.id.one);
        load.setVisibility(View.GONE);
        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Pending Bookings");
            getSupportActionBar().setSubtitle("Swipe down to refresh");
        }
        mRecyclerView = findViewById(R.id.booking_rv);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mDataset=BookingWraper.getMyBookings();

        // specify an adapter (see also next example)
        mAdapter = new BookingCardsAdapter(mDataset,this);
        mRecyclerView.setAdapter(mAdapter);


        BookingWraper.setOnLoadedBookings(new DataReadyCallback() {
            @Override
            public Object exec(Object obj) {
                mDataset=BookingWraper.getMyBookings();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(false);
                        mAdapter.notifyDataSetChanged();
                    }
                });
                return null;
            }
        });
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeZone tmz = TimeZone.getDefault();
                getFeedback();
                Log.e("TIMEZONE",""+tmz.getRawOffset());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getFeedback(){
        AlertDialog.Builder builder = new AlertDialog.Builder(BookedActivity.this);
        LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        builder.setTitle("We would appreciate your feedback");
        Log.e("FEEDBACK","STARTING");
        final View view = inflater.inflate(R.layout.booking_feedback_layout, null);
        builder.setView(view)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            JSONObject user_lytics = new JSONObject();
                            user_lytics.put("feedback", "onComplete");
                            mixpanelAPI.track("Pending Bookings", user_lytics);
                        } catch (JSONException e) {
                            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                        }
                    }
                })
                .create().show();
    }

}
