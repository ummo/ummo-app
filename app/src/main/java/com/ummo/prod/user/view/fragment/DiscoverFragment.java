package com.ummo.prod.user.view.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.share.Share;
import com.ummo.prod.user.R;
import com.ummo.prod.user.RVItemDecoration;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.adapter.BookingTreeAdapter;
import com.ummo.prod.user.ummoAPI.AgentServiceWrapper;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;
import com.wooplr.spotlight.prefs.PreferencesManager;

import java.util.Objects;

import io.doorbell.android.Doorbell;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscoverFragment extends Fragment {

    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView dismissCard;
    private CardView introCard;
    private Boolean dismissed = false;
    private LinearLayout discoverSubscribeLayout;

    public DiscoverFragment() {
        // Required empty public constructor
    }

    public static DiscoverFragment newInstance(){
        return new DiscoverFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View discoverView = inflater.inflate(R.layout.fragment_discover, container, false);

        Log.e("Discover Fragment", "onCreateView");
        // Inflate the layout for this fragment
        return discoverView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity()!=null) {
            android.support.v7.widget.Toolbar toolbar = getActivity().findViewById(R.id.tool_bar);
            toolbar.setTitle(getString(R.string.discover_fragment_title));
            swipeRefreshLayout = Objects.requireNonNull(getView()).findViewById(R.id.swipeRefBooking);
            introCard = getView().findViewById(R.id.discover_intro);
            dismissCard = getView().findViewById(R.id.dismissImageView);
            discoverSubscribeLayout = getView().findViewById(R.id.discoverSuggestionLayout);

            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

            dismissCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    introCard.setVisibility(View.GONE);
                    dismissed = true;
                    sharedPreferences.edit().putBoolean("DISCOVER_INTRO_DISMISSED", dismissed).apply();
                }
            });

            discoverSubscribeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Doorbell doorbell = new Doorbell(getActivity(), 8206, getString(R.string.doorbellSuggestions));
                    doorbell.setTitle("Suggestions")
                            .setName(qman.getQman().getName())
                            .addProperty("Contact", qman.getQman().getCellNumb())
                            .addProperty("User-Name", qman.getQman().getUname())
                            .setEmailFieldVisibility(View.VISIBLE)
                            .setEmailHint("Email (optional)")
                            .setMessageHint("Service suggestions...")
                            .setNegativeButtonText("Maybe Later")
                            .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    View parentLayout = getView().findViewById(R.id.fragment_discover);
                                    final ForegroundColorSpan feedbackSpan =
                                            new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.white));
                                    SpannableStringBuilder snackbarText = new SpannableStringBuilder("Feel free to suggest a service anytime...");
                                    snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                                    Snackbar snackbar = Snackbar.make(parentLayout, snackbarText, Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }
                            });

                    doorbell.setOnFeedbackSentCallback(new io.doorbell.android.callbacks.OnFeedbackSentCallback(){
                        @Override//TODO: Handle this Snackbar well
                        public void handle (String name){
                            View parentLayout = getView().findViewById(R.id.fragment_discover);
                            final ForegroundColorSpan feedbackSpan =
                                    new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.white));
                            SpannableStringBuilder snackbarText = new SpannableStringBuilder("Thank you "+qman.getQman().getName()+", we'll be in touch");
                            snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                            Snackbar snackbar = Snackbar.make(parentLayout, snackbarText, Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    })
                            .setPoweredByVisibility(View.GONE)
                            .captureScreenshot()
                            .setIcon(R.mipmap.ummo_new)
                            .setCancelable(false)
                            //.create()
                            .show();
                }
            });

            Boolean dismissPreference = sharedPreferences.getBoolean("DISCOVER_INTRO_DISMISSED",dismissed);

            if (dismissPreference){
                introCard.setVisibility(View.GONE);
            }

            final RelativeLayout ummoLoading = Objects.requireNonNull(getView()).findViewById(R.id.one);
            ummoLoading.setVisibility(View.VISIBLE);

            networkToggle();

            AgentServiceWrapper.setOnBookingServices(new DataReadyCallback() {
                @Override
                public Object exec(Object obj) {

                    Activity activity = getActivity();
                    if (activity == null) return null;

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);
                            ummoLoading.setVisibility(View.GONE);
                            //getActivity().adapter.notifyDataSetChanged();
                        }
                    });
                    return null;
                }
            });

            /*CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Ubuntu-C.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );*/
            RecyclerView recyclerView = getView().findViewById(R.id.booking_rv);
            assert recyclerView != null;
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.addItemDecoration(new RVItemDecoration(20));
            BookingTreeAdapter adapter = new BookingTreeAdapter(AgentServiceWrapper.getServices(), this);
            recyclerView.setAdapter(adapter);

            if(AgentServiceWrapper.getServices().size()>0){
                ummoLoading.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void networkToggle() {
        if (getActivity() != null){
            if (qman.getQman().isNetWorkStateConnected()) {
                Objects.requireNonNull(getView()).findViewById(R.id.offline).setVisibility(View.GONE);
                Log.e("OFFLINE", "Visibility View.GONE");
            }
        }
    }

    /*@Override
    public void onDestroyView() {
        super.onDestroyView();
        dismissed = true;

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        sharedPreferences.edit().putBoolean("DISCOVER_INTRO_DISMISSED", dismissed).apply();
    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*dismissed = true;

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        sharedPreferences.edit().putBoolean("DISCOVER_INTRO_DISMISSED", dismissed).apply();*/
    }
}