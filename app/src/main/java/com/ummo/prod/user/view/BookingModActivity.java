package com.ummo.prod.user.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.R;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.ummoAPI.Agent;
import com.ummo.prod.user.ummoAPI.AgentServiceWrapper;
import com.ummo.prod.user.ummoAPI.BookingWraper;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BookingModActivity extends AppCompatActivity implements MonthLoader.MonthChangeListener, WeekView.EventClickListener, WeekView.EventLongPressListener{
    private WeekView mWeekView;
    private Calendar start;
    private int month;
    private Agent agent=null;
    public MixpanelAPI mixpanelAPI;
    List<WeekViewEvent> events = new ArrayList<>();
    // private List<WeekViewEvent> = new
    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        Log.e("EVENT",event.toString()+eventRect.toString());
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> monthEvents=new ArrayList<>();
        month=month==0?newMonth:month;
        mWeekView.invalidate();
        for(WeekViewEvent event:events){
            if(event.getStartTime()==null){
                Log.e("NULL",event.toString());
                return null;
            }
            if(event.getStartTime().get(Calendar.MONTH)==newMonth)monthEvents.add(event);
        }
        mWeekView.invalidate();
        return newMonth==month?events:new ArrayList<WeekViewEvent>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_mod);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        String agentId = getIntent().getStringExtra("agent");

        for(AgentServiceWrapper service: AgentServiceWrapper.getServices()){
            for(Agent _agent:service.getAgents()){
                agent=_agent.get_id().equals(agentId)?_agent:agent;
            }
        }
        if (agent==null) {
            Toast.makeText(this, "AGENT DATA CORRUPTED - PLEASE REFRESH", Toast.LENGTH_LONG)
                    .show();
            finish();
        }

        setTitle("Select a Start Time");

        mWeekView = (WeekView) findViewById(R.id.weekView);
        mWeekView.setOnEventClickListener(this);
        mWeekView.setMonthChangeListener(this);
        mWeekView.setEventLongPressListener(this);
        Log.e("NOW",""+Calendar.getInstance().getTime().toString());
        final CardView cv = (CardView)findViewById(R.id.fab);
        cv.animate().translationY(-500);
        cv.setVisibility(View.VISIBLE);
        initAvailableBookings();
        mWeekView.notifyDatasetChanged();
        mWeekView.goToDate(Calendar.getInstance());
        mWeekView.goToHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

        mWeekView.setEmptyViewClickListener(new WeekView.EmptyViewClickListener() {
            @Override
            public void onEmptyViewClicked(Calendar time) {
                int minRounded=time.get(Calendar.MINUTE)%5<3?(time.get(Calendar.MINUTE)-time.get(Calendar.MINUTE)%5):(time.get(Calendar.MINUTE)+(5-time.get(Calendar.MINUTE)%5));
                time.set(Calendar.MINUTE,minRounded);
                cv.animate().setStartDelay(100).translationY(0).setDuration(1000);
                cv.setVisibility(View.VISIBLE);
                AppCompatTextView tv = (AppCompatTextView) findViewById(R.id.date_tv);
               // Log.e("Time",""+time.getTimeZone().toString()+" "+time.getTime().toString());
                tv.setText(time.getTime().toString().substring(0,16));
                start=time;
                Calendar _end = Calendar.getInstance();
                _end.setTime(start.getTime());
                _end.add(Calendar.MINUTE,1);
                WeekViewEvent event = new WeekViewEvent(0,"",start,_end);
                initAvailableBookings();
                events.add(event);
                mWeekView.invalidate();
                mWeekView.notifyDatasetChanged();
            }
        });

        final LinearLayout ly = (LinearLayout)findViewById(R.id.next_ly);
        ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String projectToken = getString(R.string.mixpanelToken);
                mixpanelAPI = MixpanelAPI.getInstance(getApplicationContext(), projectToken);
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("nextDialog", "onClick");
                    mixpanelAPI.track("Calendar View", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
                final AlertDialog.Builder builder = new AlertDialog.Builder(BookingModActivity.this);
                LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                final View view = inflater.inflate(R.layout.qman_end_time, null);
                final EditText durationEt = (EditText)view.findViewById(R.id.durationEt);
                //final EditText nameEt = (EditText)view.findViewById(R.id.nameEt);
                //final EditText cellEt = (EditText) view.findViewById(R.id.cellEt);
                final EditText serviceEt  = (EditText) view.findViewById(R.id.serviceEt);
                builder.setView(view)
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String service = serviceEt.getText().toString();
                                Calendar end = Calendar.getInstance();
                                Date _start = start.getTime();
                                end.setTime(_start);
                                String dur = durationEt.getText().toString();
                                if(service.length()==0||dur.length()==0){
                                    Snackbar.make(cv,"PLEASE USE VALID VALUES",Snackbar.LENGTH_SHORT).show();
                                   return;
                                }
                                int duration = Integer.valueOf(dur);
                                end.add(Calendar.MINUTE,duration);
                                WeekViewEvent event = new WeekViewEvent(1234,service,"",start,end);
                                events.add(event);
                                mWeekView.notifyDatasetChanged();
                                BookingWraper.createBooking(start,end,service,agent.get_id());
                                final AlertDialog.Builder _builder = new AlertDialog.Builder(BookingModActivity.this);
                                LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                                final View view = inflater.inflate(R.layout.layout_booking_loading, null);
                                _builder.setView(view);
                                final AlertDialog dialog1 = _builder.create();
                                dialog1.show();
                                BookingWraper.setOnBookingCreated(new DataReadyCallback() {
                                    @Override
                                    public Object exec(Object obj) {
                                        qman.getQman().getMyBookings();
                                        dialog1.dismiss();
                                        startActivity(new Intent(BookingModActivity.this,BookedActivity.class));
                                        try {
                                            JSONObject user_lytics = new JSONObject();
                                            user_lytics.put("makeBookingDialog", "onView");
                                            user_lytics.put("makeBookingDialog", "onSave");
                                            mixpanelAPI.track("Booking Saved", user_lytics);
                                        } catch (JSONException e) {
                                            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                        }
                                        finish();
                                        return null;
                                    }
                                });
                                cv.animate().setStartDelay(100).translationY(-500);
                            }
                        })
                        .create().show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initAvailableBookings(){
        events.clear();
        if(agent.getBookings().size()<1)Log.e("INIT BOOKINGS","No Bookings Here");

        for (int i = 0; i <agent.getBookings().size(); i++){
            BookingWraper bk = agent.getBookings().get(i);
            Log.e("SERVICE ",bk.getService());
            events.add(new WeekViewEvent(i,"BOOKED",bk.getStart(),bk.getEnd()));
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mWeekView.notifyDatasetChanged();
            }
        });
    }
}
