package com.ummo.prod.user.view.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.accountkit.AccountKit;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.R;
import com.ummo.prod.user.accountKit.AccountKit_MainActivity;
import com.ummo.prod.user.db.room.AppDatabase;
import com.ummo.prod.user.db.room.entities.User;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.view.SplashScreen;

import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment{

    TextView nameTV, contactTV, pinTV;
    ImageView editProfile;
    FloatingActionButton profileFAB, logoutFAB;
    private MixpanelAPI mixpanelAPI;
    private AppDatabase appDb;
    private static final String TAG = "ProfileFrag";

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(){
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View profileView = inflater.inflate(R.layout.fragment_profile, container, false);

        Log.e("Profile Fragment", "onCreateView");
        // Inflate the layout for this fragment
        return profileView;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appDb = AppDatabase.getInMemoryDatabase(getContext());

        fetchUser();
    }

    private void fetchUser(){
        List<User> user = appDb.userModel().loadUserInfo();
        Log.e(TAG+" fetchUser", "Room Query 2->"+user);
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String NAME = qman.getQman().getName();
        String CELL = qman.getQman().getCellNumb().substring(4);
        String SURNAME = qman.getQman().getSurname();
        //String PIN = "";

        if (getActivity()!=null) {
            android.support.v7.widget.Toolbar toolbar = getActivity().findViewById(R.id.tool_bar);
            toolbar.setTitle("Ummo Profile");
        }

        nameTV = Objects.requireNonNull(getView()).findViewById(R.id.account_name_placeholder);
        contactTV = Objects.requireNonNull(getView()).findViewById(R.id.account_contact_placeholder);
        /*editProfile = (ImageView) Objects.requireNonNull(getView().findViewById(R.id.editProfileButton));

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<User> user = appDb.userModel().loadUserInfo();
                Log.e(TAG+" onClick", "User Name->"+user.get(0).name);
                Log.e(TAG+" onClick", "User Surname->"+user.get(0).surname);
                Log.e(TAG+" onClick", "User Contact->"+user.get(0).cell_num);
                //Log.e(TAG+" onClick", "User Joined Q->"+user.get(0).joined_queue);
                //appDb.userModel().deleteUser(qman.getQman().getCellNumb());
                fetchUser();
            }
        });*/

        logoutFAB = (FloatingActionButton) Objects.requireNonNull(getView().findViewById(R.id.logout_fab));
        nameTV.setText(NAME + " " + SURNAME);
        contactTV.setText(CELL);
        //pinTV.setText("PIN not entered yet");

        logoutFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view1) {
                assert getContext()!=null;

                //String user = appDb.userModel().loadNameAndSurname(qman.getQman().getCellNumb()).toString();
                //Log.e(TAG+ "logout", "Room Query 4 (User)->"+user);
                AlertDialog.Builder logoutAlertDialog =
                        new AlertDialog.Builder(getContext())
                                .setTitle("Confirm Log-out")
                                .setMessage("Are you sure you want to log-out of your account?")
                                .setPositiveButton("Yes, Continue", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onLogout(view1);
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .setIcon(R.drawable.ic_exit);

                logoutAlertDialog.show().getButton(DialogInterface.BUTTON_POSITIVE)
                        .setTextColor(getResources().getColor(R.color.red_red));
            }
        });
        /*CardView shareCard = getActivity().findViewById(R.id.share_card);
        shareCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Sharing is caring...", Toast.LENGTH_LONG).show();
            }
        });*/
        /*profileFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ScanID.class));
            }
        });*/

        /*ViewCompat.setTransitionName(Objects.requireNonNull(getView()).findViewById(R.id.app_bar_layout), "Profile");

        CollapsingToolbarLayout collapsingToolbarLayout = getView().findViewById(R.id.profile_collapsing_toolbar);
        collapsingToolbarLayout.setTitle("Profile");*/
    }

    public void onLogout(View view){
        AccountKit.logOut();

        ProgressDialog dialog = ProgressDialog.show(getContext(),"Please wait","Logging you out of Ummo...");
        dialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    startActivity(new Intent(getActivity(), AccountKit_MainActivity.class));
                    //Overwriting the sharedPreferences 'cellNumb' value
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(qman.getQman());
                    sp.edit().putString("cellNumb","000").apply();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
