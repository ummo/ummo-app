package com.ummo.prod.user.interfaces;

/**
 * Created by mosaic on 10/11/16.
 **/

public interface Poll {
    void onPollData(Object obj);
}
