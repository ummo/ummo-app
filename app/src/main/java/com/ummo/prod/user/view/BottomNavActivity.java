package com.ummo.prod.user.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.R;
import com.ummo.prod.user.qman;

import static com.ummo.prod.user.R.string.nav_about;

import com.ummo.prod.user.view.fragment.ProfileFragment;
import com.ummo.prod.user.view.fragment.DiscoverFragment;
import com.ummo.prod.user.view.fragment.HomeFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import io.doorbell.android.Doorbell;

public class BottomNavActivity extends AppCompatActivity {

    public MixpanelAPI mixpanelAPI;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private android.support.v7.widget.Toolbar toolbar;
    Random RANDOM = new Random();
    MaterialLetterIcon profile;
    ActionBarDrawerToggle drawerToggle;
    int[] materialColors;
    private static final String TAG = "BottomNavAct";
    public SharedPreferences sharedPreferences;
    public static JSONArray savedQueueDetails = new JSONArray();

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    private void initNavigationDrawer() {

        drawerLayout = findViewById(R.id.bottom_navigation_DrawerLayout);
        navigationView = findViewById(R.id.nav_view);
        View headerLayout;
        headerLayout = navigationView.getHeaderView(0);
        materialColors = getResources().getIntArray(R.array.colors);
        profile = headerLayout.findViewById(R.id.circleView);
        TextView userName = headerLayout.findViewById(R.id.name);
        TextView cellNumber = headerLayout.findViewById(R.id.email);

        String NAME = qman.getQman().getName();
        String CELL = qman.getQman().getCellNumb();
        String SURNAME = qman.getQman().getSurname();

        userName.setText(NAME);
        cellNumber.setText(CELL);

        profile.setShapeColor(materialColors[RANDOM.nextInt(materialColors.length)]);
        profile.setInitials(true);
        profile.setInitialsNumber(2);
        profile.setLetterSize(18);
        profile.setLetter(NAME+SURNAME);

        setupActionBarDrawerToggle();
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
    }

    private void setupActionBarDrawerToggle() {

        drawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                toolbar,
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        )
        {
            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                //Snackbar.make(view, R.string.drawer_close, Snackbar.LENGTH_SHORT).show();
            }

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                //Snackbar.make(drawerView, R.string.drawer_open, Snackbar.LENGTH_SHORT).show();
                //   mPreferencesManager.resetAll();
            }
        };
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        addItemsRunTime(navigationView);
    }

    private void addItemsRunTime(NavigationView navigationView) {
        // refreshing navigation drawer adapter
        for (int i = 0, count = navigationView.getChildCount(); i < count; i++) {
            final View child = navigationView.getChildAt(i);
            if (child != null && child instanceof ListView) {
                final ListView menuView = (ListView) child;
                final HeaderViewListAdapter adapter = (HeaderViewListAdapter) menuView.getAdapter();
                final BaseAdapter wrapped = (BaseAdapter) adapter.getWrappedAdapter();
                wrapped.notifyDataSetChanged();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_nav);

        final String projectToken = getString(R.string.mixpanelToken);
        mixpanelAPI = MixpanelAPI.getInstance(this, projectToken);

        final BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        toolbar = findViewById(R.id.tool_bar);
        toolbar.setTitle("Ummo");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        initNavigationDrawer();
        setupNavigationView();

        /*
         *Queue details fetched from SharedPreferences successfully below;
         * now to convert them to JSONArray then pass them to HomeFragment
         */
        String sharedPrefFile = "com.ummo.prod.user";
        sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        String fetchedQueueString = sharedPreferences.getString("SAVED_QUEUE_DATA","");

        Log.e(TAG+" onCreate", "Checking: fetchedQueueDetails->"+fetchedQueueString);
        try {
            JSONArray fetchedQueueDetails = new JSONArray(fetchedQueueString);
            setSavedQueueDetails(fetchedQueueDetails);
            getSavedQueueDetails();
            //Log.e(TAG+" onCreate", "Converting: savedQueueDetails->"+savedQueueDetails);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Log.e(TAG+" onCreate","SavedQueueDetails->"+savedQueueDetails);

        Bundle saveQueueBundle = new Bundle();
        saveQueueBundle.putString("SAVED_QUEUE_DETAILS",fetchedQueueString);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull android.view.MenuItem menuItem) {
                        Log.e(TAG+" onCreate","onNavigationItemSelect -> "
                                +menuItem.getItemId()+" "+nav_about);
                        menuItem.setChecked(true);
                        android.support.v4.app.FragmentManager fragmentManager;
                        android.support.v4.app.FragmentTransaction fragmentTransaction;
                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        Fragment selectedFragment;

                        switch (menuItem.getItemId()) {
                            case R.id.nav_drawer_home:
                                selectedFragment = HomeFragment.newInstance();
                                bottomNavigationView.setSelectedItemId(R.id.bottom_nav_home);

                                fragmentTransaction.replace(R.id.rootLayout, selectedFragment);
                                fragmentTransaction.commit();
                                //Mixpanel crumbs
                                try {
                                    JSONObject user_lytics = new JSONObject();
                                    user_lytics.put("Home", "onSelect");
                                    mixpanelAPI.track("Navigation Drawer", user_lytics);
                                } catch (JSONException e) {
                                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                }

                                break;

                            case R.id.nav_drawer_discover:
                                selectedFragment = DiscoverFragment.newInstance();
                                bottomNavigationView.setSelectedItemId(R.id.bottom_nav_discover);

                                fragmentTransaction.replace(R.id.rootLayout, selectedFragment);
                                fragmentTransaction.commit();
                                //Mixpanel crumbs
                                try {
                                    JSONObject user_lytics = new JSONObject();
                                    user_lytics.put("Discover", "onSelect");
                                    mixpanelAPI.track("Navigation Drawer", user_lytics);
                                } catch (JSONException e) {
                                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                }

                                break;

                            case R.id.nav_drawer_profile:
                                selectedFragment = ProfileFragment.newInstance();
                                bottomNavigationView.setSelectedItemId(R.id.bottom_nav_profile);

                                fragmentTransaction.replace(R.id.rootLayout, selectedFragment);
                                fragmentTransaction.commit();
                                //Mixpanel crumbs
                                try {
                                    JSONObject user_lytics = new JSONObject();
                                    user_lytics.put("Profile", "onSelect");
                                    mixpanelAPI.track("Navigation Drawer", user_lytics);
                                } catch (JSONException e) {
                                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                }

                                break;

                            case R.id.nav_drawer_about:
                                startActivity(new Intent(BottomNavActivity.this,
                                        AboutActivity.class));
                                //Mixpanel crumbs
                                try {
                                    JSONObject user_lytics = new JSONObject();
                                    user_lytics.put("About", "onSelect");
                                    mixpanelAPI.track("Navigation Drawer", user_lytics);
                                } catch (JSONException e) {
                                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                }
                                break;

                            case R.id.nav_drawer_intro:
                                /*startActivity(new Intent(BottomNavActivity.this,
                                        IntroActivity.class));*/
                                final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                sharedPreferences.edit().putBoolean("UMMO_INTRO_DISMISSED", false).apply();
                                sharedPreferences.edit().putBoolean("DISCOVER_INTRO_DISMISSED", false).apply();

                                selectedFragment = HomeFragment.newInstance();
                                bottomNavigationView.setSelectedItemId(R.id.bottom_nav_home);

                                fragmentTransaction.replace(R.id.rootLayout, selectedFragment);
                                fragmentTransaction.commit();
                                //Mixpanel crumbs
                                try {
                                    JSONObject user_lytics = new JSONObject();
                                    user_lytics.put("Intro", "onSelect");
                                    mixpanelAPI.track("Navigation Drawer", user_lytics);
                                } catch (JSONException e) {
                                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                }
                                break;

                            case R.id.nav_drawer_feedback:
                                confirmFeedback();
                                //Mixpanel crumbs
                                try {
                                    JSONObject user_lytics = new JSONObject();
                                    user_lytics.put("Feedback", "onSelect");
                                    mixpanelAPI.track("Navigation Drawer", user_lytics);
                                } catch (JSONException e) {
                                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                }
                                break;

                            case R.id.nav_drawer_suggestions:
                                suggestService();
                                //Mixpanel crumbs
                                try {
                                    JSONObject user_lytics = new JSONObject();
                                    user_lytics.put("SuggestService", "onSelect");
                                    mixpanelAPI.track("Navigation Drawer", user_lytics);
                                } catch (JSONException e) {
                                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                }
                                break;
                        }

                        drawerLayout.closeDrawers();
                        return true;


                        /*if (menuItem.getItemId() == R.id.nav_intro)
                        {
                            /*SpotlightSequence.getInstance(BookingsActivity.this,null)
                                    .addSpotlight(content, "Services", "Select a service to queue in...", INTRO_SWITCH)
                                    .addSpotlight(fab,"Joined queues", "Joined a queue?\n" + "Click here to view your progress...", INTRO_CARD)
                                    .addSpotlight(tb.getChildAt(2), "Feedback", "Let us know what you think...", INTRO_FEEDBACK)
                                    .startSequence();
                            Intent intent = new Intent(BottomNavActivity.this, IntroActivity.class);
                            startActivity(intent);
                        }
                        else if (menuItem.getItemId() == nav_about)
                        {
                            try {
                                JSONObject user_lytics = new JSONObject();
                                user_lytics.put("About", "onSelect");
                                //mixpanelAPI.track("Navigation Drawer", user_lytics);
                            } catch (JSONException e) {
                                Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                            }
                            Intent i = new Intent(BottomNavActivity.this, AboutActivity.class);
                            startActivity(i);

                        }
                        else if (menuItem.getItemId() == feedback)
                        {
                            confirmFeedback();
                            try {
                                JSONObject user_lytics = new JSONObject();
                                user_lytics.put("Feedback", "onSelect");
                                //mixpanelAPI.track("Navigation Drawer", user_lytics);
                            } catch (JSONException e) {
                                Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                            }
                        }*/
                    }
                });

        Boolean isConnected = qman.getSocket().connected();
        if (!isConnected) {
            View parentLayout = findViewById(R.id.fragment_home);
            final ForegroundColorSpan redSpan =
                    new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
            SpannableStringBuilder snackbarText = new SpannableStringBuilder("Connection lost...");
            snackbarText.setSpan(redSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            Snackbar snackbar = Snackbar.make(bottomNavigationView, snackbarText, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void setupNavigationView() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        if (bottomNavigationView != null) {

            // Select first menu item by default and show Fragment accordingly.
            Menu menu = bottomNavigationView.getMenu();
            selectFragment(menu.getItem(0));

            // Set action to perform when any menu-item is selected.
            bottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            selectFragment(item);
                            return false;
                        }
                    });
        }
    }

    protected void selectFragment(MenuItem item) {
        item.setChecked(true);
        android.support.v4.app.FragmentManager fragmentManager;
        android.support.v4.app.FragmentTransaction fragmentTransaction;
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        Fragment selectedFragment = null;

        final String projectToken = getString(R.string.mixpanelToken);
        mixpanelAPI = MixpanelAPI.getInstance(this, projectToken);

        switch (item.getItemId()) {
            case R.id.bottom_nav_home:
                selectedFragment = HomeFragment.newInstance();
                //Mixpanel crumbs
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("Home", "onSelect");
                    mixpanelAPI.track("Bottom Nav.", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
                break;

            case R.id.bottom_nav_discover:
                selectedFragment = DiscoverFragment.newInstance();
                //Mixpanel crumbs
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("Discover", "onSelect");
                    mixpanelAPI.track("Bottom Nav.", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
                break;

            case R.id.bottom_nav_profile:
                selectedFragment = ProfileFragment.newInstance();
                //Mixpanel crumbs
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("Profile", "onSelect");
                    mixpanelAPI.track("Bottom Nav.", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
                break;
        }

        fragmentTransaction.replace(R.id.rootLayout, selectedFragment);
        fragmentTransaction.commit();
    }

    public void setSavedQueueDetails(JSONArray array){
        savedQueueDetails = array;
        Log.e(TAG+" setSavedQ", "SavedQueueDetails->"+savedQueueDetails);
    }

    public JSONArray getSavedQueueDetails(){
        Log.e(TAG+" getSavedQ", "SavedQueueDetails->"+savedQueueDetails);
        return savedQueueDetails;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_feedback, menu);
        //return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case R.id.feedBackDialog:
                confirmFeedback();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void suggestService(){
        Doorbell doorbell = new Doorbell(this, 8206, getString(R.string.doorbellSuggestions));
        doorbell.setTitle("Suggestions")
                .setName(qman.getQman().getName())
                .addProperty("Contact", qman.getQman().getCellNumb())
                .addProperty("User-Name", qman.getQman().getUname())
                .setEmailFieldVisibility(View.VISIBLE)
                .setEmailHint("Email (optional)")
                .setMessageHint("Where would you love to queue with Ummo?")
                .setNegativeButtonText("Maybe Later")
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        View parentLayout = findViewById(R.id.fragment_home);
                        final ForegroundColorSpan feedbackSpan =
                                new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        SpannableStringBuilder snackbarText = new SpannableStringBuilder("Feel free to suggest a service anytime...");
                        snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        Snackbar snackbar = Snackbar.make(parentLayout, snackbarText, Snackbar.LENGTH_LONG);
                        snackbar.show();

                        //Mixpanel crumbs
                        try {
                            JSONObject user_lytics = new JSONObject();
                            user_lytics.put("Dialog", "onDismiss");
                            mixpanelAPI.track("Suggest Service", user_lytics);
                        } catch (JSONException e) {
                            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                        }
                    }
                });

        doorbell.setOnFeedbackSentCallback(new io.doorbell.android.callbacks.OnFeedbackSentCallback(){
            @Override//TODO: Handle this Snackbar well
            public void handle (String name){
                View parentLayout = findViewById(R.id.fragment_home);
                final ForegroundColorSpan feedbackSpan =
                        new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.white));
                SpannableStringBuilder snackbarText = new SpannableStringBuilder("Thank you "+qman.getQman().getName()+", we'll be in touch");
                snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                Snackbar snackbar = Snackbar.make(parentLayout, snackbarText, Snackbar.LENGTH_LONG);
                snackbar.show();

                //Mixpanel crumbs
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("Dialog", "onSubmit");
                    mixpanelAPI.track("Suggest Service", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
            }
        })
        .setPoweredByVisibility(View.GONE)
        .captureScreenshot()
        .setIcon(R.mipmap.ummo_new)
        .setCancelable(false)
            //.create()
        .show();
    }

    public void confirmFeedback() {
        Log.e(TAG+" confirm", "Feedback tapped");
        //DialogFragment newFragment = new FeedbackFragment();
        //newFragment.show(getSupportFragmentManager(), "feedback");
        Doorbell doorbell = new Doorbell(this, 7790, getString(R.string.doorbellPrivateKey));
        doorbell.setTitle("Ummo Feedback")
                .setName(qman.getQman().getName())
                .addProperty("Contact", qman.getQman().getCellNumb())
                .addProperty("User-Name", qman.getQman().getUname())
                .setEmailFieldVisibility(View.VISIBLE)
                .setEmailHint("Email (optional)")
                .setMessageHint("Let us know what you think")
                .setNegativeButtonText("Maybe Later")
                .setOnFeedbackSentCallback(new io.doorbell.android.callbacks.OnFeedbackSentCallback(){
                    @Override//TODO: Handle this Snackbar well
                    public void handle (String name){
                        View parentLayout = findViewById(R.id.fragment_home);
                        final ForegroundColorSpan feedbackSpan =
                                new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.ummo_3));
                        SpannableStringBuilder snackbarText = new SpannableStringBuilder("Thank you "+qman.getQman().getName()+", we'll be in touch");
                        snackbarText.setSpan(feedbackSpan, 0, snackbarText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        Snackbar snackbar = Snackbar.make(parentLayout, snackbarText, Snackbar.LENGTH_LONG);
                        snackbar.show();
                        //Mixpanel crumbs
                        try {
                            JSONObject user_lytics = new JSONObject();
                            user_lytics.put("Dialog", "onSubmit");
                            mixpanelAPI.track("Give Feedback", user_lytics);
                        } catch (JSONException e) {
                            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                        }
                    }
                })
                .setPoweredByVisibility(View.GONE)
                .captureScreenshot()
                .setIcon(R.mipmap.ummo_new)
                .setCancelable(false)
                .show();
    }
}
