package com.ummo.prod.user.db.room.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.ummo.prod.user.db.room.ArrayListConverter;

import java.util.ArrayList;

@Entity (indices = {@Index("service_queues"), @Index(value = {"service_id"},unique = true)}/*,
        foreignKeys = {@ForeignKey(entity = Queue.class,
                                    parentColumns = "queue_id",
                                    childColumns = "service_queues")}*/)
@TypeConverters(ArrayListConverter.class)
public class Service {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "service_id")
    public String service_id;

    @ColumnInfo(name = "service_name")
    public String service_name;

    @ColumnInfo(name = "service_category")
    public String service_cat;

    @ColumnInfo(name = "service_queues")
    public ArrayList<String> service_queue;
}