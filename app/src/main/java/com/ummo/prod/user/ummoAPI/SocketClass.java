package com.ummo.prod.user.ummoAPI;

import android.util.Log;

import com.ummo.prod.user.qman;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;

/**
 * Created by mosaic on 6/4/17.
 * Socket IO Heavy lifting happens here
 */

public class SocketClass {
    private static Socket socket= qman.getSocket();
    private static boolean connected=false;

    public interface Response{
        Object ready(Object val);
    }

    private static HashMap<String,Request> requestQueue = new HashMap<>();

    public static void emit(final String key,Object value,Response repn){
        socket.emit(key,value);
        socket.off(key);
        socket.on(key, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("SOCKET_CLASS","ARGS[0] "+args[0]);
                Log.e("KEY ",key);
                Request request=requestQueue.remove(key);
                if(request==null)
                    return;
                try{
                    try {
                        JSONObject object = (JSONObject) args[0];
                        request.exec(object.has("data")?object.get("data"):object);
                    }catch (ClassCastException cce){
                        request.exec(args[0]);
                    }

                }catch (JSONException jse){
                    Log.e("SOCKET_CLASS",jse.toString());
                }
               // request.exec(args[0]);
            }
        });
        requestQueue.put(key,new Request(key,value,repn));
    }

    public static void init(){
        socket.io().on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Transport transport = (Transport)args[0];

                transport.on(Transport.EVENT_REQUEST_HEADERS, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        @SuppressWarnings("unchecked")
                        Map<String, String> headers = (Map<String, String>)args[0];
                        // modify request headers
                        //headers.put("Cookie", "usr=mosaic;uid="+qman.getQman().getUmmoId());
                    }
                });

                transport.on(Transport.EVENT_RESPONSE_HEADERS, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        @SuppressWarnings("unchecked")
                        Map<String, List<String>> headers = (Map<String, List<String>>)args[0];
                        // access response headers
                       // String cookie = headers.get("Set-Cookie").toString();
                        //Log.e("SET COOKIE",cookie);
                    }
                });
            }
        });

            socket.on(Socket.EVENT_RECONNECT_ATTEMPT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_RECONNECT_ATTEMPT "+args[0]);
                }
            });
            socket.on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_RECONNECT "+args[0]);
                }
            });
            socket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_ERROR "+args[0]);
                    //socket.disconnect();
                    socket.connect();
                }
            });
            socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if(!connected)return;
                    connected=false;
                    Log.e("SOCKET","EVENT_DISCONNECT "+args[0]);
                }
            });
            socket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_CONNECT_TIMEOUT "+args[0]);
                }
            });
            socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_CONNECT_ERROR "+args[0]);
                }
            });
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (connected) return;
                    Log.e("SOCKET","EVENT_CONNECT");
                    connected=true;
                    Object[] keys = requestQueue.keySet().toArray();
                    for (int i =0;i<keys.length;i++){
                        Request r = requestQueue.get(keys[i]);
                        Log.e("SOCKET-CLASS","RESENDING "+r.key);
                        socket.emit(r.key,r.value);
                    }
                }
            });
    }

    static class Request{
        public String key;
        public Object value;
        private Response dataReady;

        public Object exec(Object obj){
            return dataReady.ready(obj);
        }

        Request(String _key, Object _val, Response respn){
            this.key=_key;
            this.value=_val;
            this.dataReady=respn;
        }
    }
}