package com.ummo.prod.user.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.ummo.prod.user.R;

/**
 * Created by Jose on 2017/06/18.
 **/

public class IntroActivity extends AppIntro{

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance("Welcome to Ummo",
                "To find out more, check out the 'ABOUT'", R.drawable.ummo_dark,
                getResources().getColor(R.color.ummo_3)));
        /*addSlide(AppIntroFragment.newInstance("First Slide", "Do this...",
                R.drawable.booking_screenshot,
                getResources().getColor(R.color.light_yellow)));
        addSlide(AppIntroFragment.newInstance("Second Slide", "Queues...",
                R.drawable.queue_screenshot, getResources().getColor(R.color.Aqua)));
        addSlide(AppIntroFragment.newInstance("Last Slide", "Feedback",
                R.drawable.feedback_screenshot, getResources().getColor(R.color.CornflowerBlue)));*/

        setBarColor(getResources().getColor(R.color.ummo_1));

        setFadeAnimation();
        showSkipButton(false);
        setVibrate(true);
        setVibrateIntensity(30);
    }

    private void loadMainActivity(){
        Intent intent = new Intent(this, BottomNavActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        loadMainActivity();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        loadMainActivity();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
