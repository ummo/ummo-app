package com.ummo.prod.user.ummoAPI;

import android.content.Context;
import android.util.Log;

import com.ummo.prod.user.qman;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;

import com.ummo.prod.user.db.room.AppDatabase;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mosaic on 5/4/17.
 **/

public class QueueWrapper {
    private String qName;
    private String qActive;
    private String location;
    private String qRequirements;
    private String _id;
    private int ttdq;
    private int length;
    public static QueueWrapper selectedQueue;
    private static DataReadyCallback selectedQCallback=null;
    private static DataReadyCallback joinCallBack;
    private static String TAG = "QueueWrapper";
    private AppDatabase appDb;
    private static Context context = null;

    public static void setSelectedQCallback(DataReadyCallback _cb){
        selectedQCallback = _cb;
    }

    public static  void setJoinCallBack(DataReadyCallback jc){
        joinCallBack=jc;
    }

    public static QueueWrapper getSelectedQueue(){
        return selectedQueue;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String get_id() {
        return _id;
    }

    public static void loadQueue(String _id){
        Log.e("GETTING Q-DATA",_id);
        /*qman.getQman().getSocket().emit("get_queue_data",_id);
        qman.getQman().getSocket().on("queue_data", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("QUEUE DATA",args[0].toString());
                selectedQueue=new QueueWrapper((JSONObject)args[0]);
                if(selectedQCallback!=null)selectedQCallback.exec(selectedQueue);
            }
        });*/
        selectedQueue=null;
        QueueJoinWrapper.loadData();
        SocketClass.emit("get_queue_data", _id, new SocketClass.Response() {
            @Override
            public Object ready(Object val) {
                Log.e(TAG+" loadQueue","get_queue_data ->"+val.toString());
                selectedQueue=new QueueWrapper((JSONObject)val);
                if(selectedQCallback!=null)selectedQCallback.exec(selectedQueue);
                return null;
            }
        });
    }

    public static void joinSelectedQueue(){
        String cell = qman.getQman().getCellNumb();
        try{
            final JSONObject object = new JSONObject();
            object.put("que",selectedQueue.get_id());
            object.put("_cell", qman.getQman().getCellNumb());
            QueueJoinWrapper.loadData();
            SocketClass.emit("join_selected_queue", object, new SocketClass.Response() {
                @Override
                public Object ready(Object val) {
                    Log.e(TAG+" joinSelectedQueue","Object ->"+object);
                    if(joinCallBack!=null) {
                        joinCallBack.exec(val);
                    }else {
                        Log.e(TAG+" joinSelectedQueue","JOIN CALLBACK is null");
                    }
                    return null;
                }
            });
        }
        catch (JSONException jse){
            Log.e("JOIN ERR",jse.toString());
        }
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setqActive(String qActive) {
        this.qActive = qActive;
    }

    public void setqName(String qName) {
        this.qName = qName;
    }

    public String getqActive() {
        return qActive;
    }

    public String getqName() {
        return qName;
    }

    public String getqRequirements() {
        return qRequirements;
    }

    private void setqRequirements(String qRequirements) {
        this.qRequirements = qRequirements;
    }

    public int getTtdq() {
        return ttdq;
    }

    private void setTtdq(int ttdq) {
        this.ttdq = ttdq;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    QueueWrapper(JSONObject object){
        try{
            setqName(object.getString("qName"));
            setqRequirements(object.getString("qRequirements"));
            set_id(object.getString("_id"));
            setLocation(object.getString("location"));
            if(object.has("ttdq"))setTtdq(object.getInt("ttdq")/(60*1000));
            if(object.has("length"))setLength(object.getInt("length"));
            Log.e(TAG+" QueueWrapper", "Object ->"+object);
        }catch (JSONException jse){
            Log.e(TAG+"ERROR CREATING QUEUE",jse.toString());
        }
    }

   /* QueueWrapper(JSONObject object, android.content.Context context) {
       try{
            String qName = object.getString("qName");
            String qRequirements = object.getString("qRequirements");
            String qId = object.getString("_id");
            String qLocation = object.getString("location");
            int ttdq = object.has("ttdq") ? object.getInt("ttdq") : 0;
            int length = object.has("length") ? object.getInt("length") : 0;

            Queue queue = new Queue();
            queue.queue_id = qId;
            queue.queue_name = qName;
            queue.queue_location = qLocation;
            queue.queue_req = qRequirements;
            queue.queue_length = length;
            queue.queue_ttdq = ttdq;

            appDb = AppDatabase.getInMemoryDatabase(context);
            appDb.queueModel().insertQueue(queue);

            Log.e(TAG+" QueueWrapper", "Room-Queue ->"+queue.queue_name);
        }catch (JSONException jse){
            Log.e(TAG+"ERROR CREATING QUEUE",jse.toString());
        }
    }*/
}
