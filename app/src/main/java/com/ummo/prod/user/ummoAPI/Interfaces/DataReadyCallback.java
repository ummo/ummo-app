package com.ummo.prod.user.ummoAPI.Interfaces;

/**
 * Created by mosaic on 5/8/17.
 **/

public interface DataReadyCallback {
    Object exec(Object obj);
}
