package com.ummo.prod.user.onesignal;

import android.app.Application;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class OSNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler{

    private Application application;
    private static final String TAG = "OSNotifReceived ";

    public OSNotificationReceivedHandler(Application application) {
        this.application = application;
    }

    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;
        String customKey;

        if (data != null) {
            customKey = data.optString("customKey",null);

            if (customKey != null){
                Log.e(TAG+"", "customKey set with value ->"+customKey);
            }
        }
    }
}
