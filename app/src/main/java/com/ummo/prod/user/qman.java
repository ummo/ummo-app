package com.ummo.prod.user;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.support.multidex.MultiDex;
import com.facebook.accountkit.AccountKit;
import com.onesignal.OneSignal;
import com.ummo.prod.user.db.room.AppDatabase;
import com.ummo.prod.user.db.room.entities.User;
import com.ummo.prod.user.interfaces.Poll;
import com.ummo.prod.user.interfaces.UmmoEventListener;
import com.ummo.prod.user.onesignal.OSNotificationOpenedHandler;
import com.ummo.prod.user.onesignal.OSNotificationReceivedHandler;
import com.ummo.prod.user.ummoAPI.AgentServiceWrapper;
import com.ummo.prod.user.ummoAPI.BookingWraper;
import com.ummo.prod.user.ummoAPI.FormPoster;
import com.ummo.prod.user.ummoAPI.QueueJoinWrapper;
import com.ummo.prod.user.ummoAPI.SocketClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by mosaic on 6/19/16.
 **/
public class qman extends Application implements Poll {
    private  static Socket mSocket;
    private Activity currentActivity = null;
    private final String posString = "Position ";
    private boolean netWorkStateConnected =false;
    public static final int MESSAGE_NOTIFICATION_ID = 435345;
    public static final int OFFLINE_NOTIFICATION_ID = 435346;
    private qManWrapper wrapper;
    private static qman app;
    public JSONArray bookingServices=null;
    private JSONArray bookings;
    private JSONObject selectedQ=null;
    private JSONObject lastJoinedQ=null;
    private long lastOfflineTime;
    private List<Long> offlineTimes = new ArrayList<Long>();
    private JSONArray categories = null;
    public ArrayList<JSONObject> joinedQs = new ArrayList<>();
    private boolean joinedSelectedQ =false;
    public JSONArray agents=null;
    private   HashMap<String,UmmoEventListener> ummoEventsHashMap = new HashMap<>();
    private JSONObject qmanData = null;
    public boolean bookingsPause =true;
    private String TAG = "Qman ";
    private AppDatabase appDb;

    List<String> doneBookingIds = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public void loadBookings(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            bookings = new JSONArray(sp.getString("bookings","[]"));
        }
        catch (JSONException jse){
            Log.e("JSON-ERR",jse.toString());
        }
    }

    public void saveAgents(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        if(bookings!=null){
            sp.edit().putString("bookings",bookings.toString())
                    .apply();
        }
    }

    public static Socket getSocket(){
        return mSocket;
    }
    public void loadAgents(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            agents = new JSONArray(sp.getString("agents","[]"));
        }
        catch (JSONException jse){
            Log.e("JSON-ERR",jse.toString());
        }
    }

    public void setCurrentActivity(Activity actvity){
                currentActivity =actvity;
           }

    public Activity getCurrentActivity(){
        return currentActivity;
           }

    public JSONArray getBookings(){
        return bookings;
    }

    public void setUmmoId(String id){
        wrapper.setUmmoId(id);
    }

    public boolean isNetWorkStateConnected(){
        return netWorkStateConnected;
    }

    public void setAlarms(){
       // alarmMgr.
    }

    public long getTime(){
        long time;
        String[] ids = TimeZone.getAvailableIDs(2 * 60 * 60 * 1000);
        if (ids.length == 0)
            return 0;
        SimpleTimeZone pdt = new SimpleTimeZone(2 * 60 * 60 * 1000, ids[0]);
        Calendar calendar = new GregorianCalendar(pdt);
        time = calendar.getTime().getTime();
        return time;
    }

    public boolean getDayElapsed(int _year, int _month, int _day,int _hour, int _min){
        long time;
        String[] ids = TimeZone.getAvailableIDs(2 * 60 * 60 * 1000);
        if (ids.length == 0)
            return true;
        SimpleTimeZone pdt = new SimpleTimeZone(2 * 60 * 60 * 1000, ids[0]);
        Calendar calendar = new GregorianCalendar(pdt);
        Calendar calendar1 = new GregorianCalendar(pdt);
        calendar1.set(_year,_month,_day,_hour,_min);
        return calendar.getTime().after(calendar1.getTime());
    }

    public void editBooking(JSONObject _booking){
        Log.e("Editing BOOKING",_booking.toString());
        mSocket.emit("edit-booking",_booking);
        mSocket.on("edit-booking", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("Edited Booking",args[0].toString());
                getMyBookings();
                fireUmmoEvent("edit-booking",args[0]);
            }
        });
    }

    public boolean bookingElapsed(JSONObject booking){
        boolean elapsed = true;
        try {
           elapsed=getDayElapsed(booking.getInt("year"),booking.getInt("month"),
                   booking.getInt("day"),booking.getInt("hour_start"),booking.getInt("min_start"));
        }catch (JSONException jse){
            Log.e("JSON-ERR",jse.toString());
        }
        return elapsed;
    }

    @Override
    public void onPollData(Object obj) {
        Log.e(TAG+" onPollData", "Object ->"+obj.toString());
        long secs = new Date().getTime();
        try{
            JSONObject queue = new JSONObject(obj.toString());
            queue.accumulate("age",secs);
            int index=-1;

            for(int j=0;j<joinedQs.size();j++){
                if(joinedQs.get(j).getJSONObject("queue").getString("_id").contentEquals(queue.getJSONObject("queue").getString("_id"))){
                    index=j;
                    Log.e(TAG+" onPollData","QUEUE");
                    break;
                }
            }
            if(index>-1){
                if((queue.getInt("pos")<10)&&(queue.getInt("pos")!=joinedQs.get(index).getInt("pos"))){
                    int mins = queue.getInt("pos")*(queue.getJSONObject("queue").getInt("ttdq"));
                    final int ttdq_mins = mins/(60*1000);
                    final int ttqd_secs = mins/(1000)-ttdq_mins*60;
                    String ttdq_secs = ttqd_secs<10?"0"+ttqd_secs:""+ttqd_secs;
                    Log.e(TAG+" onPollData","Less than four");
                    String body = posString+queue.getInt("pos") + "  Expect to wait: "+ttdq_mins+" mins";
                    createNotification(queue.getJSONObject("queue").getString("qName"),body,queue.getInt("pos"));
                }

                joinedQs.set(index,queue);
            }
            else {
                if(queue.getInt("pos")<10){
                    int mins = queue.getInt("pos")*(queue.getJSONObject("queue").getInt("ttdq"));
                    final int ttdq_mins = mins/(60*1000);
                    final int ttqd_secs = mins/(1000)-ttdq_mins*60;
                    String ttdq_secs = ttqd_secs<10?"0"+ttqd_secs:""+ttqd_secs;
                    Log.e(TAG+" onPollData","Less than four");
                    String body = posString+queue.getInt("pos") + "  Expect to wait: "+ttdq_mins+" mins";
                    createNotification(queue.getJSONObject("queue").getString("qName"),body,queue.getInt("pos"));
                }

                Log.e(TAG+" onPollData","JOINED-QUEUE");
                joinedQs.add(queue);
            }
            fireUmmoEvent("joinedQs","JINEDQS");
        }

        catch (JSONException jse){
            Log.e(TAG+" JSONException","LOADING MY-QUES"+jse.toString());
        }
    }

    public boolean isJoined(String vq){
        boolean _joined = false;
        for (int i=0;i<joinedQs.size();i++){
            try {
                if(joinedQs.get(i).getJSONObject("queue").getString("_id").startsWith(vq)) _joined=true;
            }
            catch (JSONException jse){
                Log.e("IS Q JOINED",jse.toString());
            }

        }

        Log.e("JOINED",""+_joined);

        return _joined;
    }

    public JSONObject getBookingById(String id){
        JSONObject foundBooking=null;
        for(int i=0;i<bookings.length();i++){
            try{
                foundBooking = bookings.getJSONObject(i).getString("_id").equals(id)?bookings.getJSONObject(i):foundBooking;
                Log.e(TAG+" getBookingById","COMPARE "+id+" with "+bookings.getJSONObject(i).getString("_id"));
            }catch (JSONException jse){
                Log.e("JSONERR",jse.toString());
            }
        }
        return foundBooking;
    }

    public void getMyBookings(){
        if(!isRegistered()) return;
        mSocket.emit("booker-bookings",getUmmoId());
        mSocket.on("booking-done", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG+" getMyBookings","Booking done ->"+args[0].toString());
                    try {
                        JSONObject bkd = (JSONObject)args[0];
                        String id = bkd.getString("_id");
                        if(doneBookingIds.contains(id)&&getBookingById(id)!=null)return;
                        doneBookingIds.add(id);
                        //bookingDoneNotification(id,bkd.getString("service"));
                        fireUmmoEvent("booking-done",args[0]);
                    }catch (JSONException jse){
                        Log.e(TAG+" getMyBookings", "jse->"+jse.toString());
                    }
                mSocket.emit("booker-bookings",getUmmoId());
            }
        });
    }

    public void exitQueue(String qid){
        JSONObject qdata = new JSONObject();
        try{
            qdata.accumulate("que",qid);
            qdata.accumulate("usr",getUmmoId());
            mSocket.emit("leave-queue",qdata);
        }

        catch (JSONException jse){
            Log.e("Exit-QUEUE",jse.toString());
        }
    }

    public void dqNotify(){
        mSocket.on("notify", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject queue = (JSONObject)args[0];
                    if(queue.getInt("pos")<10){
                        int mins = queue.getInt("pos")*(queue.getJSONObject("queue").getInt("ttdq"));
                        final int ttdq_mins = mins/(60*1000);
                        final int ttqd_secs = mins/(1000)-ttdq_mins*60;
                        String ttdq_secs = ttqd_secs<10?"0"+ttqd_secs:""+ttqd_secs;
                        Log.e(TAG+" dqNotify","Pos - Less than four");
                        String body = posString+queue.getInt("pos") + "  Expect to wait: "+ttdq_mins+" mins";
                        createNotification(queue.getJSONObject("queue").getString("qName"),body,queue.getInt("pos"));
                    }
                }
                catch (JSONException jse){
                    Log.e(TAG+" NOTIFICATION-SE",jse.toString());
                }
            }
        });
    }

/*
    public JSONObject getSelectedQ() {
        return selectedQ;
    }
*/

/*
    public boolean isSelectedQJoined(){
        return joinedSelectedQ;
    }
*/

    /*public  void cancelBooking(String _bid){
        mSocket.emit("cancel-booking",_bid);
        mSocket.once("cancel-booking", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                getMyBookings();
            }
        });
    }
*/
    /*public void getBookingServices(){
        SocketClass.emit("booking-services", "booking-services", new SocketClass.Response() {
            @Override
            public Object ready(Object val) {

                JSONArray res ;

                Log.e("ARGS[0]",val.toString());

                try {
                    res=new JSONArray(val.toString());
                    bookingServices=res;
                    fireUmmoEvent("booking-services",res);
                }catch (JSONException jse){
                    Log.e("JSON-ERR",getClass().getCanonicalName()+jse.toString());
                }
                return null;
            }
        });
    }*/

    /*public void joinSelectedQ(){
        joinedSelectedQ=false;
        try {
            if(selectedQ!=null){
                JSONObject jobj = new JSONObject();
                jobj.accumulate("qman",getUmmoId());
                jobj.accumulate("qid",selectedQ.getJSONObject("queue").getString("_id"));
                mSocket.on("joinedq", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.e("JOIN","JOINED SELECTED-Q ");
                        joinedSelectedQ=true;
                        lastJoinedQ=(JSONObject)args[0];
                        //joinedQs.add(lastJoinedQ);
                        try {
                            int index=-1;
                            for(int j=0;j<joinedQs.size();j++){
                                if(joinedQs.get(j).getJSONObject("queue").getString("_id").contentEquals(lastJoinedQ.getJSONObject("queue").getString("_id"))){
                                    index=j;
                                    Log.e("STARTS-WITH","QUEUE");
                                    break;
                                }
                            }
                            if(index>-1){
                                joinedQs.set(index,lastJoinedQ);
                            }
                            else {

                                Log.e("Adding","JOINED-QUEUE");
                                joinedQs.add(lastJoinedQ);
                            }
                        }
                        catch (JSONException jse){

                        }
                    }
                });
                mSocket.emit("joinq",jobj);
            }
        }
        catch (JSONException jse){
            Log.e("JOIN-Q",jse.toString());
        }
    }*/

    public String getUmmoId(){
        return wrapper.getUmmoId();
    }


    public JSONArray agentBookings(String _id){
        JSONArray bookings=null;
        for(int i=0;i<agents.length();i++){
            try {
                if (agents.getJSONObject(i).getString("_id").equals(_id)){
                    bookings=agents.getJSONObject(i).getJSONArray("bookings");
                }
            }catch (JSONException jse){
                Log.e("JSON-ERR",jse.toString());
            }
        }
        return bookings;
    }

    public JSONArray getCategories() {
        if(categories==null){
            return null;
        }
        return categories;
    }

    public void setCategories(JSONArray categories) {
        this.categories = categories;
    }

    public void setQman(){
        app=this;
    }

    public static qman getQman(){
        return app;
    }

    public void appExit(){
        System.exit(0);
    }

    /*public void qManRegister(){
        register(wrapper.getUname(), wrapper.getFirstName(), wrapper.getSurName(), wrapper.getCellNum());
    }*/

    public void refreshCategories(){
        if (isRegistered()){ //Initialize Registered Qman server
            mSocket.emit("categories",getUmmoId());
        }
    }

    public void refreshBookings(){
        if (isRegistered()){
            mSocket.emit("qman",getUmmoId());
        }
    }

    public void getQueue(String qid){
        selectedQ=null;
        mSocket.on("got-queue", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                selectedQ=(JSONObject)args[0];
                //Log.e("GOT QUEUE",args[1].toString());
            }
        });
        mSocket.emit("get-queue",qid);

    }

    public void sendFeedback(String feedBackString){
        try {
            JSONObject feed = new JSONObject();
            feed.accumulate("user",getUmmoId());
            feed.accumulate("feedBack",feedBackString);
            mSocket.emit("feedback",feed);
            Log.e(TAG+" sendFeedBack ","Feedback -> "+feed.toString());
        }
        catch (JSONException jse){
            Log.e("FEEDBACK-ERR",jse.toString());
        }
    }

    public void poll(final String vq){
        try {
            final String user = getUmmoId();
            final FormPoster formPoster = new FormPoster(new URL(getResources().getString(R.string.SERVER_URL)+"/myqueue"));
            formPoster.add("vq", vq);
            formPoster.add("id", user);
            formPoster.add("data", "data");
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        InputStream is = formPoster.post();
                        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

                        final StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+
                        String line;
                        while ((line = rd.readLine()) != null) {
                            response.append(line);
                            response.append('\r');
                        }
                        rd.close();
                        final String objString = response.toString();
                        onPollData(objString);
                        fireUmmoEvent("connect","connect");
                        NotificationManager mNotificationManager = (NotificationManager) qman.this.getSystemService(Context.NOTIFICATION_SERVICE);
                        assert mNotificationManager != null;
                        mNotificationManager.cancel(qman.OFFLINE_NOTIFICATION_ID);
                    } catch (final IOException ioe) {
                        currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                              //  Toast.makeText(currentActivity,"Check network",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            });
            thread.start();
        }
        catch (MalformedURLException me) {
            Log.e("Network Exception", me.toString());
        }
    }

    public void createBooking(JSONObject _booking){
        Log.e("CREATING-BOOKING",_booking.toString());
        mSocket.emit("create-booking",_booking);
        mSocket.on("create-booking", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("Created Booking",args[0].toString());
                getMyBookings();
                try {
                    getAgentBookings(((JSONObject)args[0]).getString("agent"));
                }catch (JSONException jse){
                    Log.e("CREATE-BOOKING",jse.toString());
                }
                fireUmmoEvent("create-booking",args[0]);

            }
        });
        //mSocket.on("event",args->Log.e("ARGS",args[0].toString()));
    }

    /*public void checkBookingTimes(){
        for(int i=0;i<bookings.length();i++){
            try {
                bookings.getJSONObject(i).getString("start");
            }
            catch (JSONException jse){
                Log.e("CHECK-BOOKINGS",jse.toString());
            }
        }
    }*/

    public void checkQueAges(){
        final long maxAge=120000;
        for(int i=0; i<joinedQs.size();i++){
            try {
                if ((new Date().getTime()-joinedQs.get(i).getLong("age"))>maxAge) poll(joinedQs.get(i).getJSONObject("queue").getString("_id"));
            }
            catch (JSONException jse){
                Log.e(TAG+"checkQueAges","NO-AGE"+jse.toString());
            }
        }
    }

    public void getAgentBookings(String _agent_id){
        mSocket.emit("agent-bookings",_agent_id);

        Log.e("GETTING Agent Bookings",_agent_id);
        mSocket.on("agent-bookings", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONArray bookings = (JSONArray) args[0];
                if (bookings.length()<1) return;
                for (int i=0;i<agents.length();i++){
                    try{

                        if(agents.getJSONObject(i).getString("_id").equals(bookings.getJSONObject(0).getJSONObject("agent").getString("_id"))){
                            agents.getJSONObject(i).put("bookings",bookings);
                            fireUmmoEvent("agent_bookings"+agents.getJSONObject(i).getString("_id"),null);
                           // Log.e("FRESH BOOKINGS",bookings.toString());
                        }
                    }catch (JSONException jse){
                        Log.e("JSE",jse.toString());
                    }
                }
                saveAgents();
                }
        });
    }

    private void crunchServicesData(){
        String sharedPrefFile = "com.ummo.prod.user";
        final SharedPreferences sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);

        /*Fetching SharedPreferences data*/
        try {
            categories = new JSONArray(sharedPreferences.getString("SAVED_CATEGORIES", "[]"));
            Log.e(TAG+" crunchService", "Loaded Saved Cats->"+categories);

            for (int k = 0; k<categories.length(); k++) {
                JSONObject _category = categories.getJSONObject(k);
                Log.e(TAG+ " crunchService", "Category("+k+")->"+_category);

                JSONArray serviceProviders = _category.getJSONArray("providers");

                for (int l = 0; l<serviceProviders.length(); l++) {
                    JSONObject _service = serviceProviders.getJSONObject(l);
                    Log.e(TAG+ " crunchService", "ServiceProvider("+l+")->"+serviceProviders);
                    Log.e(TAG+" crunchService", "Service("+l+")->"+_service);

                    /*ArrayList<String> q_ids = new ArrayList<>();
                    String serviceName = _service.getString("name");
                    String serviceId = _service.getString("_id");
                    String serviceCat = _service.getString("category");
                    JSONArray queues = _service.getJSONArray("queues");

                    for (int m = 0; m<queues.length(); m++) {
                        String q_id = queues.getJSONObject(m).getString("_id");
                        q_ids.add(q_id);
                    }

                    Service service = new Service();
                    service.service_id = serviceId;
                    service.service_name = serviceName;
                    service.service_cat = serviceCat;
                    service.service_queue = q_ids;

                    appDb.serviceModel().insertService(service);
                    Log.e(TAG+" crunchService", "Room-Service (Q_IDS)->"+q_ids);*/

                }
            }
        } catch (JSONException jse){
            Log.e(TAG+" crunchService", "Err... Error->"+jse.toString());
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appDb = AppDatabase.getInMemoryDatabase(getApplicationContext());
        crunchServicesData();
        //App core init
        AccountKit.initialize(getApplicationContext());
        setQman();
        wrapper =new qManWrapper(this);

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.InAppAlert)
                .setNotificationReceivedHandler(new OSNotificationReceivedHandler(this))
                .setNotificationOpenedHandler(new OSNotificationOpenedHandler(this))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        OneSignal.enableVibrate(true);

        //Log.e(TAG+" onCreate"," Registered ->"+String.valueOf(wrapper.isRegistered()));

        //IMA Start checking the ages right here
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                while(true){
                    checkQueAges();

                    try {
                        synchronized (this){
                            wait(1000);
                        }
                    }
                    catch (InterruptedException ie){
                        Log.e("coitus INTERUPTUS",ie.toString());
                    }
                }
            }
        });

        thread.start();

        String url = getString(R.string.SERVER_URL);

        //Properties headers = new Properties();
        //headers.setProperty("uid",getUmmoId());

        IO.Options opts = new IO.Options();
        opts.query="uid="+getUmmoId();

        Log.e(TAG+" Application","App Created ON ->"+url);
        { //Networking Init
            try {
                mSocket = IO.socket(url,opts);
                SocketClass.init();
                mSocket.on("dequed", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        joinedQs.clear();
                        fireUmmoEvent("dequed",args[0]);
                        Log.e(TAG+" Dequed on"," args" +args[0]);
                    }
                });
                mSocket.on("qman_data-ready", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                       // Log.e(TAG+" onCreate", "Q-man Data Ready ->"+args[0].toString());
                        QueueJoinWrapper.loadData();
                        BookingWraper.initMyBookings();
                        AgentServiceWrapper.fetchAgentServices();

                        Object userObject = args[0];
                        Log.e(TAG+" onCreate", "UserObject->"+userObject);

                        try {
                            JSONObject userData = new JSONObject(userObject.toString());
                            String userName = userData.getJSONObject("fullName").getString("firstName");
                            String userSName = userData.getJSONObject("fullName").getString("surName");
                            String userContact = userData.getString("cell");

                            User user = new User();
                            user.name = userName;
                            user.surname = userSName;
                            user.cell_num = userContact;
                            //ArrayList<JSONObject> joined_queues = qman.getQman().joinedQs;
                            //Creating an instance of a User in Room
                            appDb.userModel().createUser(user);
                            //TODO:Undo

                            Log.e(TAG+" onCreate", "Room-User.joined_queues->"+user.cell_num);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG+" onCreate", "Err... Error!->"+e);
                        }
                    }
                });

                getMyBookings();
                dqNotify();
//                getBookingServices();
                mSocket.on("my-q", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {

                        JSONObject queue = (JSONObject)args[0];
                        Log.e(TAG+" onCreate", "My Queue is ->" + queue);

                        long secs = new Date().getTime();

                        try{
                            queue.accumulate("age",secs);
                            int index=-1;
                            for(int j=0;j<joinedQs.size();j++){
                                if(joinedQs.get(j).getJSONObject("queue").getString("_id")
                                        .contentEquals(queue.getJSONObject("queue").getString("_id"))){
                                    index=j;
                                    Log.e(TAG+" onCreate","QUEUE ->"+queue);
                                    break;
                                }
                            }
                            if(index>-1){
                                joinedQs.set(index,queue);
                                Log.e(TAG+" onCreate", "my queue is:" + queue);
                            }
                            else {

                                Log.e("Adding","JOINED-QUEUE");
                                joinedQs.add(queue);
                            }

                            fireUmmoEvent("joinedQs","JINEDQS");
                            Log.e(TAG, "my queue is: " + queue);
                        }

                        catch (JSONException jse){
                            Log.e(TAG+" JSONException","LOADING MY QUEUES"+jse.toString());
                        }
                        Log.e(TAG, "my queue is:" + queue);
                        //joinedSelectedQ=true;
                        //lastJoinedQ=(JSONObject)args[0];
                    }
                });

                mSocket.on("agents", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.e(TAG+" Agents", args[0].toString());
                        agents=(JSONArray)args[0];
                        for(int i=0;i<agents.length();i++){
                            try {
                                getAgentBookings(agents.getJSONObject(i).getString("_id"));
                            }catch (JSONException jse){
                                Log.e("JSON ERROR",jse.toString());
                            }
                        }
                        fireUmmoEvent("agents",agents);
                    }
                });

                mSocket.on("qman-init", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject me = (JSONObject)args[0];
                        if(me==null) return;
                        try{
                            JSONArray mine = me.getJSONArray("joinedQs");
                            for(int i =0;i<mine.length();i++){
                                Log.e(TAG+" Qman-Init", " Mine ->"+mine.getJSONObject(i)
                                        .getJSONObject("queue").getString("_id"));
                                JSONObject dat = new JSONObject();
                                dat.accumulate("qid",mine.getJSONObject(i).getJSONObject("queue")
                                        .getString("_id"));
                                dat.accumulate("qman",getUmmoId());
                                mSocket.emit("my-pos",dat);
                            }
                        }
                        catch (JSONException jse){
                            Log.e("Extracting Joined Qs",jse.toString());
                        }

                    }
                });
                mSocket.on("categories", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        setCategories((JSONArray) args[0]);
                        fireUmmoEvent("categories", args[0]);
                        //Log.e("Loaded Cats", "category"+args[0]);
                        /*
                        Set SharePreferences for Categories here
                        */
                        //SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
                        //preferencesEditor.putString("SAVED_CATEGORIES", args[0].toString()).apply();
                    }
                });
                Log.e("APPLICATION","Init Code");
                mSocket.connect();

                mSocket.on("message", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.e("Received Message",args[0].toString());
                    }
                });
                mSocket.on("connect", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        if (isRegistered()){ //Initialize Registered Qman server
                            mSocket.emit("qman",getUmmoId());
                            
                            mSocket.on("room-test", new Emitter.Listener() {
                                @Override
                                public void call(Object... args) {
                                    Log.e(TAG+" connectEvent","Room Test: PASSED!");
                                }
                            });
                            loadAgents();
                            loadBookings();
                        }
                        Log.e(TAG+" onCreate","Socket.IO Connected");
                        String cell = PreferenceManager.getDefaultSharedPreferences(qman.this).getString(getString(R.string.PREF_USER_CELLNUMBER),"766666");
                        mSocket.emit("cellNum",cell);
                        fireUmmoEvent("connect","connect");
                        NotificationManager mNotificationManager = (NotificationManager) qman.this.getSystemService(Context.NOTIFICATION_SERVICE);
                        assert mNotificationManager != null;
                        mNotificationManager.cancel(qman.OFFLINE_NOTIFICATION_ID);
                                netWorkStateConnected=true;

                    }
                });

                mSocket.on("error", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                //Toast.makeText(qman.this,"PLEASE CHECK YOUR CONNECTION",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
                mSocket.on("disconnect", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.e(TAG+" onCreate","Socket.IO Disconnected");
                      //  fireUmmoEvent("disconnected","disconnected");
                        netWorkStateConnected=false;
                        lastOfflineTime= getTime();
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    synchronized (this){
                                        wait(30000);
                                        if(!netWorkStateConnected){
                                            Log.e(TAG+" disconnectEvent","Firing disconnected Ummo event");
                                            fireUmmoEvent("disconnected","disconnected");
                                            generalNotification();
                                            }
                                        }
                                    }
                                catch (InterruptedException ie){
                                    Log.e("coitus INTERUPTUS",ie.toString());
                                    }
                                }
                            });

                                thread.start();

                        //generalNotification("You Are Offline","Please restore connection");
                    }
                });
            } catch (URISyntaxException e) {
                Log.e("SOCKET",e.toString());
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public void setName(String name){
        wrapper.setFirstName(name);
        wrapper.setUname(name);
        wrapper.save();
    }

    public String getName(){
        return wrapper.getFirstName();
    }

    public String getUname(){
        return wrapper.getUname();
    }

    public String getSurname(){
        return wrapper.getSurName();
    }

    public void setSurname(String surname){
        wrapper.setSurName(surname);
        wrapper.save();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public boolean isRegistered(){
        return wrapper.isRegistered();
    }

    public void registerEventListener(String event,UmmoEventListener listener){
        ummoEventsHashMap.put(event,listener);
    }

    public void deRegisterUmmoEvent(String eventKey){
        ummoEventsHashMap.remove(eventKey);
    }

    public void fireUmmoEvent(String event,Object data){
        Object object = ummoEventsHashMap.containsKey(event)?ummoEventsHashMap.get(event).run(data):null;
    }

    public void register(String uname, String firstName, final String surName, String cellNumb, String playerId){
        Log.e(TAG+" register","Started Register");
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().putString("uname",uname).putString("firstName",firstName).putString("surName",surName).putString("cellNumb",cellNumb).apply();
        mSocket.on("qman-register", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try{
                    JSONObject data = (JSONObject) args[0];
                    setName(data.has("fullName")?data.getJSONObject("fullName").getString("firstName"):getName());
                    setSurname(data.has("fullName")?data.getJSONObject("fullName").getString("surName"):getSurname());
                    setCellNumb(data.has("cell")?data.getString("cell"):getCellNumb());
                    setUmmoId(data.has("_id")?data.getString("_id"):getUmmoId());
                    wrapper.setRegistered();
                    wrapper.save();

                    Log.e(TAG+" register","DATA"+data.toString());
                    fireUmmoEvent("register",data);
                    mSocket.emit("qman",getUmmoId());
                }
                catch (JSONException jse){
                    Log.e(TAG+" JSONException","REGISTRATION ERR "+jse.toString());
                }
            }
        });

        try {
            JSONObject object =new JSONObject();
            JSONObject fullName = new JSONObject();

            object.accumulate("name",uname);
            fullName.accumulate("firstName",firstName);
            fullName.accumulate("surName",surName);
            object.accumulate("cell",cellNumb);
            object.accumulate("fullName",fullName);
            object.accumulate("playerId",playerId);
            Log.e(TAG+" register","Registration Object"+object.toString());
            mSocket.emit("qman-register",object.toString());
        }
        catch (JSONException jse){
            Log.e("JSON-ERROR",jse.toString());
        }
    }

    public void setCellNumb(String cell){
        wrapper.setCellNum(cell);
        wrapper.save();
    }

    public String getCellNumb(){
        return wrapper.getCellNum();
    }

    //TODO: Revise Notifications
    public void createNotification(String title, String body, int length){
        //Context context = getBaseContext();
        Intent resultIntent = new Intent(this, Main_Activity.class);
        resultIntent.putExtra("FAB","FAB");

        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        TaskStackBuilder stackBuilder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder = TaskStackBuilder.create(this);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            assert stackBuilder != null;
            stackBuilder.addParentStack(Main_Activity.class);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder.addNextIntent(resultIntent);
        }

        PendingIntent resultPendingIntent = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
        }

        long[] pattern = {500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122};

        long[] patterns = new long[length*pattern.length*6];
        for (int i=0;i<length;i++){
            System.arraycopy(pattern,0,patterns,pattern.length*i,pattern.length);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "General")
                .setSmallIcon(R.mipmap.ummo_new).setContentTitle(title)
                .setAutoCancel(true)
                //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(patterns)  /*From pattern I changed to patterns*/
                .setContentText(body)
                .setContentIntent(resultPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(MESSAGE_NOTIFICATION_ID, mBuilder.build());

        /*NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, mBuilder.build());*/
    }

    public void dequeuedNotification(String serviceProvider){
        Intent resultIntent = new Intent(this, Main_Activity.class);
        resultIntent.putExtra("FAB","serviceProvider");
        TaskStackBuilder stackBuilder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder = TaskStackBuilder.create(this);
        }
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            assert stackBuilder != null;
            stackBuilder.addParentStack(BookingsActivity.class);
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            assert stackBuilder != null;
            stackBuilder.addNextIntent(resultIntent);
        }
        PendingIntent resultPendingIntent = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        String KEY_TEXT_FEEDBACK = "key_text_feedback";
        String feedbackLabel = "Feedback";

        android.support.v4.app.RemoteInput remoteInput = new android.support.v4.app.RemoteInput.Builder(KEY_TEXT_FEEDBACK)
                .setLabel(feedbackLabel)
                .build();

        NotificationCompat.Action feedbackAction = new NotificationCompat.Action.Builder(R.drawable.feedback,
                feedbackLabel, resultPendingIntent)
                .addRemoteInput(remoteInput)
                .build();

        /*Notification.Builder mBuilder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mBuilder = new Notification.Builder(this, "serviceDone")
                    .setSmallIcon(R.mipmap.ummo_new)
                    .setContentTitle("Thank you for using Test")
                    .setContentText("How was your service?")
                    .setContentIntent(resultPendingIntent);

            NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
            notificationManagerCompat.notify(OFFLINE_NOTIFICATION_ID, mBuilder.build());
        }*/

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Service Done")
                .setSmallIcon(R.mipmap.ummo_new).setContentTitle("Thank you for using "+serviceProvider+", "+getName()+"...")
                .setAutoCancel(true)
                //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentText("How was your service?")
                //.addAction(feedbackAction)
                .setContentIntent(resultPendingIntent);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(OFFLINE_NOTIFICATION_ID, mBuilder.build());
    }

    //TODO: Find out why this notification is inactive
    public void generalNotification() {
        //Context context = getBaseContext();
        if(joinedQs.size()<1)
            return;

        Intent resultIntent = new Intent(this, Main_Activity.class);
        //resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        TaskStackBuilder stackBuilder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder = TaskStackBuilder.create(this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            assert stackBuilder != null;
            stackBuilder.addParentStack(Main_Activity.class);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder.addNextIntent(resultIntent);
        }

        PendingIntent resultPendingIntent = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Offline")
                .setSmallIcon(R.mipmap.ummo_new).setContentTitle("You are offline...")
                .setAutoCancel(true)
                //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentText(getUname()+" "+ "Please restore your connection");
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.notify(OFFLINE_NOTIFICATION_ID, mBuilder.build());
    }

    /*private long calculateMeanTimes(){
        long meanTimes=0;
        if(offlineTimes.size()==0)return 0;
        for(int i=0;i<offlineTimes.size();i++){
            meanTimes+=offlineTimes.get(i);
        }
        meanTimes/=offlineTimes.size();
        return meanTimes;
    }*/

    /*private long sDeviation(){
        long sSq = 0;
        long mean =calculateMeanTimes();
        for (int i=0;i<offlineTimes.size();i++) {
            long xm = (offlineTimes.get(i)-mean);
            sSq += xm*xm;
        }
        return (long)Math.sqrt(sSq/(offlineTimes.size()-1));
    }*/
}
