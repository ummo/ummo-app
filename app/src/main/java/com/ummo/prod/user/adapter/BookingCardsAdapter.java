package com.ummo.prod.user.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;


import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.view.BookedActivity;
import com.ummo.prod.user.R;
import com.ummo.prod.user.interfaces.UmmoEventListener;
import com.ummo.prod.user.view.RescheduleActivity;
import com.ummo.prod.user.ummoAPI.BookingWraper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by mosaic on 2/27/17.
 **/

public class BookingCardsAdapter extends RecyclerView.Adapter<BookingCardsAdapter.ViewHolder> {
    private List<BookingWraper> mDataset = new ArrayList<>();
    private BookedActivity parent;
    final String projectToken = "67547a5eed2c873e61ccbfd531eb6d26";
    public MixpanelAPI mixpanelAPI;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout bookingCard;
        LinearLayout cancelLy;
        LinearLayout reschedLy;
        public JSONObject data;
        public JSONObject newBooking;
        public LinearLayout editLy;

        ViewHolder(RelativeLayout v) {
            super(v);
            bookingCard = v;
            cancelLy= v.findViewById(R.id.cancel_booking_action_ly);
            reschedLy= v.findViewById(R.id.resched_ly);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public BookingCardsAdapter(List<BookingWraper> myDataset, BookedActivity _parent) {
        mDataset = myDataset;
        parent=_parent;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BookingCardsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_card, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    private void showDayDialog(final ViewHolder vh) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(vh.bookingCard.getContext());
        LayoutInflater inflater = (LayoutInflater) vh.bookingCard.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        Log.e("DATE","CLICKED");
        assert inflater != null;
        final View view = inflater.inflate(R.layout.date_lv, null);
        final DatePicker datePicker = view.findViewById(R.id.datePicker1);

        try {
            int year = Integer.valueOf(vh.newBooking.getString("start").substring(0,4));
            int month = Integer.valueOf(vh.newBooking.getString("start").substring(5,7))-1;
            int day = Integer.valueOf(vh.newBooking.getString("start").substring(8,10));
            Log.e("BOOKINGS-CARDS-ADAPTER",vh.newBooking.getString("start").substring(8,10));
            datePicker.updateDate(year,month,day);
        }catch (JSONException jse){

        }
        //datePicker.updateDate();
        builder.setView(view)
                // Add action buttons
                .setPositiveButton("Start Time", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        Log.e("Show Dialog","START Time");
                        Log.e("BOOKING ELAPSED",""+ qman.getQman().getDayElapsed(datePicker.getYear(),datePicker.getMonth(),datePicker.getDayOfMonth(),0,0));
                        try {
                            vh.newBooking.put("day",datePicker.getDayOfMonth());
                            vh.newBooking.put("month",datePicker.getMonth());
                            vh.newBooking.put("year",datePicker.getYear());
                            Log.e("BOOKING",vh.newBooking.toString());
                        }
                        catch (JSONException jse){
                            Log.e("JSON ERROR",jse.toString());
                        }

                        Log.e("Day Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking));
                        if(qman.getQman().bookingElapsed(vh.newBooking)){
                            Log.e("Booking Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking));

                            new AlertDialog.Builder(vh.bookingCard.getContext())
                                    .setTitle("Selected Date has Elapsed")
                                    .setPositiveButton("Set Date", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            showDayDialog(vh);
                                        }
                                    })
                                    .create().show();
                            return;
                        }
                        showStartTimeDialog(vh);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //LoginDialogFragment.this.getDialog().cancel();
                    }
                }).create().show();
    }

    private void showStartTimeDialog(final ViewHolder vh){
        AlertDialog.Builder builder = new AlertDialog.Builder(vh.bookingCard.getContext());
        LayoutInflater inflater = (LayoutInflater) vh.bookingCard.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        assert inflater != null;
        final View v = inflater.inflate(R.layout.start_time_ly, null);
        final TimePicker timePicker = v.findViewById(R.id.timePicker2);
        try {
            int hr = Integer.valueOf(vh.newBooking.getString("start").substring(11,13));
            int min = Integer.valueOf(vh.newBooking.getString("start").substring(14,16));
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
                timePicker.setHour(hr);
                timePicker.setMinute(min);
                timePicker.setIs24HourView(true);
            }
            else {
                timePicker.setCurrentHour(hr);
                timePicker.setCurrentMinute(min);
            }
        }catch (JSONException jse){
            Log.e("SETTING BOOKING TIME",jse.toString());
        }
        final AppCompatTextView unavailableStart = v.findViewById(R.id.unavailable_times_start);
        String times = "Unavailable times: \n";
        try {
            JSONArray bks = qman.getQman().agentBookings(vh.data.getString("_id"));
            for (int i=0;i<bks.length();i++){
                String t1 =bks.getJSONObject(i).getString("start");
                String t2= bks.getJSONObject(i).getString("end");
                String day = vh.newBooking.getInt("day")>10?""+(vh.newBooking.getInt("day")):"0"+(vh.newBooking.getInt("day"));
                String mnt=vh.newBooking.getInt("month")+1>10?""+(1+vh.newBooking.getInt("month")):"0"+(1+vh.newBooking.getInt("month"));
                times+=t1.contains(vh.newBooking.getInt("year")+"-"+mnt+"-"+day)?t1.substring(t1.indexOf("T")+1,t1.indexOf("T")+6)+" => "+t2.substring(t2.indexOf("T")+1,t2.indexOf("T")+6)+"\n":"";
            }
        }
        catch (JSONException jse){
            Log.e("JSON ERR",jse.toString()+vh.data.toString());
        }
        unavailableStart.setText(times);
        Log.e("DATE","CLICKED");
        builder.setView(v)
                // Add action buttons
                .setPositiveButton("End Time", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...

                        try {
                            vh.newBooking.put("hour_start",android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M?timePicker.getHour():timePicker.getCurrentHour());
                            vh.newBooking.put("min_start",android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M?timePicker.getMinute():timePicker.getCurrentMinute());
                            if(vh.data.has("bookings")){
                                JSONArray _bookings=vh.data.getJSONArray("bookings");
                                for(int i=0;i<_bookings.length();i++){
                                    if(new TimeUtils(vh.newBooking).overlaps(_bookings.getJSONObject(i))){
                                        new AlertDialog.Builder(vh.bookingCard.getContext()).setTitle("Selected Time Slot is not available")
                                                .setPositiveButton("Start Time", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        showStartTimeDialog(vh);
                                                    }
                                                })
                                                .create().show();
                                        return;
                                    }
                                }
                            }
                        }
                        catch (JSONException jse){
                            Log.e("JSON ERR",jse.toString());
                        }
                        Log.e("Booking Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking)+vh.newBooking.toString());
                        if(qman.getQman().bookingElapsed(vh.newBooking)){
                            Log.e("Booking Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking));
                            new AlertDialog.Builder(vh.bookingCard.getContext())
                                    .setTitle("Selected Time has Elapsed")
                                    .setPositiveButton("Set Date", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            showStartTimeDialog(vh);
                                        }
                                    })
                                    .create().show();
                            return;
                        }
                        showEndTime(vh);

                    }
                })
                .setNegativeButton("Day", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //LoginDialogFragment.this.getDialog().cancel();

                        showDayDialog(vh);
                    }
                }).create().show();

    }

    private void showEndTime(final ViewHolder vh){
        AlertDialog.Builder builder = new AlertDialog.Builder(vh.bookingCard.getContext());
        LayoutInflater inflater = (LayoutInflater) vh.bookingCard.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        int maxTime=0;
        Log.e("DATE","CLICKED");
        assert inflater != null;
        final View v = inflater.inflate(R.layout.layout_end_time, null);
        final TimePicker timePicker = v.findViewById(R.id.timePicker1);
        timePicker.setIs24HourView(true);
        final TextView endTimeLimitTv= v.findViewById(R.id.end_time_limit);
        try {
            int hr = Integer.valueOf(vh.newBooking.getString("end").substring(11,13));
            int min = Integer.valueOf(vh.newBooking.getString("end").substring(14,16));
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
                timePicker.setHour(hr);
                timePicker.setMinute(min);
            }
            else {
                timePicker.setCurrentHour(hr);
                timePicker.setCurrentMinute(min);
            }
        }catch (JSONException jse){
            Log.e("SETTING BOOKING TIME",jse.toString());
        }
        try {
            maxTime =vh.data.has("endTime")? new TimeUtils(vh.newBooking).nearestTime(vh.data.getJSONArray("bookings"), vh.data.getJSONObject("endTime").getInt("hour") * 60 + vh.data.getJSONObject("endTime").getInt("min")):new TimeUtils(vh.newBooking).nearestTime(vh.data.getJSONArray("bookings"), 23 * 60 + 59);
            endTimeLimitTv.setText("You have up to "+new Integer(maxTime/60)+":"+maxTime%60);
        }
        catch (JSONException jse){
            Log.e("SELECTEBLETREE",jse.toString());
        }
        if(!vh.data.has("bookinhs")){
            if(vh.data.has("endTime")&&maxTime==0){
                try {
                    endTimeLimitTv.setText("You have up to "+vh.data.getJSONObject("endTime").getInt("hour")+":"+vh.data.getJSONObject("endTime").getInt("min"));
                }
                catch (JSONException jse){
                    Log.e("SELECTABLE TREE",jse.toString());
                }
            }

        }

        builder.setView(v)
                // Add action buttons
                .setPositiveButton("SERVICE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...

                        showDialogServiceInput(vh);
                        try {
                            vh.newBooking.put("hour_end",android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M?timePicker.getHour():timePicker.getCurrentHour());
                            vh.newBooking.put("min_end",android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M?timePicker.getMinute():timePicker.getCurrentMinute());

                        }
                        catch (JSONException jse){
                            Log.e("JOSON ERR",jse.toString());
                        }

                        if(qman.getQman().bookingElapsed(vh.newBooking)){
                            Log.e("Booking Elapsed",""+ qman.getQman().bookingElapsed(vh.newBooking));

                            new AlertDialog.Builder(vh.bookingCard.getContext())
                                    .setTitle("Selected Time has Elapsed")
                                    .setPositiveButton("Set Date", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            showDayDialog(vh);
                                        }
                                    })
                                    .create().show();
                        }

                        //showDayDialog();
                    }
                })
                .setNegativeButton("START TIME", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //LoginDialogFragment.this.getDialog().cancel();
                        showStartTimeDialog(vh);
                    }
                }).create().show();
    }

    private void showDialogServiceInput(final ViewHolder vh){
        AlertDialog.Builder builder = new AlertDialog.Builder(vh.bookingCard.getContext());
        AlertDialog dialog;
        LayoutInflater inflater = (LayoutInflater) vh.bookingCard.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        assert inflater != null;
        final View v = inflater.inflate(R.layout.layout_service_input, null);
        final AppCompatEditText servEt = v.findViewById(R.id.serviceInput);
        try {
            servEt.setText(vh.newBooking.getString("service"));
        }catch (JSONException jse){
            Log.e("SETTING_SERVICE",jse.toString());
        }
        builder.setView(v)
                .setPositiveButton("BOOK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int id) {
                        String service = "";
                        try{
                            vh.newBooking.put("agent",vh.newBooking.getJSONObject("agent").getString("_id"));
                            vh.newBooking.put("booker", qman.getQman().getUmmoId());
                            vh.newBooking.put("service",servEt.getText().toString());
                            //showLoadingDialog(vh);
                            qman.getQman().editBooking(vh.newBooking);
                        }
                        catch (JSONException jse){
                            Log.e("JSON ERR",jse.toString());
                        }
                        Log.e("BOOKING",vh.newBooking.toString());
                    }
                })
                .setNegativeButton("END TIME", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showEndTime(vh);
                    }
                });
        builder.show();
    }

    /*private void showLoadingDialog(final ViewHolder vh){
        final AlertDialog.Builder builder = new AlertDialog.Builder(vh.bookingCard.getContext());
        LayoutInflater inflater = (LayoutInflater) vh.bookingCard.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        assert inflater != null;
        final View v = inflater.inflate(R.layout.layout_loading, null);
        final AlertDialog dialog=builder.setView(v).setTitle("Saving your booking").create();
        dialog.show();
        qman.getQman().registerEventListener("edit-booking", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                ((Activity)vh.bookingCard.getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
                return null;
            }
        });
    }*/

    class TimeUtils{
        JSONObject newBooking;
        TimeUtils(JSONObject _newBooking){
            newBooking = _newBooking;
        }

        public boolean overlaps(JSONObject _booking){ //This Funtion Checks if the Start of a new Booking does not overlap _booking
            boolean ovl=true;
            boolean ovr=true;
            boolean sameday = true;
            try{
                String _start=_booking.getString("start");
                String _end=_booking.getString("end");
                Log.e("OVERLAP",_end.substring(11,13));
                ovr=(newBooking.getInt("hour_start")*60+newBooking.getInt("min_start"))<(60*new Integer(_end.substring(11,13))+new Integer(_end.substring(14,16)));//+Integer.getInteger(_end.substring(14,16));*/
                ovl=(newBooking.getInt("hour_start")*60+newBooking. getInt("min_start"))>(60*new Integer(_start.substring(11,13))+new Integer(_start.substring(14,16)));
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                simpleDateFormat.setTimeZone(TimeZone.getDefault());
                try {
                    Calendar cal = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    cal.setTime(simpleDateFormat.parse(_booking.getString("start")));
                    cal2.set(newBooking.getInt("year"),newBooking.getInt("month"),newBooking.getInt("day"));
                    sameday=cal.get(Calendar.DAY_OF_YEAR)==cal2.get(Calendar.DAY_OF_YEAR);
                }catch (ParseException pse){
                    Log.e("PARSE-ERR",getClass().getCanonicalName()+pse.toString());
                }
            }
            catch (JSONException jse){
                Log.e("ERROR",jse.toString());
            }
            return ovr&&ovl&&sameday;
        }

        int nearestTime(JSONArray _bookings, int endTime){
            int nextStart=endTime;
            Log.e("NEXTSTART",""+nextStart);
            try {
                for(int i = 0;i<_bookings.length();i++){
                    JSONObject booking = _bookings.getJSONObject(i);
                    String hour = booking.getString("start").substring(11,13);
                    String min = booking.getString("start").substring(14,16);
                    int mins=0;

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    simpleDateFormat.setTimeZone(TimeZone.getDefault());
                    try {
                        Calendar cal = Calendar.getInstance();
                        Calendar cal2 = Calendar.getInstance();
                        cal.setTime(simpleDateFormat.parse(booking.getString("start")));
                        cal2.set(newBooking.getInt("year"),newBooking.getInt("month"),newBooking.getInt("day"));
                        mins = cal.get(Calendar.HOUR_OF_DAY)*60+cal.get(Calendar.MINUTE);
                        Log.e("TIME",""+cal2.get(Calendar.DAY_OF_YEAR));
                        int bookingStart = newBooking.getInt("hour_start")*60+newBooking.getInt("min_start");
                        nextStart=(bookingStart<mins)&&(mins<nextStart)&&cal.get(Calendar.DAY_OF_YEAR)==cal2.get(Calendar.DAY_OF_YEAR)?mins:nextStart;
                    }catch (ParseException pse){
                        Log.e("PARSERR",getClass().getCanonicalName()+pse.toString());
                    }
                }
            }
            catch(JSONException jse){
                Log.e("SELECTABLE-TREE",jse.toString());
            }
            return nextStart;
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        try {
            holder.data= qman.getQman().agents.getJSONObject(position);
        }
        catch (JSONException jse){
            Log.e("JSONERR",jse.toString());
        }
        TextView bookingHead = holder.bookingCard.findViewById(R.id.booking_header_tv);
        TextView agentContactDetails = holder.bookingCard.findViewById(R.id.booker_contact_tv);
        TextView emailTextView= holder.bookingCard.findViewById(R.id.booker_email_tv);
        TextView dateTimeTextView= holder.bookingCard.findViewById(R.id.booker_date_time_tv);
        /*holder.editLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.newBooking=mDataset.get(position);
                try{
                    holder.data=holder.newBooking.getJSONObject("agent");
                    Log.e("",holder.data.toString());
                    holder.newBooking.put("year",0);
                    holder.newBooking.put("month",0);
                    holder.newBooking.put("day",0);
                    holder.newBooking.put("hour_end",0);
                    holder.newBooking.put("hour_start",23);
                    holder.newBooking.put("min_end",0);
                    holder.newBooking.put("min_start",59);
                }
                catch (JSONException jse){
                    Log.e("ERROR",jse.toString());
                }
                Log.e("EDITED BOOKING",mDataset.get(position).toString());
                showDayDialog(holder);
            }
        });*/

        holder.reschedLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookingWraper.setSelectedBooking(mDataset.get(position));
                v.getContext().startActivity(new Intent(v.getContext(), RescheduleActivity.class));
                /*try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("rescheduleBooking", "onClick");
                    mixpanelAPI.track("Pending Bookings", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }*/
            }
        });

        holder.cancelLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(parent).setTitle("Cancel Booking")
                        .setMessage("By Clicking OK, you can cancel this booking")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mDataset.get(position).cancelBooking();
                                //new AlertDialog.Builder(parent).setTitle("Canceling your bookig")
                                        //.setView(R.layout.layout_booking_loading)
                                        //.create().show();
                                /*try {
                                    JSONObject user_lytics = new JSONObject();
                                    user_lytics.put("cancelBooking", "onClick");
                                    mixpanelAPI.track("Pending Bookings", user_lytics);
                                } catch (JSONException e) {
                                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                }*/
                            }
                        })
                        .create().show();
            }
        });
            String dateString=mDataset.get(position).getStart().getTime().toString();

            dateTimeTextView.setText(dateString.substring(0,16));
            bookingHead.setText(mDataset.get(position).getService());
            emailTextView.setText(mDataset.get(position).getAgent().getEmail());
            agentContactDetails.setText(mDataset.get(position).getAgent().getFirstName());
    }
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
