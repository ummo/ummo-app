package com.ummo.prod.user.db.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ummo.prod.user.db.room.entities.QueueJoin;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface QueueJoinDao {
    //Create
    @Insert(onConflict = REPLACE)
    void joinQueue(QueueJoin queueJoin);

    //Read
    @Query("select * from queuejoin")
    List<QueueJoin> listQueueJoin();

    @Query("select position from queuejoin where join_id = :j_id and user_join = :u_id")
    int getQueueJoinPosition(String j_id, String u_id);

    //Update
    @Update
    void updateQueueJoin(QueueJoin queueJoin);

    //Delete
    @Delete
    void deleteQueueJoin(QueueJoin queueJoin);

    @Query("delete from QueueJoin where join_id = :j_id")
    void deleteQueueJoinById(String j_id);
}