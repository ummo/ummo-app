package com.ummo.prod.user.ummoAPI;

import android.content.Context;
import android.util.Log;

import com.ummo.prod.user.qman;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by mosaic on 5/4/17.
 **/

public class QueueJoinWrapper {
    private String userCell;
    private int position;
    private String numCode;
    private Date joinTime;
    private String feedback;
    private String _id;
    private int ttdq;
    private String serviceName;
    private QueueWrapper queue;
    private String q_id;
    private final static String TAG = "Q_JoinWrapper";

    public int getTtdq() {
        return ttdq;
    }

    private void setTtdq(int ttdq) {
        this.ttdq = ttdq;
    }

    public QueueWrapper getQueueWrapper() {
        return queue;
    }

    public void setQueueWrapper(QueueWrapper queueWrapper) {
        this.queue = queueWrapper;
    }

    public static final List<QueueJoinWrapper> qmanQJoins = new ArrayList<>();

    private String getServiceName() {
        return serviceName;
    }

    private static Socket socket= qman.getSocket();

    private static DataReadyCallback callback=null;
    private DataReadyCallback qUpdate;
    private static DataReadyCallback onDequeue;
    private static DataReadyCallback onExit;

    public static void setOnExit(DataReadyCallback _dr){
        onExit=_dr;
    }

    public static void setOnDequeue(DataReadyCallback onDequeue) {
        QueueJoinWrapper.onDequeue = onDequeue;
    }

    public void setqUpdate(DataReadyCallback _cb){
        qUpdate=_cb;
    }

    public static int getIndexById(String _id){
        int index =-1;
        for(int i = 0;i<qmanQJoins.size();i++){
            index=qmanQJoins.get(i).get_id().equals(_id)?i:index;
        }
        return index;
    }

    public static void setDataReadyCallback(DataReadyCallback _cb){
        callback = _cb;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }

    public String getUserCell() {
        return userCell;
    }

    private static void pullJoin(String j_id){
        for(QueueJoinWrapper join:qmanQJoins){
            if(join.get_id().equals(j_id)) qmanQJoins.remove(join);
        }
    }

    public QueueJoinWrapper(final JSONObject object){

        try {
            setUserCell(object.getString("userCell"));
            setPosition(object.getInt("possition"));
            Object queue = object.get("queue");
            //Log.e(TAG+" Constr.", "Queue->"+queue.toString());
            if(object.has("ttdq"))
                setTtdq(object.getInt("ttdq")/(60*1000));

            if(queue.toString().length()==24){
                q_id=queue.toString();
            }else {
                setQueue(new QueueWrapper((JSONObject) queue));
            }
            setNumCode(object.getString("_id").substring(object.getString("_id").length()-4));
            set_id(object.getString("_id"));
        }catch (JSONException jse){
            Log.e(TAG+" JSON-ERR",jse.toString());
        }

        socket.on("qj" + _id, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    Log.e(TAG+" QJW QJ", "Args[0]->"+Arrays.toString(args));
                    JSONObject object1=(JSONObject) args[0];
                    setPosition(object1.getInt("possition"));
                    setTtdq(object1.getInt("ttdq")/(60*1000));
                    String body = "Position "+ getPosition() + ", Expect to wait about: "+getTtdq()+" mins.";
                    if (getQueue()!=null)
                        qman.getQman().createNotification(getQueue().getqName(),body,getPosition());
                    if(qUpdate!=null)
                        qUpdate.exec(this);
                }catch (JSONException jse){
                    Log.e(TAG+" QJ JSE",jse.toString()+args[0].toString());
                }
                //QueueJoinWrapper.this=new QueueJoinWrapper((JSONObject) args[0]);
            }
        });

       serviceName=getServiceProvider();
    }

    public static void loadData(){
        //Log.e("LOADING","QUEUE JOINS");
        //context=_context;

        socket.emit("load_qman_queue_joins", qman.getQman().getCellNumb());
        socket.on("qman_queue_joins", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                qmanQJoins.clear();

                try {
                    JSONArray array=(JSONArray)args[0];
                    //Log.e("LOADED JOINS",array.toString());
                    for (int i=0;i<array.length();i++) {
                        JSONObject object = array.getJSONObject(i);
                        Log.e(TAG+" loadData","User Queue Joins (object)->"+object);
                        //TODO:
                        qmanQJoins.add(new QueueJoinWrapper(object));
                    }
                    if(callback!=null) {
                        callback.exec(qmanQJoins);
                        Log.e(TAG+" loadData","Callback executed");
                    }else {
                        Log.e(TAG+" loadData","Callback is null");
                    }

                }catch (JSONException jse){
                    Log.e(TAG+" JSON ERR",getClass().getCanonicalName()+jse.toString());
                }
            }
        });

        //TODO:Check for clean execution below
        socket.on("dequeue", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if(onDequeue!=null)onDequeue.exec(args[0]);
                Log.e(TAG+" Dequeue","Args[0]->"+args[0].toString());
                QueueJoinWrapper _tempQ = new QueueJoinWrapper((JSONObject) args[0]);
                qman.getQman().dequeuedNotification(_tempQ.getServiceName());
                pullJoin(_tempQ._id);
               // loadData();
            }
        });
    }

    private String getServiceProvider(){
        String serviceName = "";
        try {
            JSONArray array= qman.getQman().getCategories();
            if (array==null) return serviceName;
            for (int i=0; i<array.length();i++){ //BIG FUCKEN NPE HERE!!!
                JSONObject object = array.getJSONObject(i);
                JSONArray providers = object.getJSONArray("providers");
                for (int j = 0; j < providers.length(); j++){
                    final JSONObject providerObj = providers.getJSONObject(j);
                    JSONArray services = providerObj.getJSONArray("queues");
                    for (int k = 0; k < services.length(); k++){
                        if(services.getJSONObject(k).getString("_id").equals(queue==null?q_id:queue.get_id())){
                            Log.e(TAG+" getServP","PROVIDER "+providerObj.getString("name"));
                            return providerObj.getString("name");
                        }
                    }
                }
            }
        }catch (JSONException jse){
            Log.e("QUEUE-JW",jse.toString());
        }
        return serviceName;
    }

    public void exitQueue(){
        SocketClass.emit("queue_exit", _id, new SocketClass.Response() {
            @Override
            public Object ready(Object val) {
                Log.e(TAG+" exitQueue","LEFT QUEUE!");
                if(onExit!=null) {
                    onExit.exec(val);
                }else {
                    Log.e(TAG+" exitQueue","onExit is null");
                }
                loadData();
                return null;
            }
        });
    }

    private void setUserCell(String userCell) {
        this.userCell = userCell;
    }

    public QueueWrapper getQueue() {
        return queue;
    }

    public void setQueue(QueueWrapper queue) {
        this.queue = queue;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getNumCode() {
        return numCode;
    }

    /**
     * This function might be useful soon
     **/
    public static boolean isQueueJoined(String _id){
        boolean joined=false;
        for(int i =0; i<qmanQJoins.size();i++){
            boolean same = qmanQJoins.get(i).getQueue().get_id().contentEquals(_id);
            joined = same?same:joined;
            Log.e(qmanQJoins.get(i).getQueue().get_id()," => " +_id );
        }
        return joined;
    }

    private void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Date joinTime) {
        this.joinTime = joinTime;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
