package com.ummo.prod.user.view;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.ummo.prod.user.R;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.ummoAPI.Agent;
import com.ummo.prod.user.ummoAPI.AgentServiceWrapper;
import com.ummo.prod.user.ummoAPI.BookingWraper;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RescheduleActivity extends AppCompatActivity implements MonthLoader.MonthChangeListener, WeekView.EventClickListener, WeekView.EventLongPressListener{
    private WeekView mWeekView;
    private Calendar start;
    private int month;
    private TextView durationTv;
    private Agent agent = null;
    private final BookingWraper selectedBooking = BookingWraper.getSelectedBooking();
    private final long duration = (selectedBooking.getEnd().getTimeInMillis()-selectedBooking.getStart().getTimeInMillis())/60000;
    List<WeekViewEvent> events = new ArrayList<>();
    private int finalDuration =0;
    // private List<WeekViewEvent> = new
    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        Log.e("EVENT",event.toString()+eventRect.toString());
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> monthEvents=new ArrayList<>();
        month=month==0?newMonth:month;
        mWeekView.invalidate();
        for(WeekViewEvent event:events){
            if(event.getStartTime()==null){
                Log.e("NULL",event.toString());
                return null;
            }
            if(event.getStartTime().get(Calendar.MONTH)==newMonth)monthEvents.add(event);
        }
        mWeekView.invalidate();
        return newMonth==month?events:new ArrayList<WeekViewEvent>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reschedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Log.e("BOOKINGSNO",""+ selectedBooking.getStart().getTime().toString());
        setTitle("Click the start time");
        final CardView cv = (CardView)findViewById(R.id.fab);
        TextView cancelTv = (TextView)findViewById(R.id.cancel_sched);
        final TextView saveTv = (TextView) findViewById(R.id.save_sched);
        durationTv = (TextView) findViewById(R.id.duration_tv);
        cancelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cv.animate().setStartDelay(100).translationY(-500).setDuration(1000);
                finish();
            }
        });


        mWeekView = (WeekView) findViewById(R.id.weekView);
        mWeekView.setOnEventClickListener(this);
        mWeekView.setMonthChangeListener(this);
        mWeekView.setEventLongPressListener(this);
        Log.e("NOW",""+Calendar.getInstance().getTime().toString());

        cv.animate().translationY(-500);
        cv.setVisibility(View.VISIBLE);
        initAvailableBookings();
        events.add(new WeekViewEvent(0,selectedBooking.getService(),selectedBooking.getStart(),selectedBooking.getEnd()));
        mWeekView.notifyDatasetChanged();
        goToSelectedBooking();
        Log.e("BOOKING TIME",selectedBooking.getStart().getTime().toString());

        mWeekView.setEmptyViewClickListener(new WeekView.EmptyViewClickListener() {
            @Override
            public void onEmptyViewClicked(Calendar time) {
                int minRounded=time.get(Calendar.MINUTE)%5<3?(time.get(Calendar.MINUTE)-time.get(Calendar.MINUTE)%5):(time.get(Calendar.MINUTE)+(5-time.get(Calendar.MINUTE)%5));
                time.set(Calendar.MINUTE,minRounded);
                cv.animate().setStartDelay(100).translationY(0).setDuration(1000);
                cv.setVisibility(View.VISIBLE);
                AppCompatTextView tv = (AppCompatTextView) findViewById(R.id.date_tv);
                tv.setText(time.getTime().toString().substring(0,16));
                start=time;
                long _duration =duration;
                Calendar _end = Calendar.getInstance();
                _end.setTime(start.getTime());
                _end.add(Calendar.MINUTE,(int)duration);
                for(BookingWraper booking:agent.getBookings()){
                    long _d = (booking.getStart().getTimeInMillis()-start.getTimeInMillis())/60000;
                    Log.e("_D",""+_d);
                    _duration=(!booking.get_id().equals(selectedBooking.get_id())&&(_d>0&&_d<duration))?_d:_duration;
                }
                Log.e("BOOKING DURATION",_duration+"");
                _end.setTime(start.getTime());
                _end.add(Calendar.MINUTE,(int)_duration);
                durationTv.setText(""+_duration);
                WeekViewEvent event = new WeekViewEvent(0,selectedBooking.getService(),start,_end);
                initAvailableBookings();
                events.add(event);
                mWeekView.invalidate();
                mWeekView.notifyDatasetChanged();
                finalDuration = (int)_duration;
            }
        });

        saveTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedBooking.setStart(start);
                Calendar _end = Calendar.getInstance();
                _end.setTime(start.getTime());
                _end.add(Calendar.MINUTE,finalDuration);
                selectedBooking.setEnd(_end);
                selectedBooking.reschedule();
                final AlertDialog.Builder _builder = new AlertDialog.Builder(RescheduleActivity.this);
                LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                final View view = inflater.inflate(R.layout.layout_booking_loading, null);
                _builder.setView(view);
                final AlertDialog dialog1 = _builder.create();
                dialog1.show();
                BookingWraper.setOnLoadedBookings(new DataReadyCallback() {
                    @Override
                    public Object exec(Object obj) {
                        //startActivity(new Intent(RescheduleActivity.this,BookedActivity.class));
                        finish();
                        return null;
                    }
                });
                Log.e("FINAL RESCHED", selectedBooking.getStart().getTime().toString()+" => "+selectedBooking.getEnd().getTime().toString());
            }
        });

        final LinearLayout ly = (LinearLayout)findViewById(R.id.next_ly);
        ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(RescheduleActivity.this);
                LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                final View view = inflater.inflate(R.layout.qman_end_time, null);
                final EditText durationEt = (EditText)view.findViewById(R.id.durationEt);
                //final EditText nameEt = (EditText)view.findViewById(R.id.nameEt);
                //final EditText cellEt = (EditText) view.findViewById(R.id.cellEt);
                final EditText serviceEt  = (EditText) view.findViewById(R.id.serviceEt);
                builder.setView(view)
                        .setPositiveButton("save", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String service = serviceEt.getText().toString();
                                Calendar end = Calendar.getInstance();
                                Date _start = start.getTime();
                                end.setTime(_start);
                                String dur = durationEt.getText().toString();
                                if(service.length()==0||dur.length()==0){
                                    Snackbar.make(cv,"PLEASE USE VALID VALUES",Snackbar.LENGTH_SHORT).show();
                                    return;
                                }
                                int duration = Integer.valueOf(dur);
                                end.add(Calendar.MINUTE,duration);
                                WeekViewEvent event = new WeekViewEvent(1234,service,"",start,end);
                                events.add(event);
                                mWeekView.notifyDatasetChanged();
                                //BookingWraper.createBooking(start,end,service,);
                                final AlertDialog.Builder _builder = new AlertDialog.Builder(RescheduleActivity.this);
                                LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                                final View view = inflater.inflate(R.layout.layout_booking_loading, null);
                                _builder.setView(view);
                                _builder.create().show();
                                BookingWraper.setOnBookingCreated(new DataReadyCallback() {
                                    @Override
                                    public Object exec(Object obj) {
                                        qman.getQman().getMyBookings();
                                        //startActivity(new Intent(RescheduleActivity.this,BookedActivity.class));
                                        finish();
                                         return null;
                                    }
                                });
                                cv.animate().setStartDelay(100).translationY(300);
                            }
                        })
                        .create().show();
            }
        });
    }


    public void goToSelectedBooking(){
        Calendar cal = Calendar.getInstance();
        Calendar ca2 = Calendar.getInstance();
        ca2.setTime(selectedBooking.getEnd().getTime());
        cal.setTime(selectedBooking.getStart().getTime());
        String name = selectedBooking.getService();
        Log.e("CAL",cal.getTime().toString());
        mWeekView.goToDate(ca2);
        mWeekView.goToHour(cal.get(Calendar.HOUR_OF_DAY));
        Log.e("CAL",cal.getTime().toString());
    }

    public void initAvailableBookings(){
        events.clear();

        for(AgentServiceWrapper service: AgentServiceWrapper.getServices()){
            for(Agent _agent:service.getAgents()){
                agent=_agent.get_id().equals(selectedBooking.getAgentId())?_agent:agent;
            }
        }
        if (agent==null) {
            Toast.makeText(this, "AGENT DATA CURUPTED< PLEASE REFRESH", Toast.LENGTH_LONG)
                    .show();
            finish();
        }

        for (int i = 0; i <agent.getBookings().size(); i++){
            BookingWraper bk = agent.getBookings().get(i);
            Log.e("SERVICE ",bk.getService());
            if(!bk.get_id().equals(selectedBooking.get_id()))
            events.add(new WeekViewEvent(i,"Booked",bk.getStart(),bk.getEnd()));
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mWeekView.notifyDatasetChanged();
            }
        });
    }

}
