package com.ummo.prod.user.ummoAPI;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mosaic on 5/4/17.
 */

public class Agent {
    private String _id;
    private String firstName;
    private String surName;
    private String email;
    private String uname;
    private String ver_code;
    private String password;
    private String cell;
    private Date startTime;
    private Date endTime;
    private Date activated;
    private String managedQ;
    private List<BookingWraper> bookings = new ArrayList<>();
    private List<Agent> agents = new ArrayList<>();

    public void setBookings(List<BookingWraper> bookings) {
        bookings = bookings;
    }

    public static void fetchAgentServices(){

    }

    public List<BookingWraper> getBookings() {
        return bookings;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getVer_code() {
        return ver_code;
    }

    public void setVer_code(String ver_code) {
        this.ver_code = ver_code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getActivated() {
        return activated;
    }

    public void setActivated(Date activated) {
        this.activated = activated;
    }

    public String getManagedQ() {
        return managedQ;
    }

    public void setManagedQ(String managedQ) {
        this.managedQ = managedQ;
    }

    public Agent(){
    }

    public Agent(JSONObject object){
       try {
            setFirstName(object.getJSONObject("fullName").getString("firstName"));
            setSurName(object.getJSONObject("fullName").getString("surName"));
            set_id(object.getString("_id"));
            setEmail(object.getString("email"));
            setCell(object.getString("cell"));
            if (object.has("bookings")){
                JSONArray _bookings = object.getJSONArray("bookings");
                for(int i=0;i<_bookings.length();i++){
                    bookings.add(new BookingWraper(_bookings.getJSONObject(i),this));
                }
            }
        }catch (JSONException jse){
            Log.e("AGENTERR",jse.toString());
        }
    }
    
}
