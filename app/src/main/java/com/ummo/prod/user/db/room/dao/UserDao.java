package com.ummo.prod.user.db.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ummo.prod.user.db.room.entities.User;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDao {
    //Create
    @Insert(onConflict = REPLACE)
    void createUser(User user);

    //Read
    @Query("select * from user")
    List<User> loadUserInfo();

    /*@Query("select user_name and user_surname from user where cell_num == :contact")
    List<User> loadNameAndSurname(String contact);*/

    //Update
    @Update
    void updateUser(User user);

    //Delete
    @Query("delete from user where user_contact == :contact")
    void deleteUser(String contact);

}