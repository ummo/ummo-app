package com.ummo.prod.user.db.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import com.ummo.prod.user.db.room.ArrayListConverter;
import com.ummo.prod.user.db.room.entities.Service;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
@TypeConverters(ArrayListConverter.class)
public interface ServiceDao {

    //Create
    @Insert(onConflict = REPLACE)
    void insertService(Service service);

    //Read
    @Query("select * from service")
    List<Service> loadAllServices();

    //Udate
    @Update
    void updateService(Service service);

    //Delete
    @Delete
    void deleteService(Service service);
}