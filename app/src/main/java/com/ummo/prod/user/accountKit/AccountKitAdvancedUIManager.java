package com.ummo.prod.user.accountKit;

import android.app.Fragment;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.View;

import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.ui.AdvancedUIManager;
import com.facebook.accountkit.ui.ButtonType;
import com.facebook.accountkit.ui.LoginFlowState;
import com.facebook.accountkit.ui.TextPosition;
import com.ummo.prod.user.PlaceholderFragment;
import com.ummo.prod.user.R;

/**
 * Created by barnes on 7/3/16.
 */
public class AccountKitAdvancedUIManager implements AdvancedUIManager, Parcelable
{
    private static final int ACTION_BAR_HEIGHT = 40;
    private static final int BODY_HEIGHT = 80;
    private static final int FOOTER_HEIGHT = 120;
    private static final int HEADER_HEIGHT = 80;

    private final ButtonType confirmButton;
    private final ButtonType entryButton;
    private AccountKitError error;
    private AdvancedUIManagerListener listener;
    private final TextPosition textPosition;

    public AccountKitAdvancedUIManager(
            final ButtonType confirmButton,
            final ButtonType entryButton,
            final TextPosition textPosition) {
        this.confirmButton = confirmButton;
        this.entryButton = entryButton;
        this.textPosition = textPosition;
    }

    @Override
    @Nullable
    public Fragment getActionBarFragment(final LoginFlowState state) {
        final PlaceholderFragment fragment = getPlaceholderFragment(
                state,
                ACTION_BAR_HEIGHT,
                "Action Bar");
        if (fragment != null) {
            fragment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    listener.onBack();
                }
            });
        }
        return fragment;
    }

    @Override
    @Nullable
    public Fragment getBodyFragment(final LoginFlowState state) {
        return getPlaceholderFragment(state, BODY_HEIGHT, "Body");
    }

    @Override
    @Nullable
    public ButtonType getButtonType(final LoginFlowState state) {
        switch (state) {
            case PHONE_NUMBER_INPUT:
                return entryButton;
            case EMAIL_INPUT:
                return entryButton;
            case CODE_INPUT:
                return confirmButton;
            default:
                return null;
        }
    }

    @Override
    @Nullable
    public Fragment getFooterFragment(final LoginFlowState state) {
        return getPlaceholderFragment(state, FOOTER_HEIGHT, "Footer");
    }

    @Override
    @Nullable
    public Fragment getHeaderFragment(final LoginFlowState state) {
        if (state != LoginFlowState.ERROR) {
            return getPlaceholderFragment(state, HEADER_HEIGHT, "Header");
        }
        final String errorMessage = getErrorMessage();
        if (errorMessage == null) {
            return PlaceholderFragment.create(HEADER_HEIGHT, R.string.error_message);
        } else {
            return PlaceholderFragment.create(HEADER_HEIGHT, errorMessage);
        }
    }

    @Override
    @Nullable
    public TextPosition getTextPosition(final LoginFlowState state) {
        return textPosition;
    }

    @Override
    public void setAdvancedUIManagerListener(final AdvancedUIManagerListener listener) {
        this.listener = listener;
    }

    @Override
    public void onError(final AccountKitError error) {
        this.error = error;
    }

    private String getErrorMessage() {
        if (error == null) {
            return null;
        }

        final String message = error.getUserFacingMessage();
        if (message == null) {
            return null;
        }

        return message;
    }

    @Nullable
    private PlaceholderFragment getPlaceholderFragment(
            final LoginFlowState state,
            final int height,
            final String suffix) {
        final String prefix;
        switch (state) {
            case PHONE_NUMBER_INPUT:
                prefix = "Custom Phone Number ";
                break;
            case EMAIL_INPUT:
                prefix = "Custom Email ";
                break;
            case EMAIL_VERIFY:
                prefix = "Custom Email Verify ";
                break;
            case SENDING_CODE:
                prefix = "Custom Sending Code ";
                break;
            case SENT_CODE:
                prefix = "Custom Sent Code ";
                break;
            case CODE_INPUT:
                prefix = "Custom Code Input ";
                break;
            case VERIFYING_CODE:
                prefix = "Custom Verifying ";
                break;
            case VERIFIED:
                prefix = "Custom Verified ";
                break;
            case RESEND:
                prefix = "Custom Resend ";
                break;
            case ERROR:
                prefix = "Custom Error ";
                break;
            default:
                return null;
        }
        return PlaceholderFragment.create(height, prefix.concat(suffix));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(confirmButton != null ? confirmButton.name() : null);
        dest.writeString(entryButton != null ? entryButton.name() : null);
        dest.writeString(textPosition != null ? textPosition.name() : null);
    }

    public static final Creator<AccountKitAdvancedUIManager> CREATOR
            = new Creator<AccountKitAdvancedUIManager>() {
        @Override
        public AccountKitAdvancedUIManager createFromParcel(final Parcel source) {
            String s = source.readString();
            final ButtonType confirmButton = s == null ? null : ButtonType.valueOf(s);
            s = source.readString();
            final ButtonType entryButton = s == null ? null : ButtonType.valueOf(s);
            s = source.readString();
            final TextPosition textPosition = s == null ? null : TextPosition.valueOf(s);
            return new AccountKitAdvancedUIManager(
                    confirmButton,
                    entryButton,
                    textPosition);
        }

        @Override
        public AccountKitAdvancedUIManager[] newArray(final int size) {
            return new AccountKitAdvancedUIManager[size];
        }
    };
}