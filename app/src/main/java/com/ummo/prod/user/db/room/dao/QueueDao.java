package com.ummo.prod.user.db.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.ummo.prod.user.db.room.entities.Queue;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;

@Dao
public interface QueueDao {

    //Create
    @Insert(onConflict = IGNORE)
    void insertQueue(Queue queue);

    //Read
    @Query("select * from queue")
    List<Queue> listAllQueues();


    /*//Update
    @Query("update position from QueueJoin ")
    void updateQueue(String queue_id, int position);*/

    //Delete
    @Delete
    void deleteQueue(Queue queue);
}