package com.ummo.prod.user.holder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.Main_Activity;
import com.ummo.prod.user.R;
import com.ummo.prod.user.db.Db;
import com.ummo.prod.user.db.room.AppDatabase;
import com.ummo.prod.user.db.room.entities.QueueJoin;
import com.ummo.prod.user.view.fragment.HomeFragment;
//import com.ummo.prod.user.ui.fragment.HomeFragment$$ViewBinder;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;
import com.ummo.prod.user.ummoAPI.QueueJoinWrapper;
import com.ummo.prod.user.ummoAPI.QueueWrapper;
import com.unnamed.b.atv.model.TreeNode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cn.refactor.lib.colordialog.ColorDialog;

import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

/**
 * Created by barnes on 8/6/15.
 **/
public class SelectableItemHolder extends TreeNode.BaseNodeViewHolder<String> {
    private TextView tvValue;
    private CheckBox nodeSelector;
    private Button exit_q_menu;
    private String selectHeader;
    private String tabTitle;
    private int i = -1;
    private HomeFragment parent;
    private int tabPos = 0;
    public List<String> qsJoinedNum = null;
    private List<String> qNameList = null;
    public List<String> qNameServiceId = null;
    private List<String> qJoinedQid = null;
    private List<String> qName_List;
    private String qsJoined;
    private Db db;
    private JSONObject vq;
    private final String projectToken = "9fbc2fa7ce24a7538fcfb0b6fbb3ebb3";
    public MixpanelAPI mixpanel;
    private static final String TAG = "SelectableItem";
    private static AppDatabase appDb;

    private void showAllModeDialog(final QueueWrapper queue, final SweetAlertDialog pdialog){
        ColorDialog dialog = new ColorDialog(context);
        dialog.setTitle(queue.getqName());
        dialog.setAnimationEnable(true);
        dialog.setLocationText(queue.getLocation());
        dialog.setLengthText(""+queue.getLength());
        dialog.setTimeText((queue.getTtdq()==0||queue.getLength()==0)?"Not available yet"
                :(queue.getTtdq()/(1000*60))*queue.getLength()+ " mins");
        dialog.setDocumentText(queue.getqRequirements());
        dialog.setCancelable(false);
        //final int fl = Integer.getInteger(final_length);
        //dialog.setContentImage(getResources().getDrawable(R.mipmap.sample_img));
        dialog.setPositiveListener(context.getResources().getString(R.string.dialog_delete),
                new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                dialog.dismiss();
                QueueWrapper.joinSelectedQueue();
                successDialog(queue.getLength(),queue,context);
            }
        }).setNegativeListener(context.getResources().getString(R.string.dialog_cancel),
                new ColorDialog.OnNegativeListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {
                        dialog.dismiss();
                        pdialog.dismiss();
                        nodeSelector.setChecked(false);
                    }
                }).show();
    }

    private void successDialog(final int final_length, final QueueWrapper queue, final Context context) {
        final SweetAlertDialog pDialog;
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText(selectHeader)
                .setContentText("Joining...");
        pDialog.show();

        //Init AppDb
        appDb = AppDatabase.getInMemoryDatabase(getApplicationContext());

        QueueWrapper.setJoinCallBack(new DataReadyCallback() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public Object exec(Object obj) {
                Log.e(TAG+" successD","Object Returned->"+obj);
                //Storing Queue-Join instance in Room
                createQueueJoin(obj);

                Random r = new Random();
                int i1 = r.nextInt(101 - 1) + 1;
                final int qpos = final_length + 1;
                final String pos = Integer.toString(i1);
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.setTitleText("Queued in...")
                                .setContentText("You have joined the " + selectHeader + " queue, your position is #" + qpos)
                                .setConfirmText("OK")
                                .showCancelButton(false)
                                .setCancelClickListener(null)
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        mixpanel= MixpanelAPI.getInstance(getApplicationContext(), projectToken);

                                        try {
                                            JSONObject user_lytics = new JSONObject();
                                            user_lytics.put("State", "onEnqueue");
                                            mixpanel.track("Queue Joined", user_lytics);
                                        } catch (JSONException e) {
                                            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                                        }
                                        //parent.joinSelectedQ();
                                        assert queue != null;
                                        tabTitle = selectHeader + " " + queue.getqName();
                                        pDialog.dismiss();
                                        nodeSelector.setChecked(true);
                                        //exit_q_menu.setVisibility(View.VISIBLE);
                                        nodeSelector.setClickable(false);
                                        final String qpos_string = Integer.toString(qpos);
                                        //((SelectableTreeFragment_dead) c).getUser().updateJoinedQs();
                                        Intent i = new Intent(context, Main_Activity.class);
                                        //i.putExtra("FAB","bk_id");
                                        //Log.e("++tabPos", "" + tabPos);
                                        context.startActivity(i);
                                        pDialog.dismiss();
                                    }
                                }).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                    }
                });
                return null;
            }
        });

        //pDialog.setCancelable(false);

        final CountDownTimer cdt=new CountDownTimer(800 * 7, 800) {
            public void onTick(long millisUntilFinished) {
                i++;
                switch (i) {
                    case 0:
                        pDialog.getProgressHelper().setBarColor(context.getResources()
                                .getColor(R.color.blue_btn_bg_color));
                        break;
                    case 1:
                        pDialog.getProgressHelper().setBarColor(context.getResources()
                                .getColor(R.color.material_deep_teal_50));
                        break;
                    case 2:
                        pDialog.getProgressHelper().setBarColor(context.getResources()
                                .getColor(R.color.success_stroke_color));
                        break;
                    case 3:
                        pDialog.getProgressHelper().setBarColor(context.getResources()
                                .getColor(R.color.material_deep_teal_20));
                        break;
                    case 4:
                        //this.cancel();
                        pDialog.getProgressHelper().setBarColor(context.getResources()
                                .getColor(R.color.material_blue_grey_80));
                        break;
                    case 5:
                        pDialog.getProgressHelper().setBarColor(context.getResources()
                                .getColor(R.color.warning_stroke_color));
                        break;
                    case 6:
                        pDialog.getProgressHelper().setBarColor(context.getResources()
                                .getColor(R.color.success_stroke_color));
                        break;
                }
            }

            @Override
            public void onFinish() {
                //this.start();
                ++tabPos;
                //Log.e("++tabPos", "" + tabPos);
                //Log.e("Position", "" + tabPos);

            }
        }.start();

    }

    private void createQueueJoin(Object object){
        try {
            JSONObject _queue;
            _queue = new JSONObject(object.toString());
            String join_id = _queue.getString("_id");
            String queue_id = _queue.getString("queue");
            String queue_joiner = _queue.getString("userCell");
            int queue_position = _queue.getInt("possition");

            QueueJoin queueJoin = new QueueJoin();
            queueJoin.join_id = join_id;
            queueJoin.user_join = queue_joiner;
            queueJoin.queue_id = queue_id;
            queueJoin.position = queue_position;

            appDb.queueJoinModel().joinQueue(queueJoin);
            Log.e(TAG+" createQJ", "QueueJoined->"+queueJoin.queue_id+"; User->"
                    +queueJoin.user_join+"; Position->"+queueJoin.position);

            Log.e(TAG+" onExit", "QueueJoin Before Del->"+queueJoin.join_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void dialog() {

        final Context c = context;

        final SweetAlertDialog pDialog;
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText(selectHeader)
                .setContentText("Loading...");
        pDialog.show();
        pDialog.setCancelable(false);
        if(QueueWrapper.getSelectedQueue()!=null){
            final QueueWrapper queue = QueueWrapper.selectedQueue;
            if(queue==null) {
                return;
            }

            //final String text_ = text.toString();
            String requirements="";
            String location = "";
            int mins=0;
            int qLength=0;
            final int ttdq_mins = mins/(60*1000);
            final int ttqd_secs = mins/(1000)-ttdq_mins*60;
            final int final_length=qLength;
            i = -1;
            //Log.e("Que",text.toString());
            String length = String.valueOf(final_length);
            String minutes = String.valueOf(ttdq_mins);
            String seconds = String.valueOf(ttqd_secs);
            pDialog.dismiss();

            showAllModeDialog(
                    queue, pDialog);

        }
        //Extracts Qstats from text

        //End Extracts Qstats from text
        QueueWrapper.setSelectedQCallback(new DataReadyCallback() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public Object exec(Object obj) {
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final QueueWrapper queue = QueueWrapper.selectedQueue;
                        if(queue==null) {
                            return;
                        }

                        //final String text_ = text.toString();
                        String requirements="";
                        String location = "";
                        int mins=0;
                        int qLength=0;
                        final int ttdq_mins = mins/(60*1000);
                        final int ttqd_secs = mins/(1000)-ttdq_mins*60;
                        final int final_length=qLength;
                        i = -1;
                        //Log.e("Que",text.toString());
                        String length = String.valueOf(final_length);
                        String minutes = String.valueOf(ttdq_mins);
                        String seconds = String.valueOf(ttqd_secs);
                        pDialog.dismiss();
                        showAllModeDialog(
                                queue, pDialog);

                    }
                });
                return null;
            }
        });

        new CountDownTimer(800 * 10, 800) {
            public void onTick(long millisUntilFinished) {
                i++;
                switch (i) {
                    case 0:
                        pDialog.getProgressHelper().setBarColor(c.getResources()
                                .getColor(R.color.blue_btn_bg_color));
                        break;
                    case 1:
                        pDialog.getProgressHelper().setBarColor(c.getResources()
                                .getColor(R.color.material_deep_teal_50));
                        break;
                    case 2:
                        pDialog.getProgressHelper().setBarColor(c.getResources()
                                .getColor(R.color.success_stroke_color));
                        break;
                    case 3:
                        pDialog.getProgressHelper().setBarColor(c.getResources()
                                .getColor(R.color.material_deep_teal_20));
                        break;
                    case 4:
                        //this.cancel();
                        pDialog.getProgressHelper().setBarColor(c.getResources()
                                .getColor(R.color.material_blue_grey_80));
                        break;
                    case 5:
                        pDialog.getProgressHelper().setBarColor(c.getResources()
                                .getColor(R.color.warning_stroke_color));
                        break;
                    case 6:
                        pDialog.getProgressHelper().setBarColor(c.getResources()
                                .getColor(R.color.success_stroke_color));
                        break;
                }
            }
            public void onFinish() {
            }
        }.start();
    }

    public SelectableItemHolder(Activity context, JSONObject itemHeader, String qjoined) {
        super(context);
        vq = itemHeader;
        parent = HomeFragment.newInstance();
        db = new Db(context);
        //String s =((SingleFragmentActivity)context).getUser().getName();
        Log.e("qman","s");
        try {
            selectHeader = itemHeader.getString("qName");
        }
        catch (JSONException jse) {
            Log.e("Error",jse.toString());
            selectHeader = "Error, get Help";
        }
        qsJoined = qjoined;
        this.context = context;
    }

    @Override
    public View createNodeView(final TreeNode node, String value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_selectable_item, null, false);
        tvValue = view.findViewById(R.id.node_value);
        tvValue.setText(value);
        tvValue.isClickable();
        tvValue.setTextIsSelectable(true);

        //bottomUp = AnimationUtils.loadAnimation(context, R.anim.bottom_up);
        
        nodeSelector = view.findViewById(R.id.node_selector);
        try {
            nodeSelector.setChecked(QueueJoinWrapper.isQueueJoined(vq.getString("_id")));
            //parent.q_text_name.setText(value);
            //parent.bottomSheet.startAnimation(bottomUp);
            //parent.bottomSheet.setVisibility(View.VISIBLE);
        }
        catch (JSONException jse){
            Log.e("NODE-CREATE",jse.toString());
            Log.e("STRING",vq.toString());
        }
        //exit_q_menu = (Button) view.findViewById(R.id.btn_exit_q_menu);
        nodeSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //db.open();
                //Log.e("qman", parent.getUser().getName());
                //qNameList = db.getQName(tvValue.getText().toString());
                if (nodeSelector.isChecked()) {
                    //dialog(tvValue.getText().toString());

                    try {
                       // parent.setSelectedQ(vq.getString("_id"));
                        //QUser user =parent.getUser(vq);
                        //if(user!=null){
                          //  user.getQ(vq.getString("_id"));
                        QueueWrapper.loadQueue(vq.getString("_id"));

                        dialog();
                        mixpanel= MixpanelAPI.getInstance(getApplicationContext(), projectToken);

                        try {
                            JSONObject user_lytics = new JSONObject();
                            user_lytics.put("State", "onCheckBox");
                            mixpanel.track("Queue Selected", user_lytics);
                        } catch (JSONException e) {
                            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                        }
                        //}

                     //   else {
                       //     Log.e("USER","NULL");
                        //}

                       // parent.setQinfoDialog(SelectableItemHolder.this);
                    }
                    catch (JSONException jse)
                    {
                        //Toast.makeText(parent,"Cannot Get The Q information, Q ID is Broken",Toast.LENGTH_LONG).show();
                    }
                }
                db.close();
            }
        });

        db.open();
        qName_List = db.getQName(tvValue.getText().toString());
        if (!qName_List.isEmpty()) {
            exit_q_menu.setVisibility(View.VISIBLE);
            nodeSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (qName_List.isEmpty()) {
                        nodeSelector.setChecked(false);
                        nodeSelector.setEnabled(true);
                        nodeSelector.setClickable(true);
                        nodeSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                qNameList = db.getQName(tvValue.getText().toString());
                                if (nodeSelector.isChecked()) {
                                    dialog();
                                    //parent.bottomSheet.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    } else if (!qName_List.isEmpty()) {
                        nodeSelector.setChecked(true);
                        nodeSelector.setClickable(false);
                    }
                }
            });
        }
        db.close();

        tvValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvValue.isPressed()) {
                    nodeSelector.setChecked(true);
                }
            }
        });

        if (node.isLastChild()) {
            view.findViewById(R.id.bot_line).setVisibility(View.INVISIBLE);
        }
        return view;
    }

    @Override
    public void toggleSelectionMode(boolean editModeEnabled) {
        nodeSelector.setVisibility(editModeEnabled ? View.VISIBLE : View.GONE);
        //nodeSelector.setChecked();
    }
}