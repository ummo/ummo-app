package com.ummo.prod.user.accountKit;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatEditText;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.ButtonType;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.TextPosition;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.ummo.prod.user.ErrorActivity;
import com.ummo.prod.user.R;
import com.ummo.prod.user.ReverbUIManager;
import com.ummo.prod.user.db.room.AppDatabase;
import com.ummo.prod.user.db.room.entities.User;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.view.BottomNavActivity;
import com.ummo.prod.user.ummoAPI.QUser;
import com.ummo.prod.user.ummoAPI.Interfaces.UserListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import io.sentry.Sentry;

import static com.ummo.prod.user.qman.getQman;

/**
 * Created by barnes on 7/3/16.
 **/
public class AccountKit_MainActivity extends Activity implements UserListener {
    private static final int FRAMEWORK_REQUEST_CODE = 1;

    private Switch advancedUISwitch;
    private ButtonType confirmButton;
    private ButtonType entryButton;
    private int selectedThemeId = -1;
    private BroadcastReceiver switchLoginTypeReceiver;
    private TextPosition textPosition;
    private final static String TAG = "AccountKit";

    AppCompatEditText ummoName, ummoSurname;
    String  cellNum;

    final String projectToken = "67547a5eed2c873e61ccbfd531eb6d26";
    final String firebaseSenderID = "438150256556";
    public MixpanelAPI mixpanel;

    private AppDatabase appDB;

    @Override
    protected void onResume() {
        super.onResume();

/*        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                final TextView userId = (TextView) findViewById(R.id.user_id);
                //userId.setText(account.getId());

                final TextView phoneNumber = (TextView) findViewById(R.id.user_phone);
                final PhoneNumber number = account.getPhoneNumber();
                phoneNumber.setText(number == null ? null : number.toString());

                cellNum = phoneNumber.getText().toString();

                Log.e("VERIFIED","cellNum");

                //final TextView email = (TextView) findViewById(R.id.user_email);
                //email.setText(account.getEmail());

                qman.getQman().setCellNumb(cellNum);
                qman.getQman().qManRegister();

            }

            @Override
            public void onError(final AccountKitError error) {
            }
        });*/
    }

    @Override
    public void userRegistered(String string) {
        final LinkedHashMap<String, Class<?>> listItems = new LinkedHashMap<>();

        //Toast.makeText(this,"Thank you for signing up "+user.getName(),Toast.LENGTH_LONG).show();
        listItems.put("Selectable Nodes", BottomNavActivity.class);
        Class<?> clazz = listItems.values().toArray(new Class<?>[]{})[0];
        Log.e(TAG+" userReg.", "String->"+string);
        mixpanel= MixpanelAPI.getInstance(this, projectToken);

        try {
            JSONObject user_lytics = new JSONObject();
            user_lytics.put("State", "onReturn");
            mixpanel.track("User-Return", user_lytics);
        } catch (JSONException e) {
            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
        }
       // Intent i = new Intent(this, SplashScreen.class);
        // i.putExtra(SingleFragmentActivity.FRAGMENT_PARAM, clazz);
        //this.startActivity(i);
        //this.finish();
        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public void qJoined(String string) {

    }

    @Override
    public void qLeft(String string) {

    }

    @Override
    public void updated(String string) {

    }

    @Override
    public void qReady(String string) {

    }

    @Override
    public void gotJoinedQs(String string) {

    }


    @Override
    public void categoriesReady(String string) {
      /*  categoriesJSON = string;
        Bundle b = getIntent().getExtras();
        //Class<?> fragmentClass = (Class<?>) b.get(FRAGMENT_PARAM);

        Fragment f = Fragment.instantiate(this, fragmentClass.getName());
        f.setArguments(b);
        getFragmentManager().beginTransaction().replace(R.id.fragment, f, fragmentClass.getName()).commit();
          user.makeNotification();*/
    }

    @Override
    public void allQsReady(String string) {
        // qsJSON = string;
        Log.d("qs",string);
    }

    //Error Handlers

    @Override
    public void joinedQsError(String err) {

    }

    @Override
    public void userRegistrationError(String err) {

    }

    @Override
    public void qJoinedError(String err) {

    }

    @Override
    public void qLeftError(String err) {

    }

    @Override
    public void updateError(String err) {

    }

    @Override
    public void categoriesError(String err) {

    }

    @Override
    public void allQError(String err) {

    }

    @Override
    public void qError(String err) {

    }

    //End Overide for Quser

    public void onClick() {
        String ummoAliasName = ummoName.getText().toString();
        String ummoAliasSurname = ummoSurname.getText().toString();

        QUser user = new QUser(this);
        user.register(ummoAliasName, ummoAliasSurname);
        Log.e(TAG+" onClick", "Clicked user->"+ummoAliasName);

        //Espresso User Test Data
//        String espressoTestName = "Espresso Name";
//        String espressoTestSurname = "Espresso Surname";
//        user.register(espressoTestName, espressoTestSurname);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //RoomDB Integration
        appDB = AppDatabase.getInMemoryDatabase(getApplicationContext());
        //populateDb();
        //fetchDb();

        setContentView(R.layout.accountkit_activity_main);
        mixpanel= MixpanelAPI.getInstance(this, projectToken);

        //TODO:Why was this configuration builder abandoned?
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder;

        findViewById(R.id.btn_loginvia_phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLogin(LoginType.PHONE);
                Log.e(TAG+" onCreate_1.0", "Login Via Phone Clicked");
            }
        });

        ummoName = findViewById(R.id.name);
        ummoSurname = findViewById(R.id.surname);

        if (AccountKit.getCurrentAccessToken() != null) {
            showHelloActivity();
            Log.e(TAG+" onCreate_1.1", "AccountKit AccessToken not NULL");
        }

        TextView linkTV = findViewById(R.id.privacyLink);
        linkTV.setClickable(true);
        linkTV.setMovementMethod(LinkMovementMethod.getInstance());
        //TODO: Might need to update these legal doc references;
        String text = "<div>By Signing up, you agree to Ummo's <a href='http://privacy_policy.ummo.xyz/UmmoTermsofUse.pdf'>Terms of Use</a> and <a href='http://privacy_policy.ummo.xyz/'> Privacy Policy </a></div>";
        linkTV.setText(Html.fromHtml(text));
        //TODO: Consider implementing an in-app web-view to load the legal docs;
        linkTV.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(),"Link loading...", Toast.LENGTH_SHORT);
                toast.show();
                Log.e("Clicked link", "Clicked");
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("State", "onLinkClick");
                    mixpanel.track("Clicked Legal Links", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                }
            }
        });

        final Spinner themeSpinner = findViewById(R.id.theme_spinner);
        if (themeSpinner != null) {
            final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                    this,
                    R.array.theme_options,
                    android.R.layout.simple_spinner_dropdown_item);
            themeSpinner.setAdapter(adapter);

            themeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(
                        final AdapterView<?> parent,
                        final View view,
                        final int position,
                        final long id) {
                    switch (position) {
                        case 0:
                            selectedThemeId = R.style.AppLoginTheme;
                            break;
                        case 1:
                            selectedThemeId = R.style.AppLoginTheme_Salmon;
                            break;
                        case 2:
                            selectedThemeId = R.style.AppLoginTheme_Yellow;
                            break;
                        case 3:
                            selectedThemeId = R.style.AppLoginTheme_Red;
                            break;
                        case 4:
                            selectedThemeId = R.style.AppLoginTheme_Dog;
                            break;
                        case 5:
                            selectedThemeId = R.style.AppLoginTheme_Bicycle;
                            break;
                        case 6:
                            selectedThemeId = R.style.AppLoginTheme_Reverb_A;
                            advancedUISwitch.setChecked(true);
                            break;
                        case 7:
                            selectedThemeId = R.style.AppLoginTheme_Reverb_B;
                            advancedUISwitch.setChecked(true);
                            break;
                        case 8:
                            selectedThemeId = R.style.AppLoginTheme_Reverb_C;
                            advancedUISwitch.setChecked(true);
                            break;
                        default:
                            selectedThemeId = -1;
                            break;
                    }
                }

                @Override
                public void onNothingSelected(final AdapterView<?> parent) {
                    selectedThemeId = -1;
                }
            });
        }

        advancedUISwitch = findViewById(R.id.advanced_ui_switch);

        final AccountKit_MainActivity thisActivity = this;
        final LinearLayout advancedUIOptionsLayout = findViewById(R.id.advanced_ui_options);

        final List<CharSequence> buttonNames = new ArrayList<>();
        buttonNames.add("Default");
        for (ButtonType buttonType : ButtonType.values()) {
            buttonNames.add(buttonType.name());
        }
        final ArrayAdapter<CharSequence> buttonNameAdapter
                = new ArrayAdapter<>(
                thisActivity,
                android.R.layout.simple_spinner_dropdown_item,
                buttonNames);

        advancedUISwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    advancedUIOptionsLayout.setVisibility(View.VISIBLE);

                    final Spinner entryButtonSpinner = findViewById(R.id.entry_button_spinner);
                    if (entryButtonSpinner != null) {
                        entryButtonSpinner.setAdapter(buttonNameAdapter);
                        entryButtonSpinner.setOnItemSelectedListener(
                                new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(
                                            final AdapterView<?> parent,
                                            final View view,
                                            final int position,
                                            final long id) {
                                        // First position is empty, so anything past that
                                        if (position > 0) {
                                            entryButton = ButtonType.valueOf(
                                                    entryButtonSpinner
                                                            .getSelectedItem()
                                                            .toString());
                                        } else {
                                            entryButton = null;
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(final AdapterView<?> parent) {
                                        entryButton = null;
                                    }
                                });
                    }

                    final Spinner confirmButtonSpinner = findViewById(R.id.confirm_button_spinner);
                    if (confirmButtonSpinner != null) {
                        confirmButtonSpinner.setAdapter(buttonNameAdapter);
                        confirmButtonSpinner.setOnItemSelectedListener(
                                new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(
                                            final AdapterView<?> parent,
                                            final View view,
                                            final int position,
                                            final long id) {
                                        // First position is empty, so anything past
                                        // that
                                        if (position > 0) {
                                            confirmButton = ButtonType.valueOf(
                                                    confirmButtonSpinner
                                                            .getSelectedItem()
                                                            .toString());
                                        } else {
                                            confirmButton = null;
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(
                                            final AdapterView<?> parent) {
                                        confirmButton = null;
                                    }
                                });
                    }

                    final Spinner textPositionSpinner = findViewById(R.id.text_position_spinner);
                    if (textPositionSpinner != null) {
                        final List<CharSequence> textPositions = new ArrayList<>();
                        textPositions.add("Default");
                        for (TextPosition textPosition : TextPosition.values()) {
                            textPositions.add(textPosition.name());
                        }
                        final ArrayAdapter<CharSequence> textPositionAdapter
                                = new ArrayAdapter<>(
                                thisActivity,
                                android.R.layout.simple_spinner_dropdown_item,
                                textPositions);

                        textPositionSpinner.setAdapter(textPositionAdapter);
                        textPositionSpinner.setOnItemSelectedListener(
                                new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(
                                            final AdapterView<?> parent,
                                            final View view,
                                            final int position,
                                            final long id) {
                                        // First position is empty, so anything past
                                        // that
                                        if (position > 0) {
                                            textPosition = TextPosition.valueOf(
                                                    textPositionSpinner
                                                            .getSelectedItem()
                                                            .toString());
                                        } else {
                                            textPosition = null;
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(
                                            final AdapterView<?> parent) {
                                        textPosition = null;
                                    }
                                });
                    }
                } else if (isReverbThemeSelected()) {
                    advancedUISwitch.setChecked(true);
                   /* Toast.makeText(
                            AccountKit_MainActivity.this,
                            R.string.reverb_advanced_ui_required,
                            Toast.LENGTH_LONG)
                            .show();*/
                } else {
                    advancedUIOptionsLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext())
                .unregisterReceiver(switchLoginTypeReceiver);
         mixpanel.flush();

        super.onDestroy();
    }

    /*public void onLoginEmail(final View view) {
        onLogin(LoginType.EMAIL);
    }*/

    public void onLoginPhone(final View view) {
        onLogin(LoginType.PHONE);
    }

    @Override
    protected void onActivityResult(final int requestCode,
                                    final int resultCode,
                                    final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG+" onActRes", "Req->"+requestCode+" Res->"+resultCode);
        if (requestCode == FRAMEWORK_REQUEST_CODE) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    Object value = bundle.get(key);
                    assert value != null;
                    Log.e(TAG+" onActRes", "Intent Values.->"
                            +String.format("%s %s (%s)", key, value.toString(),
                            value.getClass().getName()));
                }
            }
            final AccountKitLoginResult loginResult =
                    data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);

            final String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
                showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
                try {
                    JSONObject user_lytics = new JSONObject();
                    user_lytics.put("State", "onAppLoad");
                    mixpanel.track("Splash Screen", user_lytics);
                } catch (JSONException e) {
                    Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
                    //Sending an exception event to Sentry
                    Sentry.capture(e);
                }
            } else {
                final AccessToken accessToken = loginResult.getAccessToken();
                final String authorizationCode = loginResult.getAuthorizationCode();
                final long tokenRefreshIntervalInSeconds =
                        loginResult.getTokenRefreshIntervalInSeconds();
                /*Checking if an access token is returned and not null
                * */
                if (accessToken != null) {
                    toastMessage = "Success:" + accessToken.getAccountId()
                            + tokenRefreshIntervalInSeconds;

                    //cellNum=AccountKit.getCurrentPhoneNumberLogInModel().getPhoneNumber().getPhoneNumber();
                   // cellNum=((EditText)findViewById(R.id.com_accountkit_phone_number)).getText().toString();
                    Log.e(TAG+"onActRes","AccessToken->"+accessToken.getToken());
                 //   qman.getQman().registerWithToken(accessToken.getToken());
                    AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                        @Override
                        public void onSuccess(final Account account) {
                            // Get Account Kit ID
                            String accountKitId = account.getId();

                            Log.e(TAG+ " onSuccess", "AccountKit ID->"+accountKitId);
                            // Get phone number
                            JSONObject userPhone = new JSONObject();
                            JSONObject userDetails = new JSONObject();
                            PhoneNumber phoneNumber = account.getPhoneNumber();
                            String phoneNumberString = phoneNumber.toString();
                            //OneSignal player_id
                            String playerId;
                            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
                            playerId = status.getSubscriptionStatus().getUserId();

                            String id= qman.getQman().isRegistered()?
                                    qman.getQman().getUmmoId():"NOT REGISTERED";
                            String name= qman.getQman().isRegistered()?
                                    qman.getQman().getName():"NOT REGISTERED";
                            String contact= qman.getQman().isRegistered()?
                                    qman.getQman().getCellNumb():"NOT REGISTERED";

                            Log.e(TAG+" onSuccess", "Phone Number->"+phoneNumberString);
                            try {
                                userPhone.put("$phone", phoneNumberString);
                                userDetails.put("User-Name", name);
                                userDetails.put("User-Contact", contact);
                                userDetails.put("playerId", playerId);

                                mixpanel.getPeople().set(userPhone);
                                mixpanel.getPeople().set(userDetails);
                                mixpanel.getPeople().identify(id);
                                mixpanel.getPeople().initPushHandling(firebaseSenderID);
                                Log.e(TAG+" onSuccess","Successfully set 'userPhone' object");
                            } catch (JSONException e) {
                                Log.e(TAG+" onSuccess","Failed to set 'userPhone' object",e);
                                e.printStackTrace();
                            }
                            getQman().register(getQman().getUname(),
                                    getQman().getName(),
                                    getQman().getSurname(),
                                    phoneNumberString,playerId);

                            //Storing the user data into Room
                            createUserSession(appDB, getQman().getName(),getQman().getSurname(),
                                    phoneNumberString);
                            //After successfully registering the user
                            startActivity(new Intent(AccountKit_MainActivity.this,
                                    BottomNavActivity.class));

                            AccountKit_MainActivity.this.finish();
                        }

                        @Override
                        public void onError(final AccountKitError error) {
                            // Handle Error
                            Log.e(TAG+" onError"," Error->"+error.toString());
                        }
                    });

                } else if (authorizationCode != null) {
                    toastMessage = String.format(
                            "Success:%s...",
                            authorizationCode.substring(0, 10));
                    Log.e(TAG+" onActRes","Authorization Code->"+authorizationCode);
                    //showHelloActivity();
                } else {
                    toastMessage = "Unknown response type";
                }
            }
        }
    }

    private AccountKitActivity.ResponseType getResponseType() {
        final Switch responseTypeSwitch = findViewById(R.id.response_type_switch);
        if (responseTypeSwitch != null && responseTypeSwitch.isChecked()) {
            return AccountKitActivity.ResponseType.TOKEN;
        } else {
            return AccountKitActivity.ResponseType.CODE;
        }
        //return AccountKitActivity.ResponseType.TOKEN;
    }

    private AccountKitConfiguration.AccountKitConfigurationBuilder createAccountKitConfiguration(
            final LoginType loginType) {
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder
                = new AccountKitConfiguration.AccountKitConfigurationBuilder(
                loginType,
                getResponseType());

        final Switch titleTypeSwitch = findViewById(R.id.title_type_switch);
        final Switch stateParamSwitch = findViewById(R.id.state_param_switch);
        final Switch facebookNotificationsSwitch = findViewById(R.id.facebook_notification_switch);
        final Switch useManualWhiteListBlacklist = findViewById(R.id.whitelist_blacklist_switch);
        final Switch readPhoneStateSwitch = findViewById(R.id.read_phone_state_switch);
        final Switch receiveSMS = findViewById(R.id.receive_sms_switch);

        if (titleTypeSwitch != null && titleTypeSwitch.isChecked()) {
            configurationBuilder.setTitleType(AccountKitActivity.TitleType.APP_NAME);
        }
        if (advancedUISwitch != null && advancedUISwitch.isChecked()) {
            if (isReverbThemeSelected()) {
                if (switchLoginTypeReceiver == null) {
                    switchLoginTypeReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(final Context context, final Intent intent) {
                            final String loginTypeString
                                    = intent.getStringExtra(ReverbUIManager.LOGIN_TYPE_EXTRA);
                            if (loginTypeString == null) {
                                return;
                            }
                            final LoginType loginType = LoginType.valueOf(loginTypeString);
                            onLogin(loginType);
                        }
                    };
                    LocalBroadcastManager.getInstance(getApplicationContext())
                            .registerReceiver(
                                    switchLoginTypeReceiver,
                                    new IntentFilter(ReverbUIManager.SWITCH_LOGIN_TYPE_EVENT));
                }
                configurationBuilder.setAdvancedUIManager(new ReverbUIManager(
                        confirmButton,
                        entryButton,
                        loginType,
                        textPosition,
                        selectedThemeId));
            } else {
                configurationBuilder.setAdvancedUIManager(new AccountKitAdvancedUIManager(
                        confirmButton,
                        entryButton,
                        textPosition));
            }
        }
        if (stateParamSwitch != null && stateParamSwitch.isChecked()) {
            String initialStateParam = UUID.randomUUID().toString();
            configurationBuilder.setInitialAuthState(initialStateParam);
        }
        if (facebookNotificationsSwitch != null && !facebookNotificationsSwitch.isChecked()) {
            configurationBuilder.setFacebookNotificationsEnabled(false);
        }
        if (selectedThemeId > 0) {
            configurationBuilder.setTheme(selectedThemeId);
        }
        if (useManualWhiteListBlacklist != null && useManualWhiteListBlacklist.isChecked()) {
            final String[] blackList
                    = getResources().getStringArray(R.array.blacklistedSmsCountryCodes);
            final String[] whiteList
                    = getResources().getStringArray(R.array.whitelistedSmsCountryCodes);
            configurationBuilder.setSMSBlacklist(blackList);
            configurationBuilder.setSMSWhitelist(whiteList);
        }
        if (readPhoneStateSwitch != null && !(readPhoneStateSwitch.isChecked())) {
            configurationBuilder.setReadPhoneStateEnabled(false);
        }
        if (receiveSMS != null && !receiveSMS.isChecked()) {
            configurationBuilder.setReceiveSMS(false);
        }

        return configurationBuilder;
    }

    private boolean isReverbThemeSelected() {
        return selectedThemeId == R.style.AppLoginTheme_Reverb_A
                || selectedThemeId == R.style.AppLoginTheme_Reverb_B
                || selectedThemeId == R.style.AppLoginTheme_Reverb_C;
    }

    private void onLogin(final LoginType loginType) {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder
                = createAccountKitConfiguration(loginType);
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());

        String name = ummoName.getText().toString();
        String surName = ummoSurname.getText().toString();
        getQman().setName(name);
        getQman().setSurname(surName);

        mixpanel= MixpanelAPI.getInstance(this, projectToken);

        try {
            JSONObject user_lytics = new JSONObject();
            user_lytics.put("State", "onSignUp");
            mixpanel.track("Sign-up", user_lytics);
        } catch (JSONException e) {
            Log.e(TAG+" onLogin", "Unable to add properties to JSONObject", e);
        }

        if (name.isEmpty() || name.length() < 3) {
            ummoName.setError("Your name should have at least 3 letters!");
            return;
        }
        else
            ummoName.setError(null);

        if (surName.isEmpty() || surName.length() < 3) {
            ummoSurname.setError("Your surname should have at least 3 letters!");
            return;
        }
        else
            ummoSurname.setError(null);

        startActivityForResult(intent, FRAMEWORK_REQUEST_CODE);
    }

    private void createUserSession(AppDatabase appDb, String name, String surname, String contact){

        User user = new User();
        user.name = name;
        user.surname = surname;
        user.cell_num = contact;
        appDb.userModel().createUser(user);

        Log.e(TAG+ " createUser", "Room Query 1->"+appDb);

    }

    private void showHelloActivity() {
        /*final Intent intent = new Intent(this, TokenActivity.class);
        intent.putExtra(
                TokenActivity.HELLO_TOKEN_ACTIVITY_INITIAL_STATE_EXTRA,
                initialStateParam);
        intent.putExtra(TokenActivity.HELLO_TOKEN_ACTIVITY_FINAL_STATE_EXTRA, finalState);
        startActivity(intent);*/

        onClick();
    }

    private void showErrorActivity(final AccountKitError error) {
        final Intent intent = new Intent(this, ErrorActivity.class);
        //intent.putExtra(ErrorActivity.HELLO_TOKEN_ACTIVITY_ERROR_EXTRA, error);
        startActivity(intent);
    }
}