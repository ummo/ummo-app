package com.ummo.prod.user.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.AddTrace;
import com.google.firebase.perf.metrics.Trace;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.ummo.prod.user.db.room.AppDatabase;
import com.ummo.prod.user.qman;
import com.ummo.prod.user.accountKit.AccountKit_MainActivity;
import com.ummo.prod.user.R;
import com.ummo.prod.user.SingleFragmentActivity;
import com.ummo.prod.user.interfaces.UmmoEventListener;
import com.ummo.prod.user.ummoAPI.QUser;
import com.ummo.prod.user.ummoAPI.Interfaces.UserListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Timer;

import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;
import io.sentry.event.UserBuilder;
import io.socket.emitter.Emitter;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by barnes on 8/6/15.
 **/
public class SplashScreen extends Activity implements UserListener {
    private String name = "Ummo Splash";
    //final String projectToken = "67547a5eed2c873e61ccbfd531eb6d26";
    //final String projectToken = getString(R.string.mixpanelToken);
    final String firebaseSenderID = "438150256556";
    public MixpanelAPI mixpanel;
    private String sharedPrefFile = "com.ummo.prod.user";
    private String TAG = "SplashScreen";
    private BottomNavActivity bottomNavActivity = new BottomNavActivity();
    private AppDatabase appDb;
    private String playerId;

    final LinkedHashMap<String, Class<?>> listItems = new LinkedHashMap<>();
    long Delay = 10000;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void joinedQsError(String err) {

    }

    @Override
    public void gotJoinedQs(String string) {
        new QUser(this).getCategories();
    }

    @Override
    public void qReady(String string) {

    }

    @Override
    public void qError(String err) {

    }

    @Override
    public void userRegistered(String string) {
        // parse the string a JSONObject or JSONArray
        //Registration  is successful , Start Main Activity
    }

    @Override
    public void qJoined(String string) {

    }

    @Override
    public void qLeft(String string) {

    }

    @Override
    public void updated(String string) {
        new QUser(this).getCategories();
    }

    /**
     *When the categories have been retrieved (ready), the splash screen
     * should finish running and hand over to the next activity;
     */
    @Override
    public void categoriesReady(String string) {

        finish();
        //final Animation fadein = AnimationUtils.loadAnimation(this, R.anim.simple_grow);
        //final Animation fadeout = AnimationUtils.loadAnimation(this, R.anim.simple_shrink);
        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    /**
     *When the services have been retrieved (ready), the splash screen
     * should hands over the flow to the intent 'i' with the extras passed
     * into it;
     */
    @Override
    public void allQsReady(String string) {
        Log.e(TAG+" allQsReady","data->"+string);
        listItems.put("Selectable Nodes", BottomNavActivity.class);
        Class<?> clazz = listItems.values().toArray(new Class<?>[]{})[0];
        Intent i = new Intent(SplashScreen.this, SingleFragmentActivity.class);
        i.putExtra(SingleFragmentActivity.FRAGMENT_PARAM, clazz);
        SplashScreen.this.startActivity(i);
        SplashScreen.this.finish();
        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }
    @Override
    public void userRegistrationError(String err) {
        //There was an Error During Registration
    }

    @Override
    public void qJoinedError(String err) {

    }

    @Override
    public void qLeftError(String err) {

    }

    @Override
    public void updateError(String err) {

    }

    @Override
    public void categoriesError(String err) {

    }

    @Override
    public void allQError(String err) {
        Log.e(TAG+" Error Getting Qs",err);
    }

    /*public void enableStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build());
    }*/

    public void setTokens(){
        //Kicking off our Firebase App with the token
        FirebaseApp.initializeApp(this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        String userContact = qman.getQman().getCellNumb();
        JSONObject userToken = new JSONObject();
        //OneSignal player_id
        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        playerId = status.getSubscriptionStatus().getUserId();
        Log.e(TAG+" setTokens", "OneSignal player_id->"+playerId);

        try {
            userToken.put("userContact", userContact);
            userToken.put("fcmToken", refreshedToken);
            userToken.put("playerId", playerId);
        } catch (JSONException e){
            e.printStackTrace();
            Log.e(TAG+" setFCM", "Couldn't complete action ->", e);
        }

        if (refreshedToken!= null){
            Log.e(TAG+" FCM TOKEN", "Refreshed token: " + userToken);
//            qman.getSocket().emit("setFcmToken", userToken);
            qman.getSocket().emit("setUserToken", userToken);
        }
        else
            Log.e(TAG+" FCM TOKEN", "Refreshed token is null");
    }

    @AddTrace(name = "onSplashTrace")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ummo_splash);
        setTokens();
        appDb = AppDatabase.getInMemoryDatabase(getApplicationContext());

        final String projectToken = getString(R.string.mixpanelToken);

        final SharedPreferences sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        //TODO:Finish off what was started here (a tad unclear for now) #FB
        FacebookSdk.getApplicationContext();
        AppEventsLogger.activateApp(getApplication());

        //TODO: Finish off the notification subscription #Fb
        FirebaseMessaging.getInstance().subscribeToTopic("ummo-production");

        Context context = this.getApplicationContext();

        String id= qman.getQman().isRegistered()?
                qman.getQman().getUmmoId():"NOT REGISTERED (NO ID)";
        String name= qman.getQman().isRegistered()?
                qman.getQman().getName():"NOT REGISTERED (NO NAME)";
        String contact= qman.getQman().isRegistered()?
                qman.getQman().getCellNumb():"NOT REGISTERED (NO CONTACT)";

        mixpanel= MixpanelAPI.getInstance(this, projectToken);

        Log.e("Mixpanel Token",projectToken);
        mixpanel.identify(id);
        mixpanel.getPeople().identify(id);
        mixpanel.getPeople().set("qman Name", name);
        mixpanel.getPeople().set("qman Contact", contact);

        mixpanel = MixpanelAPI.getInstance(this, projectToken);
        mixpanel.getPeople().initPushHandling(firebaseSenderID);
        MixpanelAPI.People people = mixpanel.getPeople();
        people.identify(name);
        people.initPushHandling(firebaseSenderID);

        try {
            JSONObject user_lytics = new JSONObject();
            user_lytics.put("State", "onAppLoad");
            mixpanel.track("Splash Screen", user_lytics);
        } catch (JSONException e) {
            Log.e("Ummo-qman", "Unable to add properties to JSONObject", e);
            //Sending an exception event to Sentry
            Sentry.capture(e);
        }

        /*
        * Here, we;re error tracking with #Sentry and capturing the name;
        * the contact and the ID;
        * */
        //Using the Sentry DSN
        String sentryDSN = getResources().getString(R.string.sentryDSN);
        Sentry.init(sentryDSN, new AndroidSentryClientFactory(context));
        //Setting Username, Contact (IP Address), and ID
        Sentry.getContext().setUser(new UserBuilder().setUsername(name).build());
        Sentry.getContext().setUser(new UserBuilder().setIpAddress(contact).build());
        Sentry.getContext().setUser(new UserBuilder().setId(id).build());
        //TODO: Finish setting up Sentry event capturing
        //Sending an event
        //Sentry.capture("This is a test");

        //TODO: Change this event from 'register' to 'registered'
        qman.getQman().registerEventListener("register", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                //startActivity(new Intent(SplashScreen.this,SelectableTreeFragment_dead.class));
                startActivity(new Intent(SplashScreen.this, BottomNavActivity.class));
                Log.e(TAG+" onCreate","isRegistered; data->"+data.toString());
                return null;
            }
        });

        //Testing Firebase Performance Traces
        Trace myTrace = FirebasePerformance.getInstance().newTrace("test_trace");
        myTrace.start();

        Timer RunSplash = new Timer();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                }
                catch (InterruptedException ie){
                    Log.e("Interruptus",ie.toString());
                }

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(qman.getQman());
                String cellNumb = sp.getString("cellNumb","000");

                if(!cellNumb.equals("000")/*qman.getQman().isRegistered()*/){

                    SharedPreferences sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);

                    try {
                        fetchingServiceQueue();
                        String fetchedQueueDetails = sharedPreferences.getString("SAVED_QUEUE_DATA", "");
                        Log.e(TAG+" onCreate","Fetching Done (within isRegistered)->"+fetchedQueueDetails);
                        //Setting SavedQueueDetails in BottomNavActivity
                        bottomNavActivity.setSavedQueueDetails(new JSONArray(fetchedQueueDetails));

                        fetchJoinedQueue();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG+" onCreate", "JSE->"+e);
                    }

                    startActivity(new Intent(SplashScreen.this, BottomNavActivity.class));
                    SplashScreen.this.finish();
                }
                else {
                    //Running a mock registration for Espresso testing purposes
                    //TODO:Inject this data via the Mock variant
                    //qman.getQman().register("Tester","Espresso","Framework","+26879157688");
                    try {
                        fetchingServiceQueue();
                        String fetchedQueueDetails = sharedPreferences.getString("SAVED_QUEUE_DATA", "");
                        Log.e(TAG+" onCreate","Fetching Done (without isRegistered)->"+fetchedQueueDetails);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG+" onCreate", "JSE->", e);
                    }

                    /* Checking if there's a valid contact that may have been saved from a
                     *  previous attempt at signing in (that could've been interrupted);
                     */
                    if(cellNumb.equals("000")){
                        startActivity(new Intent(SplashScreen.this, AccountKit_MainActivity.class));
                        SplashScreen.this.finish();
                    }else{
                        String firstName = sp.getString("firstName","Recovered");
                        String surName = sp.getString("firstName","Recovered");
                        String uname = sp.getString("uname","");
                        qman.getQman().register(uname,firstName,surName,cellNumb,playerId);
                        startActivity(new Intent(SplashScreen.this,BottomNavActivity.class));
                    }
                }
            }
        }).start();
        //Firebase Performance trace
        myTrace.stop();

        //TODO: Figure out what was happening with Kitkat and why;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            // Kitkat and lower has a bug that can cause in correct strict mode
            // warnings about expected activity counts
           // enableStrictMode();
        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Ubuntu-C.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        //finish();
        //QUser user = new QUser(SplashScreen.this);
    }

    /*private void createUser(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                User user = new User();
                user.cell_num = "76157688";
                user.name = qman.getQman().getName();
                user.surname = qman.getQman().getSurname();
                appDb.userModel().createUser(user);

                Log.e(TAG+ " createUser", "Room Query 3 | User->"+user);
            }
        });

    }*/

    @Override
    protected void onDestroy() {
        mixpanel.flush();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    //TODO:Write function to fetch joinedQueueData

    private void fetchJoinedQueue(){
        final JSONObject[] joinedQueue = new JSONObject[1];
        qman.getSocket().emit("load_qman_queue_joins", qman.getQman().getCellNumb());
        qman.getSocket().on("qman_queue_joins", new Emitter.Listener() {
            JSONObject object;

            @Override
            public void call(Object... args) {
                try {
                    JSONArray array=(JSONArray)args[0];
                    //Log.e("LOADED JOINS",array.toString());
                    for (int i=0;i<array.length();i++){
                        object = array.getJSONObject(i);
                        Log.e(TAG+" fetchJoinedQueue","User Queue Joins (object)->"+object);
                        joinedQueue[0] =object;
                        Log.e(TAG+" fetchJoinedQueue","User Queue Joins (joinedQueue)->"+ Arrays.toString(joinedQueue));
                    }
                    SharedPreferences sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("SAVED_JOINED_QUEUES", Arrays.toString(joinedQueue)).apply();
                    Log.e(TAG+" fetchJoinedQ", "Saved Queue Joins"+ Arrays.toString(joinedQueue));

                }catch (JSONException jse){
                    Log.e("JSON ERR",getClass().getCanonicalName()+jse.toString());
                }
            }
        });
    }

    private void fetchingServiceQueue() throws JSONException {
        JSONArray categoriesArray = qman.getQman().getCategories();
        JSONArray queueDetails = new JSONArray();
        for (int i=0; i<categoriesArray.length(); i++){
            JSONObject categoriesObject = categoriesArray.getJSONObject(i);

            if (categoriesObject.has("providers")){
                JSONArray providersArray = categoriesObject.getJSONArray("providers");

                for (int j=0; j<providersArray.length(); j++){
                    JSONObject providersObject = providersArray.getJSONObject(j);
                    queueDetails = providersObject.has("queues")
                            ?providersObject.getJSONArray("queues"):new JSONArray();

                    if (queueDetails.length()!=0){
                        Log.e(TAG+" fetchingQ", "QueueDetails->"+queueDetails);
                    }
                }
            }
        }

        SharedPreferences sharedPreferences = getSharedPreferences(sharedPrefFile,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("SAVED_QUEUE_DATA", queueDetails.toString()).apply();
    }
}