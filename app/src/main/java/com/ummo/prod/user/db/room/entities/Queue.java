package com.ummo.prod.user.db.room.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity (indices = {@Index("queue_id"),@Index(value = {"service_id"},unique = true)},
        foreignKeys = {@ForeignKey(entity = Service.class,
                                    parentColumns = "service_id",
                                    childColumns = "service_id")})
public class Queue {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "queue_id")
    public String queue_id;

    @ColumnInfo(name = "queue_name")
    public String queue_name;

    @ColumnInfo(name = "service_id")
    public String service_id;

    @ColumnInfo(name = "queue_requirements")
    public String queue_req;

    @ColumnInfo(name = "queue_location")
    public String queue_location;

    @ColumnInfo(name = "queue_length")
    public int queue_length;

    @ColumnInfo(name = "queue_ttdq")
    public int queue_ttdq;
}