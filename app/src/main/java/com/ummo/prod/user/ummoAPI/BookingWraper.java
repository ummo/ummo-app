package com.ummo.prod.user.ummoAPI;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ummo.prod.user.qman;
import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by mosaic on 4/28/17.
 **/

public class BookingWraper{
    private String _id;
    private String service;
    private Calendar start;
    private Calendar end;
    private int hash;
    private String agentId;
    private String bookerId;
    private Agent agent;
    private AlarmManager alarmMgr;
    private AlarmManager alarmMgr2;
    private PendingIntent alarmIntent;
    private PendingIntent alarmIntent2;
    public static List<BookingWraper> bookings = new ArrayList<>();
    private static List<BookingWraper> myBookings = new ArrayList<>();
    private static String selectedAgent="";
    private static BookingWraper selectedBooking=null;


    private static DataReadyCallback onLoadedBookings = null;
    private static DataReadyCallback onBookingCreated = null;
    private static DataReadyCallback onScheduled = null;

    public static void setOnScheduled(DataReadyCallback onScheduled1){
        BookingWraper.onScheduled=onScheduled1;
    }

    public static void setOnBookingCreated(DataReadyCallback onBookingCreated) {
        BookingWraper.onBookingCreated = onBookingCreated;
    }

    public static BookingWraper getSelectedBooking() {
        return selectedBooking;
    }

    public static void setSelectedBooking(BookingWraper selectedBooking) {
        BookingWraper.selectedBooking = selectedBooking;
    }

    public static void setSelectedAgent(String selectedAgent) {
        BookingWraper.selectedAgent = selectedAgent;
    }

    public static List<BookingWraper> getMyBookings(){
        return myBookings;
    }

    public static void initMyBookings(){
        Log.e("INIT","MY_BOOKINGS");

        SocketClass.emit("user-bookings", qman.getQman().getUmmoId(), new SocketClass.Response() {
            @Override
            public Object ready(Object val) {
                Log.e("RETURNED_MY_BOOKINGS",val.toString());
                JSONArray array = (JSONArray)val;
                myBookings.clear();
                for(int i =0;i<array.length();i++){
                    try {
                        myBookings.add(new BookingWraper(array.getJSONObject(i)));
                        Log.e("LOADING BOOKING",""+i);
                    }catch (JSONException jse){
                        Log.e("ERRLOADBOOKINGS",jse.toString());
                    }
                }
                Log.e("LOADED MY BOOKING",""+myBookings.size());
                if (onLoadedBookings!=null) onLoadedBookings.exec(bookings);

                return null;
            }
        });
    }



    public static void setOnLoadedBookings(DataReadyCallback onLoadedBookings) {
        BookingWraper.onLoadedBookings = onLoadedBookings;
        if(bookings.size()>0) BookingWraper.onLoadedBookings.exec(bookings);
    }


    private static Socket socket = qman.getSocket();

    private int getHash(){
        if(_id==null)return 0;
        int _hash = 7;
        for (int i = _id.length()-1; i > _id.length()-5; i--) {
            _hash = _hash*31 + _id.charAt(i);
        }
        hash=_hash;
        return _hash;
    }

    public static void loadAgentBookings(String a_id){
        selectedAgent=a_id;
        socket.emit("get-agent-bookings",a_id);
        socket.on("agent-bookings", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("RETURNED_BOOKINGS",args[0].toString());
                JSONArray array = (JSONArray)args[0];
                bookings.clear();
                for(int i =0;i<array.length();i++){
                    try {
                        bookings.add(new BookingWraper(array.getJSONObject(i)));
                        Log.e("LOADING BOOKING",""+i);
                    }catch (JSONException jse){
                        Log.e("ERRLOADBOOKINGS",jse.toString());
                    }
                }
                Log.e("LOADED BOOKING",""+bookings.size());
                if (onLoadedBookings!=null) onLoadedBookings.exec(bookings);
            }
        });
    }



    public Calendar getEnd() {
        return end;
    }

    public Calendar getStart() {
        return start;
    }

    public String get_id() {
        return _id;
    }

    public String getAgentId() {
        return agentId;
    }

    public String getBookerId() {
        return bookerId;
    }

    public String getService() {
        return service;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public void setBookerId(String bookerId) {
        this.bookerId = bookerId;
    }

    public void setEnd(Calendar end) {
        this.end = end;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public static void createBooking(Calendar _start,Calendar _end, String service,String agentId){
        String booker = qman.getQman().getUmmoId();
        try {
            JSONObject object = new JSONObject();
            object.put("booker",booker);
            object.put("agent",agentId);
            object.put("service",service);
            int offset =_start.getTimeZone().getRawOffset();
            object.put("start",_start.getTimeInMillis()+offset);
            object.put("end",_end.getTimeInMillis()+offset);
            SocketClass.emit("create-booking-mod", object, new SocketClass.Response() {
                @Override
                public Object ready(Object val) {
                    Log.e("BookingWrapper","Booking Created"+val.toString());
                    return onBookingCreated==null?null:onBookingCreated.exec(val);
                }
            });
        }catch (JSONException jse){
            Log.e("JSONE",jse.toString());
        }
    }

    public void cancelBooking(){
        try {
            JSONObject obj = new JSONObject();
            obj.put("user", qman.getQman().getUmmoId());
            obj.put("booking",_id);
            SocketClass.emit("cancel-booking", obj, new SocketClass.Response() {
                @Override
                public Object ready(Object val) {
                    myBookings.clear();
                    JSONArray array = (JSONArray)val;
                    for(int i =0;i<array.length();i++){
                        try {
                            myBookings.add(new BookingWraper(array.getJSONObject(i)));
                            Log.e("LOADING BOOKING",""+i);
                        }catch (JSONException jse){
                            Log.e("ERRLOADBOOKINGS",jse.toString());
                        }
                    }
                    Log.e("LOADED MY BOOKING",""+myBookings.size());
                    if (onLoadedBookings!=null) onLoadedBookings.exec(bookings);
                    return null;
                }
            });
        }catch (JSONException jse){
            Log.e("CANCELERR",jse.toString());
        }

    }

    /*public void setAlarm(){
        alarmMgr = (AlarmManager) qman.getQman().getSystemService(Context.ALARM_SERVICE);
        alarmMgr2 = (AlarmManager) qman.getQman().getSystemService(Context.ALARM_SERVICE);
        //Intent intent = new Intent(qman.getQman(), AlarmReceiver.class);
        //intent.putExtra("service",getService());
        //intent.putExtra("period"," about two hours");
        //intent.putExtra("id",get_id());
        //intent.putExtra("hash",getHash());

        Intent intent2 = new Intent(qman.getQman(), AlarmReceiver.class);
        intent2.putExtra("service",getService());
        intent2.putExtra("period"," a day");
        intent2.putExtra("id",get_id());
        intent2.putExtra("hash",getHash()+2);
        alarmIntent = PendingIntent.getBroadcast(qman.getQman(),getHash(),intent,PendingIntent.FLAG_UPDATE_CURRENT);
        alarmIntent2 = PendingIntent.getBroadcast(qman.getQman(),getHash()+2,intent2,PendingIntent.FLAG_UPDATE_CURRENT);
        long two_hour = start.getTime().getTime()-2*60*60*1000;
        long day = start.getTime().getTime()-24*60*60*1000;
        long now = Calendar.getInstance().getTime().getTime();
        if(now>two_hour)return;
        alarmMgr.set(AlarmManager.RTC_WAKEUP,two_hour,alarmIntent);
        if (now>day)return;
        alarmMgr2.set(AlarmManager.RTC_WAKEUP,day,alarmIntent2);
    }*/

    public BookingWraper(JSONObject booking){ //This Constructor Takes A JSONOBJECT with an already populated agent and booker
        try{
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            set_id(booking.getString("_id"));

            setBookerId(booking.has("booker")?booking.getJSONObject("booker").getString("_id"):"booking-mod");
            setService(booking.getString("service"));
            Calendar cal = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();

            if(booking.has("agent")){
                agent = new Agent();
                //agent.setUname(booking.getJSONObject("agent").getString("firstName"));
                agent.setEmail(booking.getJSONObject("agent").getString("email"));
                agent.setFirstName(booking.getJSONObject("agent").getJSONObject("fullName").getString("firstName"));
                agent.setSurName(booking.getJSONObject("agent").getJSONObject("fullName").getString("surName"));
                setAgentId(booking.getJSONObject("agent").getString("_id"));
            }else{
                Log.e("BOOKINGSWRAPPER","Ths booking has no agent");
            }
            try {
                cal.setTime(simpleDateFormat.parse(booking.getString("start")));
                cal2.setTime(simpleDateFormat.parse(booking.getString("end")));
                setStart(cal);
                setEnd(cal2);
                getHash();
            }catch (ParseException jse){
                Log.e("PARSEERR",jse.toString());
            }
        }catch (JSONException jse){
            Log.e("JSONERR",jse.toString()+booking.toString());
        }
    }



    public BookingWraper(JSONObject booking,Agent _agent){ //This Constructor Takes A JSONOBJECT with an already populated agent and booker
        try{
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            set_id(booking.getString("_id"));
            setBookerId(booking.has("booker")?booking.getString("booker"):"Mod");

            //setBookerId(booking.has("booker")?booking.getJSONObject("booker").getString("_id"):"booking-mod");
            setService(booking.getString("service"));
            Calendar cal = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();

            agent=_agent;
            try {
                cal.setTime(simpleDateFormat.parse(booking.getString("start")));
                cal2.setTime(simpleDateFormat.parse(booking.getString("end")));
                setStart(cal);
                setEnd(cal2);
                getHash();
            }catch (ParseException jse){
                Log.e("PARSEERR",jse.toString());
            }
        }catch (JSONException jse){
            Log.e("JSONERR",jse.toString()+booking.toString());
        }
    }

    public Agent getAgent() {
        return agent;
    }

    public void reschedule(){
        try {
            JSONObject object = new JSONObject();
            object.put("_id",_id);
            object.put("booker",bookerId);
            object.put("agent",agentId);
            int offset =start.getTimeZone().getRawOffset();
            object.put("start",start.getTimeInMillis()+offset);
            object.put("end",end.getTimeInMillis()+offset);
            SocketClass.emit("reschedule_booking", object, new SocketClass.Response() {
                @Override
                public Object ready(Object val) {
                    Log.e("BookingWrapper","Scheduled Bookig" + val.toString());
                    JSONArray array = (JSONArray)val;
                    myBookings.clear();
                    for(int i =0;i<array.length();i++){
                        try {
                            myBookings.add(new BookingWraper(array.getJSONObject(i)));
                            Log.e("LOADING BOOKING",""+i);
                        }catch (JSONException jse){
                            Log.e("ERRLOADBOOKINGS",jse.toString());
                        }
                    }
                    Log.e("LOADED MY BOOKING",""+myBookings.size());
                    if (onLoadedBookings!=null) onLoadedBookings.exec(bookings);
                    return null;
                }
            });
        }catch (JSONException jse){
            Log.e("BookingWraper","RescheduleErr"+jse.toString());
        }
    }
}
