package com.ummo.prod.user.ummoAPI.Interfaces;

/**
 * Created by barnes on 11/1/15.
 **/
public interface UserListener {
    void userRegistered(String string);
    void qJoined(String string);
    void qLeft(String string);
    void updated(String string);
    void categoriesReady(String string);
    void allQsReady(String string);
    void qReady(String string);
    void gotJoinedQs(String string);

    //Errors
    void joinedQsError(String err);
    void userRegistrationError(String err);
    void qJoinedError(String err);
    void qLeftError(String err);
    void updateError(String err);
    void categoriesError(String err);
    void allQError(String err);
    void qError(String err);
}