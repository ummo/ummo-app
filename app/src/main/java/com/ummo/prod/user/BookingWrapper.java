package com.ummo.prod.user;

import android.util.Log;

import com.ummo.prod.user.ummoAPI.Interfaces.DataReadyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by mosaic on 5/22/17.
 **/

public class BookingWrapper {
    private static List<BookingWrapper> bookings = new ArrayList<>();
    private String _id;
    private String name;
    private String cell;
    private Calendar start;
    private Calendar end;
    private String service;
    private static Socket socket = qman.getSocket();

    private static DataReadyCallback onLoadedBookings = null;

    public static void setOnLoadedBookings(DataReadyCallback onLoadedBookings) {
        BookingWrapper.onLoadedBookings = onLoadedBookings;
        if(bookings.size()>0) BookingWrapper.onLoadedBookings.exec(bookings);
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getCell() {
        return cell;
    }

    public static List<BookingWrapper> getBookings() {
        return bookings;
    }

    public static void setBookings(List<BookingWrapper> bookings) {
        BookingWrapper.bookings = bookings;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getStart() {
        return start;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public Calendar getEnd() {
        return end;
    }

    public void setEnd(Calendar end) {
        this.end = end;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public BookingWrapper(){

    }



    private BookingWrapper(JSONObject obj){
        try {
            set_id(obj.getString("_id"));
            setName(obj.getJSONObject("user").getString("name"));
            setService(obj.getString("service"));
            setCell(obj.getJSONObject("user").getString("cell"));
            setStart(Calendar.getInstance());
            setEnd(Calendar.getInstance());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            simpleDateFormat.setTimeZone(TimeZone.getDefault());

            try {
                start.setTime(simpleDateFormat.parse(obj.getString("start")));
                end.setTime(simpleDateFormat.parse(obj.getString("end")));
                Log.e("BOOKING_TIMES",start.getTime().toString()+" => "+getEnd().getTime().toString());
            }catch (ParseException pse){
                Log.e("PARSERR",getClass().getCanonicalName()+pse.toString());
            }

        }catch (JSONException jse){
            Log.e("ERRNEWBOOKING",jse.toString()+obj.toString());
        }
    }

    public static void loadBookings(){
        Log.e("LOADBOOKINGS","LOADING");
        socket.emit("get-user-bookings", qman.getQman().getUmmoId());
        socket.on("user-bookings", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("AGENT_BOOKINGS",args[0].toString());
                JSONArray array = (JSONArray)args[0];
                bookings.clear();
                for(int i =0;i<array.length();i++){
                    try {
                        bookings.add(new BookingWrapper(array.getJSONObject(i)));
                    }catch (JSONException jse){
                        Log.e("ERRLOADBOOKINGS",jse.toString());
                    }
                }
                if (onLoadedBookings!=null) onLoadedBookings.exec(bookings);
            }
        });
    }

    public static void createBooking(String name, String cell, Calendar start,Calendar end,String service){
        Log.e("Create Booking"," Start "+start.getTimeInMillis()+" End "+end.getTime().toString());
        JSONObject object = new JSONObject();
        JSONObject user = new JSONObject();
        Calendar _start = start;
        Calendar _end = end;
        int offset =_end.getTimeZone().getRawOffset();
        Log.e("START",start.getTimeZone().toString()+" , "+_start.getTime().toString());
        try{
            user.put("name",name);
            user.put("cell",cell);
            object.put("user",user);
            object.put("service",service);
            object.put("agent", qman.getQman().getUmmoId());
            object.put("start",_start.getTimeInMillis()+offset);
            object.put("end",_end.getTimeInMillis()+offset);
            socket.emit("create-user-booking",object);
        }catch (JSONException jse){
            Log.e("ERRCREATEBOOKING",jse.toString());
        }
    }
}
