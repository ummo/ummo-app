package com.ummo.prod.user.interfaces;

/**
 * Created by mosaic on 7/22/16.
 **/
public interface UmmoEventListener {
    Object run(Object data);
}



