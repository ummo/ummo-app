package com.ummo.prod.user;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ummo.prod.user.R;

/**
 * Created by barnes on 7/3/16.
 **/
public class ReverbBodyFragment extends Fragment {
    private static final String ICON_RESOURCE_ID_KEY = "iconResourceId";
    private static final String SHOW_PROGRESS_SPINNER_KEY = "showProgressSpinner";

    private int iconResourceId = 0;
    private boolean showProgressSpinner;

    public void setIconResourceId(final int iconResourceId) {
        this.iconResourceId = iconResourceId;
        updateIcon(getView());
    }

    public void setShowProgressSpinner(final boolean showProgressSpinner) {
        this.showProgressSpinner = showProgressSpinner;
        updateIcon(getView());
    }

    public View onCreateView(
            final LayoutInflater inflater,
            final ViewGroup container,
            final Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            iconResourceId = savedInstanceState.getInt(ICON_RESOURCE_ID_KEY, iconResourceId);
            showProgressSpinner = savedInstanceState.getBoolean(
                    SHOW_PROGRESS_SPINNER_KEY,
                    showProgressSpinner);
        }

        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_reverb_body, container, false);
        }
        updateIcon(view);
        return view;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(ICON_RESOURCE_ID_KEY, iconResourceId);
        outState.putBoolean(SHOW_PROGRESS_SPINNER_KEY, showProgressSpinner);
    }

    private void updateIcon(@Nullable final View view) {
        if (view == null) {
            return;
        }

        final View progressSpinner = view.findViewById(R.id.reverb_progress_spinner);
        if (progressSpinner != null) {
            progressSpinner.setVisibility(showProgressSpinner ? View.VISIBLE : View.GONE);
        }

        final ImageView iconView = view.findViewById(R.id.reverb_icon);
        if (iconView != null) {
            if (iconResourceId > 0) {
                iconView.setImageResource(iconResourceId);
                iconView.setVisibility(View.VISIBLE);
            } else {
                iconView.setVisibility(View.GONE);
            }
        }
    }
}
