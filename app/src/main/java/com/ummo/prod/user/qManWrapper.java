package com.ummo.prod.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by mosaic on 7/16/16.
 **/
public class qManWrapper {
    private String uname;
    private String firstName;
    private String surName;
    private String cellNum;
    private boolean registered;
    private  String ummoId;
    private String fcmToken;
    Context context;

    qManWrapper(Context _context){
        context=_context;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        uname=sp.getString("UNAME","DEFAULT");
        firstName=sp.getString("FIRSTNAME","DEFAULT");
        surName=sp.getString("SURNAME","DEFAULT");
        cellNum=sp.getString("CELLNUM","DEFAULT");
        ummoId=sp.getString("UMMO_ID","DEFAULT");
        registered=sp.getBoolean("QMAN_REGISTERED",false);

    }

    void save(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString("UNAME",uname).apply();
        sp.edit().putString("FIRSTNAME",firstName).apply();
        sp.edit().putString("SURNAME",surName).apply();
        sp.edit().putString("CELLNUM",cellNum).apply();
        sp.edit().putString("UMMO_ID",ummoId).apply();
        sp.edit().putBoolean("QMAN_REGISTERED",registered).apply();
    }
    void setRegistered(){
        registered=true;
    }

    public boolean isRegistered(){
        return registered;
    }

    void setUname(String _name){
        uname=_name;
    }

    void setFirstName(String _name){
        firstName=_name;
    }

    void setSurName(String surName) {
        this.surName = surName;
    }

    void setUmmoId(String ummoId) {
        this.ummoId = ummoId;
    }

    void setCellNum(String _ce){
        this.cellNum=_ce;
    }

    String getCellNum() {
        return cellNum;
    }

    String getFirstName() {
        return firstName;
    }

    String getSurName() {
        return surName;
    }

    String getUmmoId() {
        return ummoId;
    }

    String getUname() {
        return uname;
    }

}
