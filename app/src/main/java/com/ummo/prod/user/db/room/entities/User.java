package com.ummo.prod.user.db.room.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.util.ArrayList;

@Entity (/*indices = {@Index(value = {"joined_queue"},unique = true)},
        foreignKeys = @ForeignKey(entity = Queue.class,
                                    parentColumns = "queue_id",
                                    childColumns = "joined_queue")*/)
public class User {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "user_contact")
    public String cell_num;

    @ColumnInfo(name = "user_name")
    public String name;

    @ColumnInfo(name = "user_surname")
    public String surname;

    /*@ColumnInfo(name = "joined_queue")
    public ArrayList<String> joined_queue;

    public int position;*/
}