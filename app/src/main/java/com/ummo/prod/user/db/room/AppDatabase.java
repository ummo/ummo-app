package com.ummo.prod.user.db.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.ummo.prod.user.db.room.dao.QueueDao;
import com.ummo.prod.user.db.room.dao.QueueJoinDao;
import com.ummo.prod.user.db.room.dao.ServiceDao;
import com.ummo.prod.user.db.room.dao.UserDao;
import com.ummo.prod.user.db.room.entities.Queue;
import com.ummo.prod.user.db.room.entities.QueueJoin;
import com.ummo.prod.user.db.room.entities.Service;
import com.ummo.prod.user.db.room.entities.User;

@Database(entities = {Queue.class, Service.class, User.class, QueueJoin.class}, version = 1)
@TypeConverters(ArrayListConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "ummo_user_db";

    private static AppDatabase INSTANCE;

    public abstract QueueDao queueModel();

    public abstract ServiceDao serviceModel();

    public abstract UserDao userModel();

    public abstract QueueJoinDao queueJoinModel();

    public static AppDatabase getInMemoryDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.inMemoryDatabaseBuilder(context.getApplicationContext(), AppDatabase.class)
                    .allowMainThreadQueries()
                    .build();
            /*INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();*/
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}