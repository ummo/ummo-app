package com.ummo.prod.user.view.fragment;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.ummo.prod.user.R;

/**
 * Created by barnes on 12/17/15.
 **/
@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    protected void setUpToolbarWithTitle(String title) {
        Toolbar toolBar = findViewById(R.id.toolbar_actionbar_);
        setSupportActionBar(toolBar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    protected void enterFromBottomAnimation() {
        overridePendingTransition(R.anim.activity_open_translate_from_bottom, R.anim.activity_no_animation);
    }

    protected void exitToBottomAnimation() {
        overridePendingTransition(R.anim.activity_no_animation, R.anim.activity_close_translate_to_bottom);
    }
}
