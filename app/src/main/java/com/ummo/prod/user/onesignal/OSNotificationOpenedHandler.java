package com.ummo.prod.user.onesignal;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.ummo.prod.user.MainActivity;
import com.ummo.prod.user.Main_Activity;
import com.ummo.prod.user.view.BottomNavActivity;
import com.ummo.prod.user.view.fragment.DiscoverFragment;

import org.json.JSONObject;

public class OSNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler{
    private Application application;
    private static final String TAG = "OSNotifHandler ";
    private static String click_action;

    public OSNotificationOpenedHandler(Application application){
        this.application = application;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        //Get custom data from notification
        JSONObject data = result.notification.payload.additionalData;

        if (data != null){
            Log.e(TAG+"onOpen", "data received->"+data);
            click_action = data.optString("click_action", null);
            Log.e(TAG+"onOpen", "click_action received->"+click_action);
        }

        //React to open
        OSNotificationAction.ActionType actionType = result.action.type;

        if (actionType == OSNotificationAction.ActionType.Opened){
            Log.e(TAG+"onOpen","Notification opened with id:"+result.action.actionID);
            Intent intent = new Intent(application, Main_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
//            application.startActivity(intent);
            launchActivity();
        }
    }

    private void launchActivity(){
        Intent intent;
        switch (click_action) {
            case "RPT_ACTIVITY":
                intent = new Intent(application, Main_Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                Log.e(TAG+"send", "onSwitch click_action final->"+click_action);
                break;
            case "HOME_ACTIVITY":
                intent = new Intent(application, BottomNavActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case "DISCOVER_ACTIVITY":
                intent = new Intent(application, DiscoverFragment.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            default:
                intent = new Intent(application, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                Log.e(TAG+"send", "onSwitch click_action final->"+click_action);
                break;
        }
        //Starting the called activity
        application.startActivity(intent);
    }
}
