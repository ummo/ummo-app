package com.ummo.prod.user.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.user.R;
import com.ummo.prod.user.RVItemDecoration;
import com.ummo.prod.user.view.fragment.DiscoverFragment;
import com.ummo.prod.user.ummoAPI.AgentServiceWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mosaic on 4/19/17.
 **/

public class BookingTreeAdapter extends RecyclerView.Adapter<BookingTreeAdapter.ViewHolder>  {
    private DiscoverFragment parent;
    private List<AgentServiceWrapper> data;
    final String projectToken = "67547a5eed2c873e61ccbfd531eb6d26";
    public MixpanelAPI mixpanelAPI;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        RelativeLayout bookingCard;
        LinearLayout linearLayout;
        public RecyclerView recyclerView;
        public List<AgentServiceWrapper> services=new ArrayList<>();
        BookingRecAdapter adapter;
        public ImageView moreBtn;
        TextView serviceNameTv;

        ViewHolder(RelativeLayout v) {
            super(v);
            bookingCard = v;
            linearLayout = v.findViewById(R.id.cat_ly);
            moreBtn = v.findViewById(R.id.more_btn);
            serviceNameTv= v.findViewById(R.id.booking_service_tv);
            recyclerView = v.findViewById(R.id.services_recycler);
            recyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        AgentServiceWrapper service = data.get(position);
        holder.serviceNameTv.setText(service.getName());
        holder.recyclerView.addItemDecoration(new RVItemDecoration(20));
        holder.adapter = new BookingRecAdapter(data.get(position)
                .getAgents(),parent,data.get(position).getLocation());
        holder.recyclerView.setAdapter(holder.adapter);
        holder.adapter.notifyDataSetChanged();
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.recyclerView.setVisibility(holder.recyclerView.getVisibility()==View.GONE?View.VISIBLE :View.GONE);
                holder.moreBtn.setImageResource(holder.recyclerView.getVisibility()==View.VISIBLE?R.drawable.ic_discover_business_unpressed:R.drawable.ic_discover_business_pressed);
            }
        });
    }
    public BookingTreeAdapter(List _data, DiscoverFragment _parent){
        data=_data;
        parent=_parent;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_tree_fragment, parent, false);
        return new ViewHolder(v);
    }
    @Override
    public int getItemCount() {
        return data.size();
    }
}
