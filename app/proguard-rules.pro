-dontwarn org.w3c.dom.**
-dontwarn org.joda.time.**
-dontwarn org.shaded.apache.**
-dontwarn org.ietf.jgss.**
-dontwarn com.firebase.**
-dontwarn android.support.**
-dontwarn org.slf4j.**
-dontwarn javax.**
-dontwarn bolts.**
-dontwarn com.google.common.**
-dontwarn com.crashlytics.**
-dontwarn com.mixpanel.**
-dontwarn com.facebook.**

-dontnote com.firebase.client.core.GaePlatform

#-printseeds#TODO:Note what this does, functionally; as well as other short-key functions

-dontpreverify
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic
-keepattributes Signature
-keepattributes *Annotation*
-keepattributes InnerClasses,EnclosingMethod,LineNumberTable,SourceFile

-keep class com.ummo.prod.user.** { *; }
-keep class cn.pedant.SweetAlert.** { *; }
-keep class com.crashlytics.** { *; }
-keep public class * extends java.lang.Exception
#Not recommended
#-keep class !com.ummo.prod.user.** { *; }

# Basic ProGuard rules for Firebase Android SDK 2.0.0+
-keep class com.firebase.** { *; }
#Other Google Libraries
#-keep class com.google.android.** {*;}
-keep class com.google.** {*;}
-keep class com.google.common.util.** {*;}

#Preserving all Facebook related SDKs, including AccountKit & GraphAPI
-keep class com.facebook.** { *; }
#-keep class com.facebook.FacebookSdk {
#   boolean isInitilized();
#}
#-keep class com.facebook.appevents.AppEventsLogger {
#   com.facebook.appevents.AppEventsLogger newLogger(android.content.Context);
#   void logSdkEvent(java.lang.String, java.lang.Double, android.os.Bundle);
#}====
#Socket.IO
-keep class com.github.nkzawa.**
-keepclasseswithmembers,allowshrinking class com.github.nkzawa.* { *; }
-keep class socket.io-client.**
-keepclasseswithmembers,allowshrinking class socket.io-client.* { *; }
-keep class io.socket.**
-keepclasseswithmembers,allowshrinking class io.socket.* { *; }

-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }

#Processing Butterknife code:
#   Preserves the Butterknife annotations, annotated fields & methods as well as
#   generated classes & methods that Butterknife accesses by reflection.
-keep @interface butterknife.*

-keepclasseswithmembers class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembers class * {
    @butterknife.* <methods>;
}

-keepclasseswithmembers class * {
    @butterknife.On* <methods>;
}

-keep class **$$ViewInjector {
    public static void inject(...);
    public static void reset(...);
}

-keep class **$$ViewBinder {
    public static void bind(...);
    public static void unbind(...);
}
#Removing logging code
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}